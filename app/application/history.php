<?php
error_reporting(0);
header("Content-type: application/json");
include_once 'config.php';

$connect = new ConnectionApi();

if(isset($_GET["acces"])) :
	$accesId = $_GET["acces"];
	if($accesId == "letter") :
		if(isset($_GET["nik"])) :
			$nik   = $_GET["nik"];
			$rows  = array();
			$query = $connect->query("SELECT * FROM sir_permintaan A INNER JOIN sir_warga B ON A.NIK = B.W_NIK WHERE A.NIK = '$nik' ORDER BY A.PER_CREATED_AT DESC"); 
			while($row = $query->fetch_assoc()) :
				$rows[]= $row;
			endwhile;

			echo json_encode($rows);
		else :
			$response["error"] = TRUE;
			$response["kode"]  = 0;
			$response["status"]= 404;
			$response["msg"]   = "NIK tidak ditemukan";
			echo json_encode($response);
		endif;
	elseif($accesId == "aduan") :
		if(isset($_GET["nik"])) :
			$nik   = $_GET["nik"];
			$rows  = array();
			$query = $connect->query("SELECT * FROM sir_pengaduan A INNER JOIN sir_warga B ON A.SP_NIK = B.W_NIK WHERE A.SP_NIK = '$nik' ORDER BY SP_CREATED_AT DESC"); 
			while($row = $query->fetch_assoc()) :
				$rows[]= $row;
			endwhile;

			echo json_encode($rows);
		else :
			$response["error"] = TRUE;
			$response["kode"]  = 0;
			$response["status"]= 404;
			$response["msg"]   = "NIK tidak ditemukan";
			echo json_encode($response);
		endif;
	else :
		$response["error"] = TRUE;
		$response["kode"]  = 0;
		$response["status"]= 404;
		$response["msg"]   = "Parameter Acces Anda tidak ditemukan";
		echo json_encode($response);
	endif;
else :
	$response["error"] = TRUE;
	$response["kode"]  = 3;
	$response["status"]= 403;
	$response["msg"]   = "Parameter Acces Invalid";
	echo json_encode($response);
endif;