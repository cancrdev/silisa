<?php
// session_start();
error_reporting(0);
include_once 'config.php';
include_once '../../controller/globalController.php';

$connect = new ConnectionApi();
$log     = new globalController();

$value   = $_GET["key"];
$userId  = $_GET["nik"];
$kode    = $connect->query("SELECT * FROM sir_news WHERE N_ID = '$value'");
$kodeId1 = $kode->fetch_assoc();

//user
$user    = $connect->query("SELECT * FROM sir_user WHERE U_NIK = '$userId'");
$usr     = $user->fetch_assoc();

//comment
$query   = $connect->query("SELECT * FROM sir_comment WHERE SC_BERITAID = '$value'");
//count jumlah
$jml     = $connect->query("SELECT COUNT(SC_BERITAID) jumlah FROM sir_comment WHERE SC_BERITAID = '$value'");
$comment = $jml->fetch_assoc()
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="description" content="">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <!-- Title -->
  <title>Detail Berita</title>
  <!-- Core Stylesheet -->
  <link rel="stylesheet" href="<?= $log->base_url(); ?>assets/visitor/style.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</head>

<body>
  <div class="preloader d-flex align-items-center justify-content-center">
    <div class="spinner"></div>
  </div>
  <!-- Preloader -->
  <?php if(!empty($_GET["key"])) { ?>

  <!-- ##### News Details Area Start ##### -->
  <section class="news-details-area section-padding-0-100">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <!-- News Details Content -->
          <div class="news-details-content">
            <h2 class="post-title"><?php echo $kodeId1["N_TITLE"]; ?></h2>
            <h6>Dipublikasi <a href="#" class="post-date"><?php echo $log->TanggalIndo($kodeId1["N_TANGGAL"]); ?></a> / <a href="#" class="post-author"><?php echo $kodeId1["N_CREATED_BY"]; ?></a></h6><br>
            <img src="<?= $log->base_url(); ?>assets/news/<?= $kodeId1["N_IMAGE"]; ?>" alt="gambar" width="1100px" height="450px" >
            <p><?php echo $kodeId1["N_SUBJECT"]; ?></p>

            <!-- Share & Tags -->
            <div class="share-tags d-flex flex-wrap align-items-center justify-content-between">
              <!-- Share -->
              <!-- Tags -->
            </div>
          </div>

          <!-- Post Author -->
          <!-- Comment Area Start -->
          <div class="comment_area clearfix mb-50">

            <!-- Title -->
            <h3 class="mb-50"><?= $comment["jumlah"]." Komentar"; ?></h3>

            <ul>
              <!-- Single Comment Area -->
              <?php while($cm = $query->fetch_assoc()) : ?>
              <li class="single_comment_area">
                <!-- Comment Content -->
                <div class="comment-content d-flex">
                  <!-- Comment Author -->
                  <div class="comment-author">
                    <?php if($usr["U_AVATAR"] == "" || $usr["U_AVATAR"] == NULL) :  ?>
                    <img src="<?= $log->base_url(); ?>assets/visitor/img/bg-img/22.jpg" alt="author">
                    <?php else :  ?>
                    <img src="<?= $log->base_url(); ?>assets/avatar/<?= $usr["U_AVATAR"]; ?>" alt="author">
                    <?php endif; ?>
                  </div>
                  <!-- Comment Meta -->
                  <div class="comment-meta">
                    <a href="#" class="comment-date"><?= $log->TanggalIndo($cm["SC_CREATED_AT"]); ?></a>
                    <h6><?= $cm["SC_FULLNAME"]; ?></h6>
                    <p><?= $cm["SC_SUBJECT"]; ?></p>
                  </div>
                </div>
              </li>
              <?php endwhile; ?>
            </ul>
          </div>

          <!-- Post A Comment Area -->
          <div class="post-a-comment-area">

            <!-- Title -->
            <h3 class="mb-50">Komentari Berita</h3>

            <!-- Reply Form -->
            <div class="contact-form-area">
              <?php 
              if(isset($_POST["save"])) {
               $newsId   = $connect->clean_post($_POST["newsId"]);
               $fullname = $connect->clean_post($_POST["fullname"]);
               $email    = $connect->clean_post($_POST["email"]);
               $subject  = $connect->clean_post($_POST["subject"]);
               
               $sql  = $connect->query("INSERT INTO sir_comment (SC_BERITAID, SC_FULLNAME, SC_EMAIL, SC_SUBJECT) VALUES ('$newsId', '$fullname', '$email', '$subject')");

               if($query) : ?>
               <script type="text/javascript">
                   sweetAlert({
                      position: 'top-end',
                      type: 'success',
                      title: 'Komentar Anda Berhasil Dipublikasin',
                      showConfirmButton: true,
                      showLoaderOnConfirm: true,
                    }, function() {
                          window.location = "newsdetail-web?acces=detail&key=<?= $value; ?>&nik=<?= $userId; ?>";
                      })</script>
               <?php else : ?>
               <script type="text/javascript">sweetAlert("Oops...", "Inputan gagal disimpan :(", "error");</script>
               <?php endif; } ?>
              <form action="" method="post">
              	<input type="hidden" name="newsId" value="<?php echo $_GET["key"]; ?>">
                <div class="row">
                  <div class="col-12 col-lg-6">
                    <input type="text" class="form-control" name="fullname" value="<?= $usr["U_FULLNAME"]; ?>" required id="name" placeholder="Your Name*">
                  </div>
                  <div class="col-12 col-lg-6">
                    <input type="email" class="form-control" name="email" id="email" value="<?= $usr["U_EMAIL"]; ?>" placeholder="Your Email*">
                  </div>
                  <div class="col-12">
                    <textarea name="subject" class="form-control" id="message" placeholder="Message*"></textarea>
                  </div>
                  <div class="col-12">
                    <button class="btn famie-btn mt-30" style="background-color: #2693ca;" name="save" type="submit">Submit Comment</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php } else { ?>
  <!-- ##### News Details Area Start ##### -->
  <section class="news-details-area section-padding-0-100">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <!-- News Details Content -->
          <div class="news-details-content">
            <h4 class="text-center">Data Tidak ditemukan</h4>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php } ?>
  <!-- ##### All Javascript Files ##### -->
  <!-- jquery 2.2.4  -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jquery.min.js"></script>
  <!-- Popper js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/popper.min.js"></script>
  <!-- Bootstrap js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/bootstrap.min.js"></script>
  <!-- Owl Carousel js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/owl.carousel.min.js"></script>
  <!-- Classynav -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/classynav.js"></script>
  <!-- Wow js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/wow.min.js"></script>
  <!-- Sticky js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jquery.sticky.js"></script>
  <!-- Magnific Popup js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jquery.magnific-popup.min.js"></script>
  <!-- Scrollup js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jquery.scrollup.min.js"></script>
  <!-- Jarallax js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jarallax.min.js"></script>
  <!-- Jarallax Video js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jarallax-video.min.js"></script>
  <!-- Active js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/active.js"></script>
</body>
</html>