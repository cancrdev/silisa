<?php
error_reporting(0);
header("Content-type: application/json");
include_once 'config.php';

$connect = new ConnectionApi();

if(isset($_GET["acces"])) :
	$accesId = $_GET["acces"];
	if($accesId == "login") :
		if(isset($_POST["loginNIK"])) {
			$nik   = $connect->clean_all($_POST["loginNIK"]);
			$ip       = $_SERVER['REMOTE_ADDR'];
		    $browser  = $_SERVER['HTTP_USER_AGENT'];
		    $token    = base64_encode(date('YmdHis').$nik);
		    $now      = date("Y-m-d H:i:s");

			$query = $connect->query("SELECT * FROM sir_user WHERE U_NIK = '$nik'");
			// $num   = $connect->num_rows($query);

			if(mysqli_num_rows($query) > 0) {
				$row  = $query->fetch_assoc();
				$userId = $row["ID_USER"];
				$update = $connect->query("UPDATE sir_user SET U_LOGIN_TOKEN = '$token', U_LOGIN_WAKTU = '$now', U_DEFAULT_BROWSER = '$browser', U_IP_POSITION = '$ip' WHERE U_NIK = '$nik'");

				//cek statusrow["U_FULLNAME"];
				if($row["U_STATUS"] == 1) {
					// cek user per rule acces
					if($row["U_GROUP_RULE"] == "U_RT") :
						//get data rt
						$sql = $connect->query("SELECT P_RT, P_RW FROM sir_rt WHERE P_USERID = '$userId' AND P_STATUS = '1'");
						$data= $sql->fetch_assoc();
						//end
						$response["error"] 		= FALSE;
						$response["kode"]  		= 1;
						$response["status"]		= 200;
						$response["msg"]   		= "List Data User RT"; 
						$response["userId"]		= $row["ID_USER"];
						$response["nik"]   		= $row["U_NIK"];
						$response["fullname"]	= $row["U_FULLNAME"];
						$response["email"]		= $row["U_EMAIL"];
						$response["telpon"]     = $row["U_MOBILE"];
						$response["rule"] 		= $row["U_GROUP_RULE"];
						$response["status"]     = $row["U_STATUS"];
						$response["token"]      = $row["U_LOGIN_TOKEN"];
						$response["login_waktu"]= $row["U_LOGIN_WAKTU"];
						$response["ip_position"]= $row["U_IP_POSITION"];
						$response["ketua_rt"]   = $data["P_RT"];
						$response["rw"]			= $data["P_RW"];
						echo json_encode($response);
					elseif($row["U_GROUP_RULE"] == "U_RW") :
						$sql  = $connect->query("SELECT PW_RW FROM sir_rw WHERE PW_USERID = '$userId' AND PW_STATUS = '1'");
						$data = $sql->fetch_assoc();

						$response["error"] 		= FALSE;
						$response["kode"]  		= 1;
						$response["status"]		= 200;
						$response["msg"]   		= "List Data User RW"; 
						$response["userId"]		= $row["ID_USER"];
						$response["nik"]   		= $row["U_NIK"];
						$response["fullname"]	= $row["U_FULLNAME"];
						$response["email"]		= $row["U_EMAIL"];
						$response["telpon"]     = $row["U_MOBILE"];
						$response["rule"] 		= $row["U_GROUP_RULE"];
						$response["status"]     = $row["U_STATUS"];
						$response["token"]      = $row["U_LOGIN_TOKEN"];
						$response["login_waktu"]= $row["U_LOGIN_WAKTU"];
						$response["ip_position"]= $row["U_IP_POSITION"];
						$response["ketua_rw"]			= $data["PW_RW"];
						echo json_encode($response);
					elseif($row["U_GROUP_RULE"] == "U_WARGA") :
						$sql = $connect->query("SELECT * FROM sir_warga WHERE W_NIK = '$nik'");
						$data= $sql->fetch_assoc();
						$response["error"] 		= FALSE;
						$response["kode"]  		= 1;
						$response["status"]		= 200;
						$response["msg"]   		= "List Data User Warga"; 
						$response["userId"]		= $row["ID_USER"];
						$response["nik"]   		= $row["U_NIK"];
						$response["fullname"]	= $row["U_FULLNAME"];
						$response["email"]		= $row["U_EMAIL"];
						$response["telpon"]     = $row["U_MOBILE"];
						$response["rule"] 		= $row["U_GROUP_RULE"];
						$response["status"]     = $row["U_STATUS"];
						$response["token"]      = $row["U_LOGIN_TOKEN"];
						$response["login_waktu"]= $row["U_LOGIN_WAKTU"];
						$response["ip_position"]= $row["U_IP_POSITION"];
						$response["rt"]   		= $data["W_RT"];
						$response["rw"]			= $data["W_RW"];
						$response["agama"]      = $data["W_AGAMA"];
						$response["no_kk"]		= $data["W_KK"];
						echo json_encode($response);
					else :
						$response["error"]  = TRUE;
						$response["kode"]   = 3;
						$response["status"] = 403;
						$response["msg"]    = "Anda Tidak Memiliki Akses Kesini";
						echo json_encode($response);
					endif; 
				} else {
					$response["error"] 	= TRUE;
					$response["kode"]  	= 1;
					$response["status"] = 200;
					$response["msg"]	= "Akun Anda belum aktif,silahkan update password anda untuk aktivasi";
					$response["no_nik"] = $nik; 
					echo json_encode($response);
				}
			} else {
				$response["error"]  = TRUE;
				$response["kode"]   = 0;
				$response["status"] = 404;
				$response["msg"]    = "Akun anda tidak ditemukan, silahkan hubungi administrator desa untuk info lebih lanjut";
				echo json_encode($response);
			}
		} else {
			$response["error"] = TRUE;
			$response["kode"]  = 2;
			$response["status"]= 200;
			$response["msg"]   = "Parameter kurang";
			echo json_encode($response);
		}
	elseif($accesId == "aktivasi") :
		if(isset($_GET["nik"])) :
			$nik = $_GET["nik"];
			if(isset($_POST["password"]) || isset($_POST["confirm"])) :
				$password = $connect->clean_post($_POST["password"]);
				$confirm  = $connect->clean_post($_POST["confirm"]);
				$ip       = $_SERVER['REMOTE_ADDR'];
		    	$browser  = $_SERVER['HTTP_USER_AGENT'];
		    	$token    = base64_encode(date('YmdHis').$nik);
		    	$now      = date("Y-m-d H:i:s");
				$pass     = md5(md5($password));

				if($password != $confirm) :
					$response["error"] = TRUE;
					$response["kode"]  = 2;
					$response["status"]= 200;
					$response["msg"]   = "Password tidak ada yang cocok";
					echo json_encode($response);
				else :
					$query = $connect->query("UPDATE sir_user SET U_PASSWORD = '$pass', U_STATUS = '1' WHERE U_NIK = '$nik'");
					$update = $connect->query("UPDATE sir_user SET U_LOGIN_TOKEN = '$token', U_LOGIN_WAKTU = '$now', U_DEFAULT_BROWSER = '$browser', U_IP_POSITION = '$ip' WHERE U_NIK = '$nik'");
					if($query && $update) :
						$sql  = $connect->query("SELECT * FROM sir_user WHERE U_NIK = '$nik'");
						$row  = $sql->fetch_assoc();

						$response["error"] = TRUE;
						$response["kode"]  = 1;
						$response["status"]= 200;
						$response["msg"]   = "Profile berhasil diaktifkan";
						if($row["U_GROUP_RULE"] == "U_RT") :
						//get data rt
							$sql = $connect->query("SELECT P_RT, P_RW FROM sir_rt WHERE P_USERID = '$userId' AND P_STATUS = '1'");
							$data= $sql->fetch_assoc();
							//end
							$response["error"] 		= FALSE;
							$response["kode"]  		= 1;
							$response["status"]		= 200;
							$response["msg"]   		= "List Data User RT"; 
							$response["userId"]		= $row["ID_USER"];
							$response["nik"]   		= $row["U_NIK"];
							$response["fullname"]	= $row["U_FULLNAME"];
							$response["email"]		= $row["U_EMAIL"];
							$response["telpon"]     = $row["U_MOBILE"];
							$response["rule"] 		= $row["U_GROUP_RULE"];
							$response["status"]     = $row["U_STATUS"];
							$response["token"]      = $row["U_LOGIN_TOKEN"];
							$response["login_waktu"]= $row["U_LOGIN_WAKTU"];
							$response["ip_position"]= $row["U_IP_POSITION"];
							$response["ketua_rt"]   = $data["P_RT"];
							$response["rw"]			= $data["P_RW"];
							echo json_encode($response);
						elseif($row["U_GROUP_RULE"] == "U_RW") :
							$sql  = $connect->query("SELECT PW_RW FROM sir_rw WHERE PW_USERID = '$userId' AND PW_STATUS = '1'");
							$data = $sql->fetch_assoc();

							$response["error"] 		= FALSE;
							$response["kode"]  		= 1;
							$response["status"]		= 200;
							$response["msg"]   		= "List Data User RT"; 
							$response["userId"]		= $row["ID_USER"];
							$response["nik"]   		= $row["U_NIK"];
							$response["fullname"]	= $row["U_FULLNAME"];
							$response["email"]		= $row["U_EMAIL"];
							$response["telpon"]     = $row["U_MOBILE"];
							$response["rule"] 		= $row["U_GROUP_RULE"];
							$response["status"]     = $row["U_STATUS"];
							$response["token"]      = $row["U_LOGIN_TOKEN"];
							$response["login_waktu"]= $row["U_LOGIN_WAKTU"];
							$response["ip_position"]= $row["U_IP_POSITION"];
							$response["ketua_rw"]			= $data["PW_RW"];
							echo json_encode($response);
						elseif($row["U_GROUP_RULE"] == "U_WARGA") :
							$sql = $connect->query("SELECT * FROM sir_warga WHERE W_NIK = '$nik'");
							$data= $sql->fetch_assoc();
							$response["error"] 		= FALSE;
							$response["kode"]  		= 1;
							$response["status"]		= 200;
							$response["msg"]   		= "List Data User Warga"; 
							$response["userId"]		= $row["ID_USER"];
							$response["nik"]   		= $row["U_NIK"];
							$response["fullname"]	= $row["U_FULLNAME"];
							$response["email"]		= $row["U_EMAIL"];
							$response["telpon"]     = $row["U_MOBILE"];
							$response["rule"] 		= $row["U_GROUP_RULE"];
							$response["status"]     = $row["U_STATUS"];
							$response["token"]      = $row["U_LOGIN_TOKEN"];
							$response["login_waktu"]= $row["U_LOGIN_WAKTU"];
							$response["ip_position"]= $row["U_IP_POSITION"];
							$response["rt"]   		= $data["W_RT"];
							$response["rw"]			= $data["W_RW"];
							$response["agama"]      = $data["W_AGAMA"];
							$response["no_kk"]		= $data["W_KK"];
							echo json_encode($response);
						else :
							$response["error"]  = TRUE;
							$response["kode"]   = 3;
							$response["status"] = 403;
							$response["msg"]    = "Anda Tidak Memiliki Akses Kesini";
							echo json_encode($response);
						endif; 
						echo json_encode($response);
					else :
						$response["error"] = TRUE;
						$response["kode"]  = 2;
						$response["status"]= 200;
						$response["msg"]   = "Profile Gagal diaktifkan, silahkan ulangi kembali beberapa saat lagi";
						echo json_encode($response);
					endif;
				endif;
			else :
				$response["error"] = TRUE;
				$response["kode"]  = 2;
				$response["status"]= 200;
				$response["msg"]   = "Parameter anda kurang";
				echo json_encode($response);
			endif;
		else :
			$response["error"] = TRUE;
			$response["kode"]  = 0;
			$response["status"]= 404;
			$response["msg"]   = "Parameter NIK anda tidak ditemukan";
			echo json_encode($response);
		endif;
	else :
		$response["error"] = TRUE;
		$response["kode"]  = 0;
		$response["status"]= 404;
		$response["msg"]   = "Parameter Acces anda tidak ditemukan";
		echo json_encode($response);
	endif;
else :
	$response["error"] = TRUE;
	$response["kode"]  = 3;
	$response["status"]= 403;
	$response["msg"]   = "Parameter Acces Invalid";
	echo json_encode($response);
endif;