<?php
error_reporting(0);
header("Content-type: application/json");
include_once 'config.php';

$connect = new ConnectionApi();

if(isset($_GET["acces"])) :
	$accesId = $_GET["acces"];
	if($accesId == "profile") :
		if(isset($_GET["nik"])) :
			$nik  = $_GET["nik"];
			if(isset($_POST["username"]) || isset($_POST["fullname"]) || isset($_POST["mobile"]) || isset($_POST["email"])) :
				$username = $connect->clean_post($_POST["username"]);
				$fullname = $connect->clean_post($_POST["fullname"]);
				$mobile   = $connect->clean_post($_POST["mobile"]);
				$email    = $connect->clean_post($_POST["email"]);

				//get data user
				$name     = $connect->query("SELECT U_NAME FROM sir_user WHERE U_NIK = '$nik'");
				$mobile   = $connect->query("SELECT U_MOBILE FROM sir_user WHERE U_NIK = '$nik'");
				$mail     = $connect->query("SELECT U_EMAIL FROM sir_user WHERE U_NIK = '$nik'");

				// validasi
				if(mysqli_num_rows($name) > 0) :
					$response["error"] = TRUE;
					$response["kode"]  = 1;
					$response["status"]= 200;
					$response["msg"]   = "username sudah ada,silahkan coba yang lain";
					echo json_encode($response);
				elseif(mysqli_num_rows($mobile) > 0) :
					$response["error"] = TRUE;
					$response["kode"]  = 1;
					$response["status"]= 200;
					$response["msg"]   = "No Telpon sudah ada,silahkan coba yang lain";
					echo json_encode($response);
				elseif(mysqli_num_rows($mail) > 0) :
					$response["error"] = TRUE;
					$response["kode"]  = 1;
					$response["status"]= 200;
					$response["msg"]   = "Email sudah ada,silahkan coba yang lain";
					echo json_encode($response);
				else :
					$query    = $connect->query("UPDATE sir_user SET U_NAME = '$username', U_FULLNAME = '$fullname', U_MOBILE = '$mobile', U_EMAIL = '$email' WHERE U_NIK = '$nik'");
					if($query) :
						$connect->query("UPDATE sir_warga SET W_NAMA = '$fullname' WHERE W_NIK = '$nik'");

						$response["error"] = TRUE;
						$response["kode"]  = 1;
						$response["status"]= 200;
						$response["msg"]   = "Update Profile berhasil";
						echo json_encode($response);
					else :
						$response["error"] = TRUE;
						$response["kode"]  = 1;
						$response["status"]= 200;
						$response["msg"]   = "Update Profile gagal";
						echo json_encode($response);
					endif;
				endif;
			else :
				$response["error"] = TRUE;
				$response["kode"]  = 2;
				$response["status"]= 200;
				$response["msg"]   = "Parameter anda kurang";
				echo json_encode($response);
			endif;
		else :
			$response["error"] = TRUE;
			$response["kode"]  = 0;
			$response["status"]= 404;
			$response["msg"]   = "Parameter NIK anda tidak ditemukan";
			echo json_encode($response);
		endif;
	elseif($accesId == "setting") :
		if(isset($_POST["password"]) || isset($_POST["konfirm"])) :
			$password = $connect->clean_post($_POST["password"]);
			$confirm  = $connect->clean_post($_POST["confirm"]);
			$nik      = $connect->clean_all($_POST["nik"]);

			$pass     = md5(md5($password));

			if($password != $confirm) :
				$response["error"] = TRUE;
				$response["kode"]  = 2;
				$response["status"]= 200;
				$response["msg"]   = "Password tidak ada yang cocok";
				echo json_encode($response);
			else :
				$query = $connect->query("UPDATE sir_user SET U_PASSWORD = '$pass' WHERE U_NIK = '$nik'");
				if($query) :
					$response["error"] = FALSE;
					$response["kode"]  = 1;
					$response["status"]= 200;
					$response["msg"]   = "Update Profile anda berhasil";
					echo json_encode($response);
				else :
					$response["error"] = TRUE;
					$response["kode"]  = 1;
					$response["status"]= 200;
					$response["msg"]   = "Update Profile anda gagal";
					echo json_encode($response);
				endif;
			endif;
		else :
			$response["error"] = TRUE;
			$response["kode"]  = 2;
			$response["status"]= 200;
			$response["msg"]   = "Parameter anda kurang";
			echo json_encode($response);
		endif;
	elseif($accesId == "avatar") :
		$image    = $connect->clean_post($_POST["image"]);
		$nik      = $connect->clean_all($_POST["nik"]);

		$query    = $connect->query("UPDATE sir_user SET U_AVATAR = '$image' WHERE U_NIK = '$nik'");

		if($query) :
			$response["error"] = FALSE;
			$response["kode"]  = 1;
			$response["status"]= 200;
			$response["msg"]   = "Photo profile anda berhasil diubah";
			echo json_encode($response);
		else :
			$response["error"] = TRUE;
			$response["kode"]  = 1;
			$response["status"]= 200;
			$response["msg"]   = "Photo profile anda gagal diubah";
			echo json_encode($response);
		endif;
	elseif($accesId == "upload") :
		$name    = "image21";
		$target_dir = "../../assets/avatar/";
		$image      = $_FILES["image"]["name"];
		$newimage = str_replace(" ", "", basename($name))."_".date('dmYHis')."_".str_replace(" ", "", basename($image));
		$tar_img	= $target_dir . $newimage;
		$upload1 = move_uploaded_file($_FILES["image"]["tmp_name"], $tar_img);

		if($upload1) :
			$response['error'] = FALSE;
			$response['status'] = 200;
			$response['msg'] = 'Berhail Upload Image';
			$response['image_name'] = $newimage;
			echo(json_encode($response));
		exit();
		else :
			$response['error'] = TRUE;
			$response['status'] = 200;
			$response['msg'] = 'gagal Upload Image';
			echo(json_encode($response));
		endif;
	elseif($accesId == "logout") :
		$userId  = $_GET["userId"];
		$date    = date("Y-m-d H:i:s");

		$query   = $connect->query("UPDATE sir_user SET U_LOGOUT_WAKTU = '$date' WHERE ID_USER = '$userId'");
		if($query) :
			$response["error"] = FALSE;
			$response["kode"]  = 1;
			$response["status"]= 200;
			$response["msg"]   = "Anda Berhail logout,dan tercatat dilog pada ".$date;
			echo json_encode($response);
		else :
			$response["error"] = TRUE;
			$response["kode"]  = 1;
			$response["status"]= 200;
			$response["msg"]   = "Anda gagal logout";
			echo json_encode($response);
	else :
		$response["error"] = TRUE;
		$response["kode"]  = 0;
		$response["status"]= 404;
		$response["msg"]   = "Parameter Acces anda tidak ditemukan";
		echo json_encode($response);
	endif;
else :
	$response["error"] = TRUE;
	$response["kode"]  = 3;
	$response["status"]= 403;
	$response["msg"]   = "Parameter Acces Invalid";
	echo json_encode($response);
endif;
?>