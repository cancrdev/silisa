<?php
error_reporting(0);
header("Content-type: application/json");
include_once 'config.php';

$connect = new ConnectionApi();

if(isset($_GET["acces"])) :
	$accesId = $_GET["acces"];
	if($accesId == "waitingrt") :
		$kode  = $_GET["from"];
		$rows  = array();
		$query = $connect->query("SELECT * FROM sir_permintaan WHERE PM_RT = '$kode' AND PER_STATUS = 'WAITING_APPROVAL_RT' ORDER BY PER_CREATED_AT DESC");
		while($row  = $query->fetch_assoc()) :
			$rows[] = $row;
		endwhile;

		echo json_encode($rows);
	elseif($accesId == "updatert") :
		$userId = $_GET["userId"];
		$kode   = $_GET["kode"];

		$query  = $connect->query("UPDATE sir_permintaan SET PER_STATUS = 'WAITING_APPROVAL_RW', PER_ACC_RT = '$userId' WHERE PER_ID = '$kode'");
		if($query) :
			$response["error"] = FALSE;
			$response["kode"]  = 1;
			$response["status"]= 200;
			$response["msg"]   = "update status oleh rt berhasil dilakukan";
			echo json_encode($response);
		else :
			$response["error"] = TRUE;
			$response["kode"]  = 1;
			$response["status"]= 200;
			$response["msg"]   = "update status oleh rt gagal dilakukan";
			echo json_encode($response);
		endif;
	elseif($accesId == "waitingrw") :
		$kode  = $_GET["from"];
		$rows  = array();
		$query = $connect->query("SELECT * FROM sir_permintaan WHERE PM_RW = '$kode' AND PER_STATUS = 'WAITING_APPROVAL_RW' ORDER BY PER_CREATED_AT DESC");
		while($row  = $query->fetch_assoc()) :
			$rows[] = $row;
		endwhile;

		echo json_encode($rows);
	elseif($accesId == "updaterw") :
		$userId = $_GET["userId"];
		$kode   = $_GET["kode"];

		$query  = $connect->query("UPDATE sir_permintaan SET PER_STATUS = 'WAITING_KADES', PER_ACC_RT = '$userId' WHERE PER_ID = '$kode'");
		if($query) :
			$response["error"] = FALSE;
			$response["kode"]  = 1;
			$response["status"]= 200;
			$response["msg"]   = "update status oleh rw berhasil dilakukan";
			echo json_encode($response);
		else :
			$response["error"] = TRUE;
			$response["kode"]  = 1;
			$response["status"]= 200;
			$response["msg"]   = "update status oleh rw gagal dilakukan";
			echo json_encode($response);
		endif;
	elseif($accesId == "waitinglurah") :
		$rows  = array();
		$query = $connect->query("SELECT * FROM sir_permintaan WHERE PER_STATUS = 'WAITING_KADES' ORDER BY PER_CREATED_AT DESC");
		while($row  = $query->fetch_assoc()) :
			$rows[] = $row;
		endwhile;

		echo json_encode($rows);
	elseif($accesId == "toprint") :
		$kode   = $_GET["kode"];
		$query  = $connect->query("UPDATE sir_permintaan SET PER_STATUS = 'PRINT' WHERE PER_ID = '$kode'");
		if($query) :
			$response["error"] = FALSE;
			$response["kode"]  = 1;
			$response["status"]= 200;
			$response["msg"]   = "update status oleh Lurah/Kades berhasil dilakukan";
			echo json_encode($response);
		else :
			$response["error"] = TRUE;
			$response["kode"]  = 1;
			$response["status"]= 200;
			$response["msg"]   = "update status oleh Lurah/Kades gagal dilakukan";
			echo json_encode($response);
		endif;
	elseif($accesId == "reject") :
		$kode  = $_GET["from"];
		$rows  = array();
		$query = $connect->query("SELECT * FROM sir_permintaan WHERE PM_RT = '$kode' AND PER_STATUS = 'REJECT' ORDER BY PER_CREATED_AT DESC");
		while($row  = $query->fetch_assoc()) :
			$rows[] = $row;
		endwhile;

		echo json_encode($rows);
	elseif($accesId == "updatereject") :
		$userId = $_GET["userId"];
		$kode   = $_GET["kode"];

		if(isset($_POST["keterangan"])) :
			$note  = $_POST["keterangan"];

			$query = $connect->query("UPDATE sir_permintaan SET PER_STATUS = 'REJECT', PER_KETERANGAN = '$note', PER_ACC_RT = '$userId' WHERE PER_ID = '$kode'");

			if($query) :
				$response["error"] = FALSE;
				$response["kode"]  = 1;
				$response["status"]= 200;
				$response["msg"]   = "reject status berhasil dilakukan";
				echo json_encode($response);
			else :
				$response["error"] = TRUE;
				$response["kode"]  = 1;
				$response["status"]= 200;
				$response["msg"]   = "reject status gagal dilakukan";
				echo json_encode($response);
			endif;
		else :
			$response["error"] = TRUE;
			$response["kode"]  = 2;
			$response["status"]= 200;
			$response["msg"]   = "Parameter kurang";
			echo json_encode($response);
		endif;
	elseif($accesId == "listsurat") :
		$rows  = array();
		$query = $connect->query("SELECT * FROM sir_surat ORDER BY S_URUTAN ASC");
		while($query = $query->fetch_assoc()) :
			$rows[]  = $row;
		endwhile;

		echo json_encode($response);
	else :
		$response["error"] = TRUE;
		$response["kode"]  = 0;
		$response["status"]= 404;
		$response["msg"]   = "Parameter Acces anda tidak ditemukan";
		echo json_encode($response);
	endif;
else :
	$response["error"] = TRUE;
	$response["kode"]  = 3;
	$response["status"]= 403;
	$response["msg"]   = "Parameter Acces Invalid";
	echo json_encode($response);
endif;
?>