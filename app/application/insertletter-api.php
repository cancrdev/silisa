<?php
error_reporting(0);
header("Content-type: application/json");
include_once 'config.php';


$connect = new ConnectionApi();

if(isset($_GET["acces"])) :
	$accesId = $_GET["acces"];
	 $c = array('','I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII');
	if($_SERVER['REQUEST_METHOD'] == 'POST') :
		//parameter
		$suratId  	= $connect->clean_post($_POST["suratId"]);
		$keperluan	= $connect->clean_post($_POST["keperluan"]);
		$nik		= $connect->clean_post($_POST["nik"]);
		$no_kk      = $connect->clean_post($_POST["no_kk"]);
		$nama       = $connect->clean_post($_POST["nama"]);
		$rt 		= $connect->clean_post($_POST["rt"]);
		$rw     	= $connect->clean_post($_POST["rw"]);
		$tanggal    = date("Y-m-d");
		// list proses param surat
		/* PIHAK KEDUA */
		$pkd_nik    = $connect->clean_post($_POST["pkd_nik"]);
		$pkd_nama   = $connect->clean_post($_POST["pkd_nama"]);
		$pkd_rt     = $connect->clean_post($_POST["pkd_rt"]);
		$pkd_rw     = $connect->clean_post($_POST["pkd_rw"]);
		$pkd_jk     = $connect->clean_post($_POST["pkd_jk"]);
		$pkd_lurah  = $connect->clean_post($_POST["pkd_lurah"]);
		/* HEWAN */
		$jenis_hewan= $connect->clean_post($_POST["jenis_hewan"]);
		$jk_hewan   = $connect->clean_post($_POST["jk_hewan"]);
		$warna      = $connect->clean_post($_POST["warna"]);
		$tanduk     = $connect->clean_post($_POST["tanduk"]);
		$nopol		= $connect->clean_post($_POST["nopol"]);
		/* USAHA */
		$jenis_usaha= $connect->clean_post($_POST["jenis_usaha"]);
	 $lokasi_usaha  = $connect->clean_post($_POST["lokasi_usaha"]);

	    /* HIBAH, JUAL BELI */
	    $tgl_proses = $connect->clean_post($_POST["tgl_proses"]);
	    $sph_alamat = $connect->clean_post($_POST["sph_alamat"]);
	    $sph_rt     = $connect->clean_post($_POST["sph_rt"]);
	    $sph_rw     = $connect->clean_post($_POST["sph_rw"]);
	    $sph_lurah  = $connect->clean_post($_POST["sph_lurah"]);
	    $sph_camat	= $connect->clean_post($_POST["sph_camat"]);
	    $sph_sppt   = $connect->clean_post($_POST["sph_sppt"]);
	    $sph_luas   = $connect->clean_post($_POST["sph_luas"]);
	    $sph_kegiatan = $connect->clean_post($_POST["sph_kegiatan"]);

	    /* JUAL BELI, HEWAN */
	    $jual_harga = $connect->clean_post($_POST["jual_harga"]);
	    $jual_utara = $connect->clean_post($_POST["jual_utara"]);
	    $jual_sltn  = $connect->clean_post($_POST["jual_sltn"]);
	    $jual_barat = $connect->clean_post($_POST["jual_barat"]);
	    $jual_timur = $connect->clean_post($_POST["jual_timur"]);
	    $saksi1     = $connect->clean_post($_POST["saksi1"]);
	    $saksi2     = $connect->clean_post($_POST["saksi2"]);
	    $saksi3     = $connect->clean_post($_POST["saksi3"]);
	    $tgl_merriage= $connect->clean_post($_POST["tgl_merriage"]);

		//end parameter

		//NO SURAT
		$sql = $connect->query("SELECT TS_KEY FROM ts_settings WHERE TS_BIGID = '2'");
		$row = $sql->fetch_assoc();
		$urut= intval($row["TS_KEY"])+1;
		$jml = $connect->query("SELECT COUNT(PER_ID) jml FROM sir_permintaan");
		$kode = $jml->fetch_assoc();
		$lab = intval($lab["jml"])+1;

		$no  = $urut.'/0'.$lab.'/'.$c[date('n')].'/'.date('Y');

		if($accesId == "surat-keterangan") :
			$query = $connect->query("INSERT INTO sir_permintaan (S_ID, NO_SURAT, PER_LAMPIRAN, PER_KEPERLUAN, PER_STATUS, NIK, PER_KK, PER_NAMA_WARGA, PM_RT, PM_RW, PER_TANGGAL) VALUES ('$suratId', '$no', '2', '$keperluan', 'WAITING_APPROVAL_RT', '$nik', '$no_kk', '$nama', '$rt', '$rw', '$tanggal')");
			if($query) :
				$max  = $connect->query("SELECT MAX(PER_ID) maxId FROM sir_permintaan");
				$rows = $max->fetch_assoc();
				$maxId= $rows["maxId"];

				//update
				if($keperluan == "KURANG MAMPU")
					$up   = $connect->query("UPDATE sir_permintaan SET PER_TYPE = 'kurang_mampu' WHERE PER_ID = '$maxId'");
				elseif($keperluan == "KETERANGAN KTP") :
					$up   = $connect->query("UPDATE sir_permintaan SET PER_TYPE = 'keterangan_ktp' WHERE PER_ID = '$maxId'");
				elseif($keperluan == "KETERANGAN KK") :
					$up   = $connect->query("UPDATE sir_permintaan SET PER_TYPE = 'keterangan_kk' WHERE PER_ID = '$maxId'");
				endif;
				$response["error"] = TRUE;
				$response["kode"]  = 1;
				$response["status"]= 200;
				$response["msg"]   = "Permintaan surat berhasil diinput";
			else :
				$response["error"] = TRUE;
				$response["kode"]  = 2;
				$response["status"]= 200;
				$response["msg"]   = "Permintaan surat gagal diinput";
				echo json_encode($response);
			endif;
		elseif($accesId == "hibah-jual") :
			$query = $connect->query("INSERT INTO sir_permintaan (S_ID, NO_SURAT, PER_LAMPIRAN, PER_KEPERLUAN, 	PER_STATUS, PER_TYPE, NIK, PER_KK, PER_NAMA_WARGA, ,PKD_NIK, PKD_NAMA, PKD_RT, 	PKD_RW, PKD_JK, PKD_KELURAHAN, 	SPH_SPJL_TANGGAL, SPH_SPJL_ALAMAT, SPH_SPJL_RT, SPH_SPJL_RW, SPH_SPJL_KELURAHAN, SPH_SPJL_KECAMATAN, SPH_SPJL_SPPT, SPH_SPJL_LUAS, 	SPH_KEGIATAN, 	SPJL_SKHW_HARGA, SPH_SPJL_BUTARA, SPH_SPJL_BSELATAN, SPH_SPJL_BTIMUR, 	SPH_SPJL_BBARAT, 	SPH_SPJL_SPBN_SAKSI1, 	SPH_SPJL_SPBN_SAKSI2) VALUES('$suratId', '$no', '2', 'Jual Beli ', 'WAITING_APPROVAL_RT', 'jual_beli', '$nik', '$no_kk', '$nama', '$pkd_nik', '$pkd_nama', '$pkd_rt', '$pkd_rw', '$pkd_jk', '$pkd_lurah', '')");
		elseif($accesId == "usaha") :
		elseif($accesId == "hewan") :
		else :
			$response["error"] = TRUE;
			$response["kode"]  = 0;
			$response["status"]= 404;
			$response["msg"]   = "Parameter Acces anda tidak ditemukan";
			echo json_encode($response);
		endif;
	else :
		$response["error"] = TRUE;
		$response["kode"]  = 1;
		$response["status"]= 200;
		$response["msg"]   = "Request tidak valid";
		echo json_encode($response);
	endif;
else :
	$response["error"] = TRUE;
	$response["kode"]  = 3;
	$response["status"]= 403;
	$response["msg"]   = "Parameter Acces Invalid";
	echo json_encode($response);
endif;