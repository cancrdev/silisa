<?php
error_reporting(0);
header("Content-type: application/json");
include_once 'config.php';

$connect = new ConnectionApi();


if(isset($_GET["acces"])) :
	$accesId = $_GET["acces"];
	if($accesId == "waitingrt") :
		$kode  = $_GET["kode"];
		$rows  = array();
		$query = $connect->query("SELECT * FROM sir_pengaduan a INNER JOIN sir_warga b ON a.SP_NIK = b.W_NIK WHERE a.SP_RTID = '$kode' AND a.SP_STATUS = 'WAITING_APPROVAL_RT' ORDER BY a.SP_CREATED_AT DESC");
		while($row  = $query->fetch_assoc()) :
			$rows[] = $row;
		endwhile;

		echo json_encode($response);
	elseif($accesId == "updatert") :
		$userId = $_GET["userId"];
		$kode   = $_GET["kode"];

		$query  = $connect->query("UPDATE sir_pengaduan SET SP_STATUS = 'SELESAI', SP_ACC_RT = '$userId' WHERE SP_BIGID = '$kode'");
		if($query) :
			$response["error"] = FALSE;
			$response["kode"]  = 1;
			$response["status"]= 200;
			$response["msg"]   = "update status oleh rt berhasil dilakukan";
			echo json_encode($response);
		else :
			$response["error"] = TRUE;
			$response["kode"]  = 1;
			$response["status"]= 200;
			$response["msg"]   = "update status oleh rt gagal dilakukan";
			echo json_encode($response);
		endif;
	elseif($accesId == "waitingrw") :
		$kode  = $_GET["kode"];
		$rows  = array();
		$query = $connect->query("SELECT * FROM sir_pengaduan a INNER JOIN sir_warga b ON a.SP_NIK = b.W_NIK WHERE a.SP_RWID = '$kode' AND a.SP_STATUS = 'WAITING_APPROVAL_RW' ORDER BY a.SP_CREATED_AT DESC");
		while($row  = $query->fetch_assoc()) :
			$rows[] = $row;
		endwhile;

		echo json_encode($response);
	elseif($accesId == "updaterw") :
		$userId = $_GET["userId"];
		$kode   = $_GET["kode"];

		$query  = $connect->query("UPDATE sir_pengaduan SET SP_STATUS = 'SELESAI', SP_ACC_RW = '$userId' WHERE SP_BIGID = '$kode'");
		if($query) :
			$response["error"] = FALSE;
			$response["kode"]  = 1;
			$response["status"]= 200;
			$response["msg"]   = "update status oleh rt berhasil dilakukan";
			echo json_encode($response);
		else :
			$response["error"] = TRUE;
			$response["kode"]  = 1;
			$response["status"]= 200;
			$response["msg"]   = "update status oleh rt gagal dilakukan";
			echo json_encode($response);
		endif;
	elseif($accesId == "waitinglurah") :
		$kode  = $_GET["kode"];
		$rows  = array();
		$query = $connect->query("SELECT * FROM sir_pengaduan a INNER JOIN sir_warga b ON a.SP_NIK = b.W_NIK WHERE a.SP_STATUS = 'WAITING_KADES' ORDER BY a.SP_CREATED_AT DESC");
		while($row  = $query->fetch_assoc()) :
			$rows[] = $row;
		endwhile;

		echo json_encode($response);
	elseif($accesId == "updatelurah") :
		$kode   = $_GET["kode"];

		$query  = $connect->query("UPDATE sir_pengaduan SET SP_STATUS = 'SELESAI' WHERE SP_BIGID = '$kode'");
		if($query) :
			$response["error"] = FALSE;
			$response["kode"]  = 1;
			$response["status"]= 200;
			$response["msg"]   = "update status oleh kades berhasil dilakukan";
			echo json_encode($response);
		else :
			$response["error"] = TRUE;
			$response["kode"]  = 1;
			$response["status"]= 200;
			$response["msg"]   = "update status oleh kades gagal dilakukan";
			echo json_encode($response);
		endif;
	elseif($accesId == "unablert") :
		$kode   = $_GET["kode"];

		if(isset($_POST["keterangan"])) :
			$note  = $connect->clean_post($_POST["keterangan"]);

			$query = $connect->query("UPDATE sir_pengaduan SET SP_KETERANGAN_CANCEL = '$note', SP_STATUS = 'WAITING_APPROVAL_RW' WHERE SP_BIGID = '$kode'");
			if($query) :
				$response["error"] = FALSE;
				$response["kode"]  = 1;
				$response["status"]= 200;
				$response["msg"]   = "update status berhasil dilakukan";
				echo json_encode($response);
			else :
				$response["error"] = TRUE;
				$response["kode"]  = 1;
				$response["status"]= 200;
				$response["msg"]   = "update status gagal dilakukan";
				echo json_encode($response);
			endif;
		else :
			$response["error"] = TRUE;
			$response["kode"]  = 2;
			$response["status"]= 200;
			$response["msg"]   = "Parameter Kurang";
			echo json_encode($response);
		endif;
	elseif($accesId == "unablerw") :
		$kode   = $_GET["kode"];

		if(isset($_POST["keterangan"])) :
			$note  = $connect->clean_post($_POST["keterangan"]);

			$query = $connect->query("UPDATE sir_pengaduan SET SP_KETERANGAN_CANCEL = '$note', SP_STATUS = 'WAITING_KADES' WHERE SP_BIGID = '$kode'");
			if($query) :
				$response["error"] = FALSE;
				$response["kode"]  = 1;
				$response["status"]= 200;
				$response["msg"]   = "update status berhasil dilakukan";
				echo json_encode($response);
			else :
				$response["error"] = TRUE;
				$response["kode"]  = 1;
				$response["status"]= 200;
				$response["msg"]   = "update status gagal dilakukan";
				echo json_encode($response);
			endif;
		else :
			$response["error"] = TRUE;
			$response["kode"]  = 2;
			$response["status"]= 200;
			$response["msg"]   = "Parameter Kurang";
			echo json_encode($response);
		endif;
	elseif($accesId == "tambahaduan") :
		$userId = $_GET["userId"];
		
		if(isset($_POST["nik"]) || isset($_POST["nama"]) || isset($_POST["rtId"]) || isset($_POST["rwId"]) || isset($_POST["subject"])) :
			$nik    	= $connect->clean_all($_POST["nik"]);
			$nama		= $connect->clean_post($_POST["nama"]);
			$rtId   	= $connect->clean_all($_POST["rtId"]);
			$rwId   	= $connect->clean_all($_POST["rwId"]);
			$subject	= $connect->clean_post($_POST["subject"]);
			$note       = $connect->clean_post($_POST["keterangan"]);
			$image      = $connect->clean_post($_POST["iamge"]);
			$date       = date("Y-m-d");

			// get nomer
			$qq  		= $connect->query("SELECT COUNT(SP_BIGID) AS jml FROM sir_pengaduan");
			$urut 		= $qq->fetch_assoc();
			$no         = intval($urut["jml"])+1;
			$kode       = "ADN-".date('YdHi')."-0".$no;

			$query      = $connect->query("INSERT INTO sir_pengaduan (SP_NOMER, SP_USERID, SP_NIK, SP_NAMA_WARGA, SP_RTID, SP_RWID, SP_SUBJECT, SP_KETERANGAN, SP_IMAGE, SP_STATUS, SP_NOTIF, SP_TANGGAL) VALUES ('$kode', '$userId', '$nik', '$nama', '$rtId', '$rwId', '$subject', '$note', '$image', 'WAITING_APPROVAL_RT', '1', '$date')");

			if($query) :
				$response["error"] = FALSE;
				$response["kode"]  = 1;
				$response["status"]= 200;
				$response["msg"]   = "Pengaduan anda berhasil disimpan";
				echo json_encode($response);
			else :
				$response["error"] = TRUE;
				$response["kode"]  = 1;
				$response["status"]= 200;
				$response["msg"]   = "pengaduan anda gagal disimpan";
				echo json_encode($response);
			endif;
		else :
			$response["error"] = TRUE;
			$response["kode"]  = 1;
			$response["status"]= 200;
			$response["msg"]   = "Parameter kurang";
			echo json_encode($response);
		endif;
	elseif($accesId == "upload") :
		$name    = "image21";
		$target_dir = "../../assets/user/aduan/";
		$image      = $_FILES["image"]["name"];
		$newimage = str_replace(" ", "", basename($name))."_".date('dmYHis')."_".str_replace(" ", "", basename($image));
		$tar_img	= $target_dir . $newimage;
		$upload1 = move_uploaded_file($_FILES["image"]["tmp_name"], $tar_img);
		if($upload1) :
			$response['error'] = FALSE;
			$response['status'] = 200;
			$response['msg'] = 'Berhail Upload Image';
			$response['image_name'] = $newimage;
			echo(json_encode($response));
		exit();
		else :
			$response['error'] = TRUE;
			$response['status'] = 200;
			$response['msg'] = 'gagal Upload Image';
			echo(json_encode($response));
		endif;
	else :
		$response["error"] = TRUE;
		$response["kode"]  = 0;
		$response["status"]= 404;
		$response["msg"]   = "Parameter Acces anda tidak ditemukan";
		echo json_encode($response);
	endif;
else :
	$response["error"] = TRUE;
	$response["kode"]  = 3;
	$response["status"]= 403;
	$response["msg"]   = "Parameter Acces Invalid";
	echo json_encode($response);
endif;