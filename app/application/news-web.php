<?php
// error_reporting(0);
include_once 'config.php';
include_once '../../controller/globalController.php';
$connect = new ConnectionApi();
$log     = new globalController();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="">
    <title>List Berita</title>
    <!-- Bagian css -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">  
    <style type="text/css">
        .pemisah-btnnav{
            margin-left: 20px;
            margin-right: 20px;
        }

        .recent{
            padding-top:20px;
            
        }
        .info-meta{
            padding-top: 10px;
            color:#9999;
        }
        a:focus, 
        a:hover {
        text-decoration: none;
        outline: none;
        color: #9c9c9c;
        }
        .footer-bottom {
        background-color:#3b5998;
        color:#fff;
        padding-top:10px;
        padding-bottom:10px;
        }

    </style>
</head>
<body>    
<div class="container" style="margin-top:40px">
    <?php if(isset($_GET["acces"]) == "list") : 
    $page   = $_GET['page'];
    $start    = $page > 1 ? ($page * 10) - 10 : 0;
    $news  = $connect->query("SELECT * FROM sir_news ORDER BY N_ID DESC LIMIT 3 OFFSET ".$page);
    ?>
    <div class="row">
        <div class="col-md-12">
              <div class="panel panel-default">
               <div class="panel-body">
                  <?php while($row = $news->fetch_assoc()) : 
                  $pecah    = substr($row["N_SUBJECT"], 0, 450);
                  $lin      = $row["N_ID"];
                  ?>
                  <h4><?= $row["N_TITLE"]; ?></h4>
                  <p>Dipublikasi <?= $log->TanggalIndo($row["N_TANGGAL"]).' / '. $row["N_CREATED_BY"]; ?></p><br>
                   <img class = "media-object " src = "../../assets/news/<?= $row["N_IMAGE"]; ?>" width="100%" height="450px" >                 
                    <div class = "media">
                       <div class = "media-body">
                          <p><?= $pecah; ?></p> 
                         <p style="text-align:right;">
                            <a href="newsdetail-web?acces=detail&key=<?= $lin; ?>">
                                <button class="btn btn-primary">Baca Selengkapnya</button>
                            </a>
                        </p>
                       </div>
                    </div>
                    <hr>
                  <?php endwhile; ?>
               </div>
            </div>
         </div>         
    </div>          
    <?php endif; ?>
</div>
<!--FOOTER-->            
   <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>