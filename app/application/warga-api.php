<?php
error_reporting(0);
header("Content-type: application/json");
include_once 'config.php';

$connect = new ConnectionApi();

if(isset($_GET["acces"])) :
	$accesId = $_GET["acces"];
	if($accesId == "tambah") :
		$kode  = $_GET["kode"];
		$rw    = $_GET["rw"];

		if(isset($_POST["fullname"]) || isset($_POST["nik"]) || isset($_POST["no_kk"]) || isset($_POST["username"]) || isset($_POST["mobile"]) || isset($_POST["email"])) :
			 $nik  = $connect->clean_all($_POST["nik"]);
			 $kk   = $connect->clean_all($_POST["no_kk"]);
			 // $name = $connect->clean_all($_POST["username"]);
			 $fullname = $connect->clean_post($_POST["fullname"]);
			 $email = $connect->clean_all($_POST["email"]);
			 $mobile = $connect->clean_all($_POST["mobile"]);
			 $tempat = $connect->clean_all($_POST["tempat_lahir"]);
			 $tglLahir = date(strtotime('Y-m-d', $_POST["tglLahir"]));
			 $jk   = $connect->clean_all($_POST["jenis_kelamin"]);
			 $alamat = $connect->clean_post($_POST["alamat"]);
			 $kelurahan = $connect->clean_post($_POST["kelurahan"]);
			 $kecamatan = $connect->clean_post($_POST["kecamatan"]);
			 $kab = $connect->clean_post($_POST["kabupaten"]);
			 $prov = $connect->clean_post($_POST["provinsi"]);
			 $agama = $connect->clean_post($_POST["agama"]);
			 $status = $connect->clean_post($_POST["status_nikah"]);
			 $status_kk = $connect->clean_post($_POST["status_kk"]);
			 $study = $connect->clean_post($_POST["study"]);
			 $work = $connect->clean_post($_POST["kerjaan"]);
			 $wni  = "WNI";
			 $password = md5(md5(12345678));

			 $query = $connect->query("INSERT INTO sir_user (U_NIK, U_NAME, U_FULLNAME, U_EMAIL, U_PASSWORD, U_MOBILE, U_GROUP_RULE, U_STATUS) VALUES ('$nik', '$nik', '$fullname', '$email', '$password', '$mobile', 'U_WARGA', '1')");
			 $sql   = $connect->query("INSERT INTO sir_warga (W_NIK, W_KK, W_NAMA, W_TMP_LAHIR, W_TGL_LAHIR, W_JK, W_ALAMAT, W_RT, W_RW, W_KELURAHAN, W_KECAMATAN, W_KABUPATEN, W_PROVINSI, W_AGAMA, W_STATUS, W_STATUS_KK, W_PENDIDIKAN, W_PEKERJAAN, W_STATUS_PENDUDUK, W_CREATED_BY) VALUES ('$nik', '$kk', '$fullname', '$tempat', '$tglLahir', '$jk', '$alamat', '$kode', '$rw', '$kelurahan', '$kecamatan', '$kab', '$prov', '$agama', '$status', '$status_kk', '$study', '$work', '$wni', 'RT')");

			 if($query && $sql) :
				$response["error"] = FALSE;
				$response["kode"]  = 1;
				$response["status"]= 200;
				$response["msg"]   = "tambah warga berhasil";
				echo json_encode($response);
			else :
				$response["error"] = TRUE;
				$response["kode"]  = 1;
				$response["status"]= 200;
				$response["msg"]   = "tambah warga gagal";
				echo json_encode($response);
			endif;
		else :
			$response["error"] = TRUE;
			$response["kode"]  = 2;
			$response["status"]= 200;
			$response["msg"]   = "Parameter Kurang";
			echo json_encode($response);
		endif;
	elseif($accesId == "listdata") :
		$kode  = $_GET["kode"];
		$page		= $_GET['page'];
		$start		= $page > 1 ? ($page * 10) - 10 : 0;

		$rows  = array();
		$query = $connect->query("SELECT * FROM sir_warga WHERE W_RT = '$kode' LIMIT 10 OFFSET ".$start);
		while($row = $query->fetch_assoc()) :
			$rows[]= $row;
		endwhile;

		if($rows == null || $rows == "") :
			$response["error"] = TRUE;
			$response["kode"]  = 1;
			$response["status"]= 200;
			$response["msg"]   = "Data Kosong";
			echo json_encode($response);
		else :
			echo json_encode($rows);
		endif;
	else :
		$response["error"] = TRUE;
		$response["kode"]  = 0;
		$response["status"]= 404;
		$response["msg"]   = "Parameter Acces anda tidak ditemukan";
		echo json_encode($response);
	endif; 
else :
	$response["error"] = TRUE;
	$response["kode"]  = 3;
	$response["status"]= 403;
	$response["msg"]   = "Parameter Acces Invalid";
	echo json_encode($response);
endif;