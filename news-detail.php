<?php
session_start();
error_reporting(0);
include_once 'controller/globalController.php';
include_once 'controller/news-vistorController.php';

$log    = new globalController();
$berita1 = new NewsVisitorController();

$kodeId1 = $berita1->detailNews($_GET["key"]);
$comm    = $berita1->getComments($_GET["key"]);
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="description" content="">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <!-- Title -->
  <title><?= $log->name_app(); ?> | News Details</title>
  <!-- Favicon -->
  <link rel="icon" href="<?= $log->base_url(); ?>assets/visitor/img/core-img/favicon.ico">
  <!-- Core Stylesheet -->
  <link rel="stylesheet" href="<?= $log->base_url(); ?>assets/visitor/style.css">
</head>

<body>
  <!-- Preloader -->
  <div class="preloader d-flex align-items-center justify-content-center">
    <div class="spinner"></div>
  </div>

  <!-- ##### Header Area Start ##### -->
  <?php include_once 'layouts/visitor/navbar.php'; ?>
  <!-- ##### Header Area End ##### -->

  <!-- ##### Breadcrumb Area Start ##### -->
  <div class="breadcrumb-area bg-img bg-overlay jarallax" style="background-image: url('<?= $log->base_url(); ?>assets/visitor/img/bg-img/18.jpg');">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-12">
          <div class="breadcrumb-text">
            <h2>Detail Berita</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php if(!empty($_GET["key"])) { ?>
  <div class="famie-breadcrumb">
    <div class="container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Detail Berita <?php echo $kodeId1["N_TITLE"]; ?></li>
        </ol>
      </nav>
    </div>
  </div>
  <!-- ##### Breadcrumb Area End ##### -->

  <!-- ##### News Details Area Start ##### -->
  <section class="news-details-area section-padding-0-100">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <!-- News Details Content -->
          <div class="news-details-content">
            <h6>Post on <a href="#" class="post-date"><?php echo $log->TanggalIndo($kodeId1["N_TANGGAL"]); ?></a> / <a href="#" class="post-author"><?php echo $kodeId1["N_CREATED_BY"]; ?></a></h6>
            <h2 class="post-title"><?php echo $kodeId1["N_TITLE"]; ?></h2>

            <p><?php echo $kodeId1["N_SUBJECT"]; ?></p>

            <!-- Share & Tags -->
            <div class="share-tags d-flex flex-wrap align-items-center justify-content-between">
              <!-- Share -->
              <!-- Tags -->
            </div>
          </div>

          <!-- Post Author -->
          <!-- Comment Area Start -->
          <div class="comment_area clearfix mb-50">

            <!-- Title -->
            <h3 class="mb-50"><?php echo count($comm)." Komentar"; ?></h3>

            <ul>
              <!-- Single Comment Area -->
              <?php foreach($comm as $cm) : ?>
              <li class="single_comment_area">
                <!-- Comment Content -->
                <div class="comment-content d-flex">
                  <!-- Comment Author -->
                  <div class="comment-author">
                    <img src="<?= $log->base_url(); ?>assets/visitor/img/bg-img/22.jpg" alt="author">
                  </div>
                  <!-- Comment Meta -->
                  <div class="comment-meta">
                    <a href="#" class="comment-date"><?php echo $log->TanggalIndo($cm["SC_CREATED_AT"]); ?></a>
                    <h6><?php echo $cm["SC_FULLNAME"]; ?></h6>
                    <p><?php echo $cm["SC_SUBJECT"]; ?></p>
                    <!-- <a href="#" class="reply">Reply</a> -->
                  </div>
                </div>
              </li>
              <?php endforeach; ?>
            </ul>
          </div>

          <!-- Post A Comment Area -->
          <div class="post-a-comment-area">

            <!-- Title -->
            <h3 class="mb-50">Komentari Berita</h3>

            <!-- Reply Form -->
            <div class="contact-form-area">
              <?php 
              if(isset($_POST["save"])) {
               $koment["newsId"]   = $_POST["newsId"];
               $koment["fullname"] = $_POST["fullname"];
               $koment["email"]    = $_POST["email"];
               $koment["subject"]  = $_POST["subject"];
               $ctrl = $berita1->add_koment($koment);
              }
              ?>
              <form action="#" method="post">
              	<input type="hidden" name="newsId" value="<?php echo $_GET["key"]; ?>">
                <div class="row">
                  <div class="col-12 col-lg-6">
                    <input type="text" class="form-control" name="fullname" required id="name" placeholder="Your Name*">
                  </div>
                  <div class="col-12 col-lg-6">
                    <input type="email" class="form-control" name="email" id="email" placeholder="Your Email*">
                  </div>
                  <div class="col-12">
                    <textarea name="subject" class="form-control" id="message" placeholder="Message*"></textarea>
                  </div>
                  <div class="col-12">
                    <button class="btn famie-btn mt-30" name="save" type="submit">Submit Comment</button>
                  </div>
                </div>
              </form>
            </div>
          </div>

        </div>
      </div>
    </div>
  </section>
  <?php } else { ?>
  	<div class="famie-breadcrumb">
    <div class="container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Detail Berita Tidak Ditemukan</li>
        </ol>
      </nav>
    </div>
  </div>
  <!-- ##### Breadcrumb Area End ##### -->

  <!-- ##### News Details Area Start ##### -->
  <section class="news-details-area section-padding-0-100">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <!-- News Details Content -->
          <div class="news-details-content">
            <h4 class="text-center">Data Tidak ditemukan</h4>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php } ?>
  <!-- ##### News Details Area End ##### -->

  <!-- ##### Footer Area Start ##### -->
  <?php include_once 'layouts/visitor/footer.php'; ?>
  <!-- ##### Footer Area End ##### -->

  <!-- ##### All Javascript Files ##### -->
  <!-- jquery 2.2.4  -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jquery.min.js"></script>
  <!-- Popper js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/popper.min.js"></script>
  <!-- Bootstrap js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/bootstrap.min.js"></script>
  <!-- Owl Carousel js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/owl.carousel.min.js"></script>
  <!-- Classynav -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/classynav.js"></script>
  <!-- Wow js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/wow.min.js"></script>
  <!-- Sticky js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jquery.sticky.js"></script>
  <!-- Magnific Popup js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jquery.magnific-popup.min.js"></script>
  <!-- Scrollup js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jquery.scrollup.min.js"></script>
  <!-- Jarallax js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jarallax.min.js"></script>
  <!-- Jarallax Video js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jarallax-video.min.js"></script>
  <!-- Active js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/active.js"></script>
</body>

</html>