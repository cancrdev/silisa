<?php
include_once '../../model/config.php';

/**
* 
*/
class SliderController extends Connection
{
	
	function __construct()
	{
		# code...
		date_default_timezone_set("Asia/Jakarta");
	}

	function fetchSlider() {
		$rows  = array();
		$query = $this->query("SELECT * FROM  sir_slider ORDER BY SS_CREATED_AT DESC");
		while($row  = $query->fetch_assoc()) {
			$rows[] = $row; 
		}

		return $rows;
	}

	function addSlider($slider) {
		$judul  = $this->clean_post($slider["judul"]);
		$note   = $this->clean_post($slider["note"]);
		$action = $this->clean_post($slider["action"]);
		$link   = $this->clean_post($slider["link"]);
		$image  = $_FILES["image"]["name"];
		$name    = "image21";
		$target_dir = "../../assets/slider/";

		$newimage = str_replace(" ", "", basename($name))."_".date('dmYHis')."_".str_replace(" ", "", basename($image));
		$tar_img  = $target_dir . $newimage;
		$upload1  = move_uploaded_file($_FILES["image"]["tmp_name"], $tar_img);

		$query    = $this->query("INSERT INTO sir_slider (SS_JUDUL, SS_KETERANGAN, 	SS_IMAGE, SS_ACTION, SS_LINK) VALUES ('$judul', '$note', '$newimage', '$action', '$link')");

		if($query && $upload1) {
		echo "<script>alert('Slider Added Succesfully')
            location.replace('../slider')</script>";
        } else {
        	echo "<script>alert('Slider Add Failed')</script>";
        }
	}

	function editByOne($id) {
		$query = $this->query("SELECT * FROM sir_slider WHERE SS_BIGID = '$id'");
		$row   = $query->fetch_assoc();
		$kodeId= $row["SS_BIGID"];

		if($id == $kodeId) {
			return $row;
		} else {
			header("location: 404");
		}
	}

	function updateByOne($update) {
		$id     = $this->clean_all($update["bigId"]);
		$judul  = $this->clean_post($update["judul"]);
		$note   = $this->clean_post($update["note"]);
		$action = $this->clean_post($update["action"]);
		$link   = $this->clean_post($update["link"]);
		$image  = $_FILES["image"]["name"];
		$name    = "image21";
		$target_dir = "../../assets/slider/";

		if($image == "" || $image == null) {
			$query    = $this->query("UPDATE sir_slider SET SS_JUDUL = '$judul', SS_KETERANGAN = '$note', SS_ACTION = '$action', SS_LINK = '$link' WHERE SS_BIGID = '$id'");

			if($query) {
				echo "<script>alert('Slider Update Succesfully')
                location.replace('../slider')</script>";
            } else {
            	echo "<script>alert('Slider Update Failed')</script>";
            }
		} else {
			$newimage = str_replace(" ", "", basename($name))."_".date('dmYHis')."_".str_replace(" ", "", basename($image));
			$tar_img  = $target_dir . $newimage;
			$upload1  = move_uploaded_file($_FILES["image"]["tmp_name"], $tar_img);

			$query    = $this->query("UPDATE sir_slider SET SS_JUDUL = '$judul', SS_KETERANGAN = '$note', SS_IMAGE = '$image', SS_ACTION = '$action', SS_LINK = '$link' WHERE SS_BIGID = '$id'");

			if($query && $upload1) {
			echo "<script>alert('Slider Update Succesfully')
                location.replace('../slider')</script>";
            } else {
            	echo "<script>alert('Slider Update Failed')</script>";
            }
		}
	}

	function deleteSlider($id) {
		$query = $this->query("DELETE FROM sir_slider WHERE SS_BIGID = '$id'");

		if($query) {
			echo "<script>alert('Slider Delete Succesfully')
            location.replace('../slider')</script>";
        } else {
        	echo "<script>alert('Slider Delete Failed')</script>";
        }
	}


}