<?php

/**
* 
*/
class globalController
{
	
	function __construct()
	{
		date_default_timezone_set("Asia/Jakarta");
	}

	//fungsi global
    function TanggalIndo($date){
		$BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
	 
		$tahun = substr($date, 0, 4);
		$bulan = substr($date, 5, 2);
		$tgl   = substr($date, 8, 2);
	 
		$result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;		
		return($result);
	}

    function diffInMonths(\DateTime $date1, \DateTime $date2)
    {
        $diff =  $date1->diff($date2);

        $months = $diff->y * 12 + $diff->m + $diff->d / 30;

        return (int) round($months);
    }

	function frmDate($date,$code){
        $explode = explode("-",$date);
        $year  = $explode[0];
        $month = (substr($explode[1],0,1)=="0")?str_replace("0","",$explode[1]):$explode[1];
        $dated = $explode[2];
        $explode_time = explode(" ",$dated);
        $dates = $explode_time[0];
        switch($code){
            // Per Object
            case 4: $format = $dates; break;                                                   
            case 5: $format = $month; break;                                                       
            case 6: $format = $year; break;               
        }       
        return $format; 
    }


	function dateRange($start,$end){
        $xdate    = $start;
        $ydate    = $end;
        $xmonth    = $start;
        $ymonth    = $end;
        $xyear    = $start;
        $yyear    = $end;
        // Jika Input tanggal berada ditahun yang sama
        if($xyear==$yyear){
            // Jika Input tanggal berada dibulan yang sama
            if($xmonth==$ymonth){
                $nday=$ydate+1-$xdate;
            } else {
                $r2=NULL;
                $nmonth = $ymonth-$xmonth;           
                $r1 = nmonth($xmonth)-$xdate+1;
                for($i=$xmonth+1;$i<$ymonth;$i++){
                    $r2 = $r2+nmonth($i);
                }
                $r3 = $ydate;
                $nday = $r1+$r2+$r3;
            }
        } else {
            // Jika Input tahun awal berbeda dengan tahun akhir
            $r2=NULL; $r3=NULL;
            $r1=nmonth($xmonth)-$xdate+1;

            for($i=$xmonth+1;$i<13;$i++){
                $r2 = $r2+nmonth($i);
            }
            for($i=1;$i<$ymonth;$i++){
                $r3 = $r3+nmonth($i);
            }
            $r4 = $ydate;
            $nday = $r1+$r2+$r3+$r4;
        }           
        return $nday;
    }

    function base_url() {
        $row = "http://candev.hadi.id/bac/silisa/";

        //$row = "http://192.168.1.12/bac/silisa/"; 

        return $row;
    }

    function name_app() {
        $row  = "BULU LOR";

        return $row;
    }

    function deskripsi() {
        $row = "Selamat Datang di Website Resmi Kelurahan Bulu Lor , Kecamatan Semarang Utara Kota Semarang. Media komunikasi dan transparansi Pemerintah Kelurahan Bulu Lor untuk seluruh masyarakat.";

        return $row;
    }

    function themeBackend() {
        $row = "#3498d4";

        return $row;
    }

    function background() {
        $row = "#0777bb";

        return $row;

    }

    function logo_situs() {
        $row = "http://candev.hadi.id/bac/silisa/assets/logo-semarang.png";

        return $row;
    }

    function loading_apps() {
        $row = "Mohon tunggu,data sedang dipersiapkan";

        return $row;
    }
}

?> 