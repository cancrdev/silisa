<?php
include_once '../model/config.php';
/**
*
*/
class loginController extends Connection
{

	function __construct()
	{
		date_default_timezone_set("Asia/Jakarta");
	}

	function mobileLogin($login) {
		$mail 		= mysqli_escape_string($this->conn(), $login["loginEmail"]);
    	$pass 		= mysqli_escape_string($this->conn(), $login["loginPassword"]);
    	$password 	= md5(md5($pass));
    	$ip = $_SERVER['REMOTE_ADDR'];
		$browser = $_SERVER['HTTP_USER_AGENT'];

		$query  = $this->query("SELECT * FROM sir_user WHERE (U_NAME = '$mail' OR U_NIK = '$mail') AND U_PASSWORD = '$password'");

		if(mysqli_num_rows($query) > 0) {
			$users 	=  $query->fetch_assoc();
			$userId = $users["ID_USER"];
			$now    = $dateNow = date("Y-m-d H:i:s");
			$loginToken = substr(md5($users["U_FULLNAME"].$dateNow), 0, 30);
			$updateData = $this->query("UPDATE sir_user SET U_LOGIN_TOKEN = '$loginToken', U_LOGIN_WAKTU = '$now', U_IP_POSITION = '$ip', U_DEFAULT_BROWSER = '$browser' WHERE ID_USER = '$userId'");
			// rt 
			$rt    = $this->query("SELECT * FROM sir_rt WHERE P_USERID = '$userId'");
			$fet   = $rt->fetch_assoc();
			$_SESSION["P_RT"] = $fet["P_RT"];
			$_SESSION["P_RW"] = $fet["P_RW"];
			$_SESSION["P_GENDER"] = $fet["P_GENDER"];

			// rw
			$rw    = $this->query("SELECT * FROM sir_rw WHERE PW_USERID = '$userId'");
			$fet1   = $rt->fetch_assoc();
			$_SESSION["PW_RW"] = $fet1["PW_RW"];
			$_SESSION["PW_MASA_JABATAN"] = $fet1["PW_MASA_JABATAN"];
			$_SESSION["PW_GENDER"] = $fet1["PW_GENDER"];

			$_SESSION["ID_USER"] = $users["ID_USER"];
			$_SESSION["U_NAME"]  = $users["U_NAME"];
			$_SESSION["U_FULLNAME"] = $users["U_FULLNAME"];
			$_SESSION["U_EMAIL"] = $users["U_EMAIL"];
			$_SESSION["U_KEY"]   = $users["U_KEY"];
			$_SESSION["U_GROUP_RULE"] = $users["U_GROUP_RULE"];

			//proses system
			if($users["U_STATUS"] == 1) {
				//jika rule admin/kades
				if($users["U_GROUP_RULE"] == "U_ADMIN") {
					echo "<script>alert('Selamat datang ".$users["U_FULLNAME"].",anda login sebagai Admin')
                    location.replace('../admin/home')</script>";
                } elseif($users["U_GROUP_RULE"] == "U_KADES") {
                	echo "<script>alert('Selamat datang ".$users["U_FULLNAME"].",anda login sebagai Kades')
                    location.replace('../goverment/home')</script>";
				} elseif($users["U_GROUP_RULE"] == "U_RT") {
					echo "<script>alert('Selamat datang ".$users["U_FULLNAME"].",anda login sebagai Ketua RT')
                    location.replace('../goverment/home')</script>";
				} elseif($users["U_GROUP_RULE"] == "U_RW") {
					echo "<script>alert('Selamat datang ".$users["U_FULLNAME"].",anda login sebagai Ketua RW')
                    location.replace('../goverment/home')</script>";
				} elseif($users["U_GROUP_RULE"] == "U_WARGA") {
					echo "<script>alert('Selamat datang ".$users["U_FULLNAME"].",anda login sebagai Ketua RW')
                    location.replace('../resident/home')</script>";
				}
			} else {
				echo '<div class="alert bg-pink alert-dismissible" role="alert">
                <button typ="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  Status anda tidak aktif,silahkan hubungi administrator untuk info lebih lanjut
          </div>';
        //echo "<script>alert('Status anda tidak aktif,silahkan hubungi administrator untuk info lebih lanjut')</script>";
			}
		} else {
			//echo "<script>alert('Periksa lagi data login anda')</script>";
      echo '<div class="alert bg-pink alert-dismissible" role="alert">
                <button typ="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  Periksa Kembali data login anda
          </div>';
		}
	}

	function mobileLogout() {
		$dateOut = date("Y-m-d H:i:s");
		$email = $_SESSION["ID_USER"];
		$name = $_SESSION["U_FULLNAME"];
		$logout = $this->query("UPDATE sir_user SET U_LOGOUT_WAKTU = '$dateOut' WHERE ID_USER = '$email'");
		session_destroy();
		setcookie('notifLogin','Berhasil Logout',time() + 10);
		if($logout) {
			echo "<script>alert('Anda berhasil logout ".$name."')
			location.replace('../v1')</script>";
		}
	}

	// log data profile

	function my_profile() {
		$sesId = $_SESSION["ID_USER"];
		$query = $this->query("SELECT * FROM sir_user WHERE ID_USER = '$sesId'");
		$row   = $query->fetch_assoc();

		return $row;
	}

	function ubah_profile($profile) {
		$sesId 	   = $_SESSION["ID_USER"];
		$fullname  = $this->clean_post($profile["fullname"]);
		$email     = $this->clean_post($profile["email"]);
		$noTelp    = $this->clean_post($profile["noTelp"]);

		$query     = $this->query("UPDATE sir_user SET U_FULLNAME = '$fullname', U_EMAIL = '$email', U_MOBILE = '$noTelp' WHERE ID_USER = '$sesId'");

		if($query) {
			echo "<script>alert('Profil anda berhasil diubah')
                    location.replace('../../v1/my-profile')</script>";
		} else {
			echo "<script>alert('Profil anda gagal diubah')</script>";
		}
	}

	function ubah_avatar($vatar) {
		$sesId  = $_SESSION["ID_USER"];
		$image  = $_FILES["image"]["name"];
		$name    = "image21";
		$target_dir = "../assets/avatar/";

		$newimage = str_replace(" ", "", basename($name))."_".date('dmYHis')."_".str_replace(" ", "", basename($image));
		$tar_img  = $target_dir . $newimage;
		$upload1  = move_uploaded_file($_FILES["image"]["tmp_name"], $tar_img);

		$query    = $this->query("UPDATE sir_user SET U_AVATAR = '$newimage' WHERE ID_USER = '$sesId'");

		if($query) {
			echo "<script>alert('Profil anda berhasil diubah')
                    location.replace('../v1/my-profile')</script>";
		} else {
			echo "<script>alert('Profil anda gagal diubah')</script>";
		}
	}

	function ubah_password($pass) {
		$sesId   = $_SESSION["ID_USER"];
		$password= $pass["password"];
		$confirm = $pass["confirm"];

		$pswd    = md5(md5($password));

		if($password == $confirm) {
			$query = $this->query("UPDATE sir_user SET 	U_PASSWORD = '$pswd' WHERE ID_USER = '$sesId'");

			if($query) {
				echo "<script>alert('Profil anda berhasil diubah')
                    location.replace('../../v1/my-profile')</script>";
			} else {
				echo "<script>alert('Profil anda gagal diubah')</script>";
			}
		} else {
			echo "<script>alert('password yang anda masukkan tidak sama')</script>";
		}
	}

	function reset_password($resets) {
		require '../assets/PHPMailer/src/Exception.php';
        require '../assets/PHPMailer/src/PHPMailer.php';
        require '../assets/PHPMailer/src/SMTP.php';

        $email = $this->clean_post($resets["email"]);
        $kode  = base64_encode($email.date("YmdHis"));
        
        //get data system
        $sql   = $this->query("SELECT U_EMAIL FROM sir_user WHERE U_EMAIL = '$email'");
        if(mysqli_num_rows($sql) > 0) {
        	$query = $this->query("UPDATE sir_user SET U_FORGOT = '$kode' WHERE U_EMAIL = '$email'");
        	if($query) {
        		 ob_start();
                 include 'mail.php';
                 $page    = ob_get_contents();
                 ob_clean();
                    
				  $mail = new PHPMailer\PHPMailer\PHPMailer();
                  $mail->IsSMTP();
                  try {
                      $mail->SMTPDebug = 0;                                 // Enable verbose debug output
                      $mail->isSMTP();                                      // Set mailer to use SMTP
                      $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
                      $mail->SMTPAuth = true;                               // Enable SMTP authentication
                      $mail->Username = 'crmanagement.official@gmail.com';                 // SMTP username
                      $mail->Password = 'P@ssw0rdadmin';                           // SMTP password
                      $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                      $mail->Port = 587;                                      // TCP port to connect to
                      $mail->SMTPOptions = array(
                          'ssl' => array(
                              'verify_peer' => false,
                              'verify_peer_name' => false,
                              'allow_self_signed' => true
                          )
                      ); 
                    $mail->AddAddress($email);
                     //email tujuan isi dengan emailmu misal test@test.com
                    $message = $page;
                    $mail->SetFrom('crmanagement.official@gmail.com','(noreply)DokterApps'); // email pengirim

                    $mail->Subject = 'Forgot password Success';                       
                    $mail->MsgHTML('<p>'.$message);
                    $mail->Send();{
                       
                    }   
                  } catch (phpmailerException $e) {
                     echo $e->errorMessage(); 
                 } 
			echo "<script>alert('Forgot Berhasil dilakukakn')
                    location.replace('../v1')</script>";
        	} else {
        		echo "<script>alert('Forgot password gagal')</script>";
        	}
        } else {
        	echo "<script>alert('Akun email yang anda masukkan tidak ada yang cocok')</script>";
        }
	}

	function recovery_password($pass) {
		$sesId   = $pass["key"];
		$password= $pass["password"];
		$confirm = $pass["konfirm"];

		if($password == $confirm) {
			$pswd    = md5(md5($password));
			$query = $this->query("UPDATE sir_user SET U_PASSWORD = '$pswd' WHERE U_EMAIL= '$sesId'");

			if($query) {
				echo "<script>alert('Profil anda berhasil diubah')
                    location.replace('../v1')</script>";
			} else {
				echo "<script>alert('Profil anda gagal diubah')</script>";
			}
		} else {
			echo "<script>alert('password yang anda masukkan tidak sama')</script>";
		}
	} 


}


?>
