<?php

include_once 'model/config.php';

/**
* 
*/
class NewsVisitorController extends Connection {
	
	function __construct()
	{
		# code...
		date_default_timezone_set("Asia/Jakarta");
	}

	function limitPage($start, $limit) {
		$rows  = array();
		$query = $this->query("SELECT * FROM sir_news ORDER BY N_CREATED_AT DESC LIMIT $start, $limit");
		while($row = $query->fetch_assoc()) {
			$rows[] = $row;
		}

		return $rows;
	}

	function limitData() {
		$rows  = array();
		$query = $this->query("SELECT * FROM sir_news ORDER BY N_CREATED_AT DESC LIMIT 0,2");
		while($row = $query->fetch_assoc()) {
			$rows[] = $row;
		}

		return $rows;
	}

	function detailNews($id) {
		$kodeId = $this->clean_all($id);
		//get data
		$sql1= $this->query("SELECT * FROM sir_news WHERE N_ID = '$kodeId'");
		$sql = $sql1->fetch_assoc();
		$row = $sql["N_ID"];
        
		if($row == $kodeId) {
			$query = $this->query("SELECT * FROM sir_news WHERE N_ID = '$row'");
			$datas = $query->fetch_assoc();

			return $datas;
		} else {
			header("location: 404");
		}
	}

	function fetchSettingsVisit() {
		$query = $this->query("SELECT * FROM sir_about");
		$row   = $query->fetch_assoc();

		return $row;
	}

	function namaLurah() {
		$query = $this->query("SELECT PL_LURAH, PL_NAMA FROM sir_lurah WHERE PL_STATUS = 1");
		$row   = $query->fetch_assoc();

		return $row;
	}

	function countRT() {
		$query  = $this->query("SELECT COUNT(P_ID) jmlRT FROM sir_rt");
		$row    = $query->fetch_assoc();
		$rows   = $row["jmlRT"];

		return $rows;
	}

	function countRW() {
		$query  = $this->query("SELECT COUNT(PW_ID) jmlRW FROM sir_rw");
		$row    = $query->fetch_assoc();
		$rows   = $row["jmlRW"];

		return $rows;
	}

	function countKeluarga() {
		$query  = $this->query("SELECT COUNT(W_KK) jmlKK FROM sir_warga");
		$row    = $query->fetch_assoc();
		$rows   = $row["jmlKK"];

		return $rows;
	}

	function countLaki() {
		$query  = $this->query("SELECT COUNT(W_KK) jmlLaki FROM sir_warga WHERE W_JK = 'Laki-Laki'");
		$row    = $query->fetch_assoc();
		$rows   = $row["jmlLaki"];

		return $rows;
	}

	function countFemale() {
		$query  = $this->query("SELECT COUNT(W_KK) female FROM sir_warga WHERE W_JK = 'Perempuan'");
		$row    = $query->fetch_assoc();
		$rows   = $row["female"];

		return $rows;
	}

	function allWarga() {
		$rows  = array();
		$query = $this->query("SELECT *, COUNT(W_PEKERJAAN) AS data FROM sir_warga WHERE W_PEKERJAAN NOT IN ('-') GROUP BY W_PEKERJAAN ORDER BY W_CREATED_AT DESC");
		while($row = $query->fetch_assoc()) {
			$rows[] = $row;
		}

		return $rows;
	}

	function allStudy() {
		$rows  = array();
		$query = $this->query("SELECT *, COUNT(W_PENDIDIKAN) AS dataS FROM sir_warga WHERE W_PENDIDIKAN NOT IN ('-') GROUP BY W_PENDIDIKAN ORDER BY W_CREATED_AT DESC");
		while($row = $query->fetch_assoc()) {
			$rows[] = $row;
		}

		return $rows;
	}

	function allGender() {
		$rows  = array();
		$query = $this->query("SELECT *, COUNT(W_JK) AS dataJk FROM sir_warga GROUP BY W_JK ORDER BY W_CREATED_AT DESC");
		while($row = $query->fetch_assoc()) {
			$rows[] = $row;
		}

		return $rows;
	}

	function allMeride() {
		$rows  = array();
		$query = $this->query("SELECT *, COUNT(W_STATUS) AS dataM FROM sir_warga GROUP BY W_STATUS ORDER BY W_CREATED_AT DESC");
		while($row = $query->fetch_assoc()) {
			$rows[] = $row;
		}

		return $rows;
	}

	function getComments($id) {
		$rows  = array();
		$query = $this->query("SELECT * FROM sir_comment WHERE SC_BERITAID = '$id' ORDER BY SC_CREATED_AT DESC");
		while($row = $query->fetch_assoc()) {
			$rows[] = $row;
		}

		return $rows;
	}

	function add_koment($koment) {
		$beritaId = $this->clean_all($koment["newsId"]);
		$fullname = $this->clean_post($koment["fullname"]);
		$email    = $this->clean_post($koment["email"]);
		$subject  = $this->clean_post($koment["subject"]);

		$query    = $this->query("INSERT INTO sir_comment (SC_BERITAID, SC_FULLNAME, SC_EMAIL, 	SC_SUBJECT) VALUES ('$beritaId', '$fullname', '$email', '$subject')");
		if($query) {
			echo "<script>alert('Komentar anda berhasil ditambahkan')
                    location.replace('news-detail?key=$beritaId')</script>";
		} else {
			echo "<script>alert('Komentar anda gagal ditambahkan')</script>";
		}
	}

	function fetchSlider() {
		$rows  = array();
		$query = $this->query("SELECT * FROM  sir_slider WHERE SS_IMAGE IS NOT NULL");
		while($row  = $query->fetch_assoc()) {
			$rows[] = $row; 
		}

		return $rows;
	}

	function subcribeNews($sub) {
		$email  = $this->clean_post($sub["email"]);

		$sql    = $this->query("SELECT * FROM sir_subcribe WHERE SB_EMAIL = '$email'");
		if(mysqli_num_rows($sql) > 0) {
			echo "<script>alert('email yang anda masukkan sudah tersedia')</script>";
		} else {
			$query  = $this->query("INSERT INTO sir_subcribe (SB_EMAIL) VALUES ('$email')");
			if($query) {
				echo "<script>alert('Subcribe anda berhasil ditambahkan')
                    location.replace('../silisa')</script>";
			} else {
				echo "<script>alert('Subcribe anda gagal ditambahkan')</script>";
			}
		}
	}

	function get_gear() {
		$query = $this->query("SELECT * FROM ts_settings WHERE TS_BIGID = '1'");
		$row   = $query->fetch_assoc();

		return $row;
	}

}