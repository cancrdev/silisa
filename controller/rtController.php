<?php
include_once '../../model/config.php';

/**
* 
*/
class RtController extends Connection
{
	
	function __construct()
	{
		# code...
		date_default_timezone_set("Asia/Jakarta");
	}

	function fetchAll() {
		$rows   = array();
		$query  = $this->query("SELECT * FROM `sir_user` A INNER JOIN sir_rt B ON A.ID_USER = B.P_USERID WHERE A.U_GROUP_RULE = 'U_RT' ORDER BY A.U_CREATED_AT DESC");
		while($row  = $query->fetch_assoc()) {
			$rows[] = $row; 
		}

		return $rows;
	}

	function add_rt($rt) { 
		include_once '../model/connect.php';

		$nik   	  = $this->clean_all($rt["nik"]);
		$name  	  = $this->clean_post($rt["username"]);
		$fullname = $this->clean_post($rt["fullname"]);
		$email    = $this->clean_post($rt["email"]);
		$telpon   = $this->clean_post($rt["telpon"]);
		$jabatan  = $this->clean_post($rt["jabatan"]);
		$gender   = $this->clean_post($rt["gender"]);
		$wilayah  = $this->clean_post($rt["wilayah"]);
		$rwId     = $this->clean_all($rt["rw"]);
		$pass     = md5(md5(12345678));
		$token    = base64_encode('rt'.date('YmdHis').$name);

		
		  $qq    =  $this->query("INSERT INTO sir_user (U_NIK, U_NAME, U_FULLNAME, U_EMAIL, U_PASSWORD, U_MOBILE, U_GROUP_RULE, U_STATUS, U_REMINDER, U_KEY) VALUES ('$nik', '$name', '$fullname', '$email', '$pass', '$telpon', 'U_RT', '1', '0', '$token')");

		  if($qq) :
		  	 $max   = $this->query("SELECT MAX(ID_USER) AS maxId FROM sir_user");
		     $row   = mysqli_fetch_assoc($max);
		     $maxId = $row["maxId"];
		     $sql = mysqli_query($conn, "INSERT INTO sir_rt (P_USERID, P_RW, P_RT,  P_KETUA_RT, P_MASA_JABATAN, P_GENDER, P_STATUS) VALUES ('$maxId', '$rwId', '$wilayah', '$fullname', '$jabatan', '$gender', '1')");
		  	echo "<script>alert('User RT Added Succesfully')
                location.replace('../official/listrt')</script>";
          endif;

		//get nik
		$nik      = $this->query("SELECT * FROM sir_user WHERE U_NIK = '$nik'");
		// get username
		$uname    = $this->query("SELECT * FROM sir_user WHERE U_NAME = '$name'");
		// get email & no telpon
		$mail	  = $this->query("SELECT * FROM sir_user WHERE U_MOBILE = '$telpon'");

		if(mysqli_num_rows($nik) > 0) {
			 echo "<script>alert('NIK yang anda masukkan sudah tersedia')
                    location.replace('../official/listrt')</script>";
		} elseif(mysqli_num_rows($uname) > 0) {
			 echo "<script>alert('Username yang anda masukkan sudah tersedia')
                    location.replace('../official/listrt')</script>";
		} elseif(mysqli_num_rows($mail) > 0) {
			echo "<script>alert('No telp yang anda masukkan sudah tersedia')
                    location.replace('../official/listrt')</script>";
		} elseif(mysqli_num_rows($nik) < 1 || mysqli_num_rows($uname) < 1 || mysqli_num_rows($mail) < 1) {
		  
		}
	}

	function fetchByOne($id) {
		$kodeId  = $this->clean_all($id);
		if($kodeId != "") {
			$query = $this->query("SELECT * FROM sir_user A INNER JOIN sir_lurah B ON A.ID_USER = B.PL_USERID WHERE A.ID_USER = '$kodeId'");
			if(mysqli_num_rows($query) > 0) {
				$row = $query->fetch_assoc();

				return $row;
			} else {
				echo '<div class="alert bg-pink alert-dismissible" role="alert">
                <button typ="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  Masukkan ID User yang benar
          </div>';
			}
		} else {
			echo '<div class="alert bg-pink alert-dismissible" role="alert">
                <button typ="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  Id User tidak ditemukan
          </div>';
		}
	}

	function updateStatus($id, $status) {
		$query  = $this->query("UPDATE sir_user SET U_STATUS = '0' WHERE ID_USER = '$id'");
		$sql    = $this->query("UPDATE sir_rt SET P_STATUS = '0' WHERE P_USERID = '$id'");

		if($query && $sql) {
			echo "<script>alert('User RW status updated')
                    location.replace('../official/listrt')</script>";
		} else {
			echo "<script>alert('User RW status updated failed')</script>";
		}
	}

	function fetchAllCount() {
		$query = $this->query("SELECT COUNT(P_ID) AS jmlRt FROM sir_rt");
		$row   = $query->fetch_assoc();
		$data  = $row["jmlRt"];

		return $data;
	}

	function fetchRTAktif() {
		$query = $this->query("SELECT COUNT(P_ID) AS aktifRt FROM sir_rt WHERE P_STATUS = '1'");
		$row   = $query->fetch_assoc();
		$data  = $row["aktifRt"];

		return $data;
	}

	function fetchRTDeAktif() {
		$query = $this->query("SELECT COUNT(P_ID) AS deaktifRt FROM sir_rt WHERE P_STATUS = '0'");
		$row   = $query->fetch_assoc();
		$data  = $row["deaktifRt"];

		return $data;
	}

	function globalfetchRequest($id, $status) {
		$bigId = $_SESSION["ID_USER"];
	    $query = $this->query("UPDATE sir_permintaan SET PER_STATUS = '$status' WHERE PM_RT = '$id'");
	    if($query) {
	    	echo "<script>alert('User RW status updated')
                    location.replace('../official/listrt')</script>";
	    } else {
	    	echo "<script>alert('Ubah status gagal')</script>";
	    }
	}

	function historyRequest($id) {
		$rows  = array();
		$query = $this->query("SELECT * FROM sir_permintaan WHERE PER_RT = '$id' ORDER BY PER_CREATED_AT DESC");
		while($row = $query->fetch_assoc()) {
			$rows[] = $row;
		}

		return $rows;
	}

	function codelist() {
		$rows = array();
		$query = $this->query("SELECT *, COUNT(PER_ID) AS jmlCode, IFULL(SUM(MAX(PER_NILAI)), 0) AS kodeGlobal FROM sir_permintaan WHERE PER_RT = '$id' GROUP BY PER_ACC_RT = '$id' LIMIT 0,5");
		while($row = $query->fetch_assoc()) {
			$rows[] = $row;
		}

		return $rows;
	} 

	// get dashboard RT
	function get_letter_waiting($sessId) {
		$rows  = array();
		$query = $this->query("SELECT * FROM sir_permintaan WHERE PM_RT = '$sessId' AND PER_STATUS = 'WAITING_APPROVAL_RT' ORDER BY PER_CREATED_AT DESC");
		while($row = $query->fetch_assoc()) {
			$rows[]= $row;
		}

		return $rows;
	}

	function fetch_detail_letter_wait($kodeId) {
		$query  = $this->query("SELECT * FROM sir_permintaan WHERE PER_ID = '$kodeId'");
		$row    = $query->fetch_assoc();

		return $row;
	}

	function get_letter_selesai() {
		$sessId = $_SESSION["P_RT"];
		$rows  = array();
		$query = $this->query("SELECT * FROM sir_permintaan WHERE PM_RT = '$sessId' AND PER_STATUS = 'PRINT' ORDER BY PER_CREATED_AT DESC");
		while($row = $query->fetch_assoc()) {
			$rows[]= $row;
		}

		return $rows;
	}

	function get_aduan_waiting($sessId) {
		$rows  = array();
		$query = $this->query("SELECT * FROM  sir_pengaduan WHERE SP_RTID = '$sessId' AND SP_STATUS = 'WAITING_APPROVAL_RT' ORDER BY SP_CREATED_AT DESC");
		while($row  = $query->fetch_assoc()) {
			$rows[] = $row;
		}

		return $rows;
	}

	function get_aduan_selesai($sessId) {
		$rows  = array();
		$query = $this->query("SELECT * FROM sir_pengaduan WHERE SP_RTID = '$sessId' AND SP_STATUS = 'SELESAI' ORDER BY SP_CREATED_AT DESC");
		while($row = $query->fetch_assoc()) {
			$rows  = $row; 
		}

		return $rows;
	}
    
    function get_warga_byRt($sessId) {
    	$rows  = array();
    	$query = $this->query("SELECT * FROM sir_warga WHERE W_RT = '$sessId'");
    	while($row  = $query->fetch_assoc()) {
    		$rows[] = $row; 
    	}

    	return $rows;
    }

    function get_letter_status($sessId) {
    	$rows  = array();
    	$query = $this->query("SELECT * FROM sir_permintaan AS A INNER JOIN sir_warga AS B ON A.NIK = B.W_NIK WHERE A.PM_RT = '$sessId' ORDER BY A.PER_UPDATED_AT DESC LIMIT 0,5");
    	while($row  = $query->fetch_assoc()) {
    		$rows[] = $row; 
    	}

    	return $rows;
    }

    function get_aduan_status($sessId) {
    	$rows  = array();
    	$query = $this->query("SELECT * FROM sir_pengaduan AS A INNER JOIN sir_warga AS B ON A.SP_NIK = B.W_NIK WHERE A.SP_RTID = '$sessId' ORDER BY A.SP_CREATED_AT DESC LIMIT 0,5");
    	while($row  = $query->fetch_assoc()) {
    		$rows[] = $row; 
    	}

    	return $rows;
    }

    function fetch_history_letter() {
    	//if($_SESSION["U_GROUP_RULE"] == "U_RT") :
    	    $rows  = array();
    		$sessId = $_SESSION["P_RT"];
    		$query= $this->query("SELECT * FROM sir_permintaan WHERE PER_ACC_RT = '$sessId' ORDER BY PER_UPDATED_AT DESC");
    		while($row = $query->fetch_assoc()) {
	    		$rows[]=$row;
	    	}
	    	return $rows;
    }

    function fetch_rw_history_letter() {
    	$rows  = array();
    	$sessId = $_SESSION["PW_RW"];
    	$query= $this->query("SELECT * FROM sir_permintaan WHERE PER_ACC_RW = '$sessId' ORDER BY PER_UPDATED_AT DESC");
    	while($row = $query->fetch_assoc()) {
    		$rows[]=$row;
    	}
    	return $rows;
    }

    function fetch_ubah_status_letter($kodeId, $status) {
    	$sessId = $_SESSION["ID_USER"];
    	if($status == "perrt") {
    		$query = $this->query("UPDATE sir_permintaan SET PER_STATUS = 'WAITING_APPROVAL_RW', PER_ACC_RT = '$sessId' WHERE PER_ID = '$kodeId'");
    		if($query) {
    			echo "<script>alert('Letter status updated')
                    location.replace('../letter')</script>";
    		} else {
    			echo "<script>alert('Letter status failed')
                    location.replace('../letter')</script>";
    		}
    	} elseif($status == "perrw") {
    		$query = $this->query("UPDATE sir_permintaan SET PER_STATUS = 'WAITING_KADES', PER_ACC_RW = '$sessId' WHERE PER_ID = '$kodeId'");
    		if($query) {
    			echo "<script>alert('Letter status updated')
                    location.replace('../letter')</script>";
    		} else {
    			echo "<script>alert('Letter status failed')
                    location.replace('../letter')</script>";
    		}
    	} elseif($status == "reject") {
    		$query = $this->query("UPDATE sir_permintaan SET PER_STATUS = 'REJECT', PER_ACC_RT = '$sessId' WHERE PER_ID = '$kodeId'");
    		if($query) {
    			echo "<script>alert('Letter status updated')
                    location.replace('../letter')</script>";
    		} else {
    			echo "<script>alert('Letter status failed')
                    location.replace('../letter')</script>";
    		}
    	}
    }

    function get_slow_report($lap) {
    	$rows     = array();
    	$sessId   = $_SESSION["P_RT"];
    	$tglAwal  = $this->clean_post($lap["awal"]);
    	$tglAkhir = $this->clean_post($lap["akhir"]);

    	// generate format tanggal
    	$tgl1     = date("Y-m-d", strtotime($tglAwal));
    	$tgl2     = date("Y-m-d", strtotime($tglAkhir));

    	$query    = $this->query("SELECT * FROM sir_permintaan WHERE PER_TANGGAL BETWEEN '$tgl1' AND '$tgl2' AND PM_RT = '$sessId' ORDER BY PER_CREATED_AT DESC");

    	while($row  = $query->fetch_assoc()) {
    		$rows[] = $row;
    	}

    	return $rows;
    }
    
    function get_rw_report($lap) {
    	$rows     = array();
    	$sessId   = $_SESSION["P_RW"];
    	$tglAwal  = $this->clean_post($lap["tglAwal"]);
    	$tglAkhir = $this->clean_post($lap["tglAkhir"]);

    	// generate format tanggal
    	$tgl1     = date("Y-m-d", strtotime($tglAwal));
    	$tgl2     = date("Y-m-d", strtotime($tglAkhir));

    	$query    = $this->query("SELECT * FROM sir_permintaan WHERE PER_TANGGAL BETWEEN '$tgl1' AND '$tgl2' AND PM_RW = '$sessId' ORDER BY PER_CREATED_AT DESC");
    	while($row = $query->fetch_assoc()) {
    		$rows[]= $row;
    	}

    	return $rows;
    }

    // PENGADUAN
    function get_pengaduan_byRT($sessId) {
    	$rows  = array();
    	$query = $this->query("SELECT * FROM sir_pengaduan WHERE SP_RTID = '$sessId' AND SP_STATUS = 'WAITING_APPROVAL_RT' ORDER BY SP_CREATED_AT DESC");
    	while($row = $query->fetch_assoc()) {
    		$rows[]= $row;
    	}

    	return $rows;
    }

    function get_report_pengaduan($lap) {
    	$rows     = array();
    	$sessId   = $_SESSION["P_RT"];
    	$tglAwal  = $this->clean_post($lap["awal"]);
    	$tglAkhir = $this->clean_post($lap["akhir"]);

    	// generate format tanggal
    	$tgl1     = date("Y-m-d", strtotime($tglAwal));
    	$tgl2     = date("Y-m-d", strtotime($tglAkhir));

    	$query    = $this->query("SELECT * FROM sir_pengaduan WHERE SP_TANGGAL BETWEEN '$tgl1' AND '$tgl2' AND SP_RTID = '$sessId' ORDER BY SP_CREATED_AT DESC");

    	while($row  = $query->fetch_assoc()) {
    		$rows[] = $row;
    	}

    	return $rows;
    }

    function get_pengaduan_detail($kodeId) {
    	$query = $this->query("SELECT * FROM sir_pengaduan A INNER JOIN sir_warga B ON A.SP_NIK = B.W_NIK WHERE A.SP_BIGID = '$kodeId'");
    	$row   = $query->fetch_assoc();

    	return $row;
    }

    function execution_pengaduan($exec) {
    	$sessId     = $_SESSION["P_RT"];
    	$keterangan = $this->clean_post($exec["keterangan"]);
    	$kodeId     = $this->clean_all($exec["kodeId"]);
    	$param      = $this->clean_all($exec["param"]);

    	if($param == "approve") {
    		$query = $this->query("UPDATE sir_pengaduan SET SP_KETERANGAN = '$keterangan', SP_STATUS = 'SELESAI', SP_ EXEC = '$sessId' WHERE SP_BIGID = '$kodeId'");
    		if($query) {
    			echo "<script>alert('pengaduan status updated')
                    location.replace('../complaint')</script>";
    		} else {
    			echo "<script>alert('pengaduan status failed')
                    location.replace('../complaint')</script>";
    		}
    	} else {
    		$query = $this->query("UPDATE sir_pengaduan SET SP_KETERANGAN = '$keterangan', SP_STATUS = 'WAITING_APPROVAL_RW', SP_ EXEC = '$sessId' WHERE SP_BIGID = '$kodeId'");
    		if($query) {
    			echo "<script>alert('pengaduan status updated')
                    location.replace('../complaint')</script>";
    		} else {
    			echo "<script>alert('pengaduan status failed')
                    location.replace('../complaint')</script>";
    		}
    	}
    }

    function get_history_pengaduan($sessId) {
    	$rows  = array();
    	$query = $this->query("SELECT * FROM sir_pengaduan WHERE SP_EXEC = '$sessId' ORDER BY SP_CREATED_AT DESC");
    	while($row  = $query->fetch_assoc()) {
    		$rows[] = $row;
    	}

    	return $rows;
    }
}
?>