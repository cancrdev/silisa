<?php

include_once '../../model/config.php';
/**
*
*/
class NewsController extends Connection
{

	function __construct()
	{
		# code...
		date_default_timezone_set("Asia/Jakarta");
	}

	function fetchAll() {
		$rows  = array();
		$query = $this->query("SELECT * FROM sir_news ORDER BY N_CREATED_AT DESC");
		while($row = $query->fetch_assoc()) {
			$rows[] = $row;
		}

		return $rows;
	}

	function add_news($new) {
		require '../../assets/PHPMailer/src/Exception.php';
        require '../../assets/PHPMailer/src/PHPMailer.php';
        require '../../assets/PHPMailer/src/SMTP.php';

		$id       = $_SESSION["ID_USER"];
		$title    = $this->clean_post($new["judul"]);
		$subject  = $this->clean_post($new["subject"]);
		$date     = date("Y-m-d");

		$image  = $_FILES["image"]["name"];
		$name    = "image21";
		$target_dir = "../../assets/news/";

		$newimage = str_replace(" ", "", basename($name))."_".date('dmYHis')."_".str_replace(" ", "", basename($image));
		$tar_img  = $target_dir . $newimage;
		$upload1  = move_uploaded_file($_FILES["image"]["tmp_name"], $tar_img);

		$query    = $this->query("INSERT INTO sir_news (N_TITLE, N_SUBJECT, N_TANGGAL, N_IMAGE, N_CREATED_BY) VALUES ('$title', '$subject', '$date', '$newimage', 'ADMIN')");

		if($query) {
			//page mail
    		     ob_start();
                 include 'mail.php';
                 $page    = ob_get_contents();
                 ob_clean();
                    
				  $mail = new PHPMailer\PHPMailer\PHPMailer();
                  $mail->IsSMTP();
                  try {
                      $mail->SMTPDebug = 0;                                 // Enable verbose debug output
                      $mail->isSMTP();                                      // Set mailer to use SMTP
                      $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
                      $mail->SMTPAuth = true;                               // Enable SMTP authentication
                      $mail->Username = 'crmanagement.official@gmail.com';                 // SMTP username
                      $mail->Password = 'P@ssw0rdadmin';                           // SMTP password
                      $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                      $mail->Port = 587;                                      // TCP port to connect to
                      $mail->SMTPOptions = array(
                          'ssl' => array(
                              'verify_peer' => false,
                              'verify_peer_name' => false,
                              'allow_self_signed' => true
                          )
                      );

                    $data = $this->query("SELECT * FROM sir_subcribe");
                    while($row  = $data->fetch_assoc()) {
                    	$email  = $row["SB_EMAIL"];
                    	$mail->AddAddress($email);
                    }
                     //email tujuan isi dengan emailmu misal test@test.com
                    $message = $page;
                    $mail->SetFrom('crmanagement.official@gmail.com','(noreply)DokterApps'); // email pengirim

                    $mail->Subject = 'Ada berita terbaru';                       
                    $mail->MsgHTML('<p>'.$message);
                    $mail->Send();{
                       
                    }   
                  } catch (phpmailerException $e) {
                     echo $e->errorMessage(); 
                 } 
			echo "<script>alert('News Added Succesfully')
                    location.replace('../news')</script>";
		} else {
			echo "<script>alert('News Added Failed')
                    location.replace('../news')</script>";
		}
	}

	function fetchByOne($id) {
		$kodeId = $this->clean_all($id);
		$query  = $this->query("SELECT * FROM sir_news WHERE N_ID = '$kodeId'");

		$row    = $query->fetch_assoc();

		return $row;
	}

	function updateByOne($newsa) {
		$id       = $this->clean_all($_GET["key"]);
		$title    = $this->clean_post($newsa["judul"]);
		$subject  = $this->clean_post($newsa["subject"]);

		$image  = $_FILES["image"]["name"];
		$name    = "image21";
		$target_dir = "../../assets/news/";

		if($image == "" || $image == null) {
			$query    = $this->query("UPDATE sir_news SET N_TITLE = '$title', N_SUBJECT = '$subject' WHERE N_ID = '$id'");
			if($query) {
				echo "<script>alert('News Updated Succesfully')
                    location.replace('../news')</script>";
			} else {
				echo "<script>alert('News Updated Failed')
	                    location.replace('../news')</script>";
			}
		} else {
			$newimage = str_replace(" ", "", basename($name))."_".date('dmYHis')."_".str_replace(" ", "", basename($image));
			$tar_img  = $target_dir . $newimage;
			$upload1  = move_uploaded_file($_FILES["image"]["tmp_name"], $tar_img);

			$query    = $this->query("UPDATE sir_news SET N_TITLE = '$title', N_SUBJECT = '$subject', N_IMAGE = '$newimage' WHERE N_ID = '$id'");

			if($query) {
				echo "<script>alert('News Updated Succesfully')
                    location.replace('../news')</script>";
			} else {
				echo "<script>alert('News Updated Failed')</script>";
			}
		}
	}

	function deleteByOne($id) {
		$kodeId = $this->clean_all($id);
		$query  = $this->query("DELETE FROM sir_news WHERE N_ID = '$kodeId'");

		if($query) {
			echo "<script>alert('News Deleted Succesfully')
                    location.replace('../news')</script>";
		} else {
			echo "<script>alert('News Deleted Failed')
                    location.replace('../news')</script>";
		}
	}

	function fetchAllCount() {
		$query = $this->query("SELECT COUNT(N_ID) AS jmlBerita FROM sir_news");
		$row   = $query->fetch_assoc();
		$data  = $row["N_ID"];

		return $rows;
	}
}

?>
