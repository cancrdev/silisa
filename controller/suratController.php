<?php
include_once '../../../model/config.php';
/**
* 
*/
/**
* 
*/
/**
* 
*/
class SuratController extends Connection
{
	
	function __construct()
	{
		# code...
		date_default_timezone_set("Asia/Jakarta");
	}

	function view_print_request($id) {
		$kodeId = $this->clean_all($id);
		$v 		= $this->query("SELECT PER_ID FROM sir_permintaan WHERE PER_ID = '$kodeId'");
		$vi     = $v->fetch_assoc();
		$bigId  = $vi["PER_ID"];

		if($kodeId == "" || $bigId != $kodeId) {
			header("Location: ../../404");
		} else {
			$query = $this->query("SELECT * FROM  sir_permintaan AS A INNER JOIN sir_warga AS B ON A.NIK = B.W_NIK INNER JOIN sir_surat AS C ON A.S_ID = C.S_ID WHERE PER_ID = '$kodeId'");
			$row   = $query->fetch_assoc();

			return $row;
		}
	}

	function get_lurah() {
		$query = $this->query("SELECT * FROM sir_lurah WHERE PL_STATUS = '1'");
		$row   = $query->fetch_assoc();

		return $row;
	}
}

?>