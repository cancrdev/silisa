<?php
include_once '../../model/config.php';

/**
* 
*/
class CounterController extends Connection
{
	
	function __construct()
	{
		date_default_timezone_set("Asia/Jakarta");
	}

	// ==== ADMIN ===== //

	//LETTER

	function letter_to_print() {
		$query = $this->query("SELECT COUNT(PER_ID) AS jmlLetter FROM  sir_permintaan WHERE PER_STATUS = 'PRINT'");
		$rows   = $query->fetch_assoc();
		$row    = $rows["jmlLetter"];

		return $row;
	}	

	function letter_waiting_kades() {
		$query = $this->query("SELECT COUNT(PER_ID) AS jmlWaitingKades FROM  sir_permintaan WHERE PER_STATUS = 'WAITING_KADES'");
		$rows   = $query->fetch_assoc();
		$row    = $rows["jmlWaitingKades"];

		return $row;
	}


	function letter_waiting_rw() {
		$query = $this->query("SELECT COUNT(PER_ID) AS jmlWaitingrw FROM  sir_permintaan WHERE PER_STATUS = 'WAITING_APPROVAL_RW'");
		$rows   = $query->fetch_assoc();
		$row    = $rows["jmlWaitingrw"];

		return $row;
	}

	function letter_waiting_rt() {
		$query = $this->query("SELECT COUNT(PER_ID) AS jmlWaitingrt FROM  sir_permintaan WHERE PER_STATUS = 'WAITING_APPROVAL_RT'");
		$rows   = $query->fetch_assoc();
		$row    = $rows["jmlWaitingrt"];

		return $row;
	}

	// END LETTER

	// ADUAN
	function aduan_waiting_rt() {
		$query = $this->query("SELECT COUNT(SP_BIGID) AS jmlAduanRT FROM sir_pengaduan WHERE SP_STATUS = 'WAITING_APPROVAL_RT'");
		$rows  = $query->fetch_assoc();
		$row   = $rows["jmlAduanRT"];

		return $row;
	}

	function aduan_waiting_rw() {
		$query = $this->query("SELECT COUNT(SP_BIGID) AS jmlAduanRW FROM sir_pengaduan WHERE SP_STATUS = 'WAITING_APPROVAL_RW'");
		$rows  = $query->fetch_assoc();
		$row   = $rows["jmlAduanRW"];

		return $row;
	}

	function aduan_waiting_kades() {
		$query = $this->query("SELECT COUNT(SP_BIGID) AS jmlAduanKades FROM sir_pengaduan WHERE SP_STATUS = 'WAITING_KADES'");
		$rows  = $query->fetch_assoc();
		$row   = $rows["jmlAduanKades"];

		return $row;
	}

	function aduan_selesai() {
		$query = $this->query("SELECT COUNT(SP_BIGID) AS aduanFixed FROM sir_pengaduan WHERE SP_STATUS = 'FIXED'");
		$rows  = $query->fetch_assoc();
		$row   = $rows["aduanFixed"];

		return $row;
	}
	// END ADUAN

	// get warga kelompok
	function warga_menikah() {
		$query = $this->query("SELECT COUNT(ID_WARGA) AS menikah FROM sir_warga WHERE W_STATUS = 'Kawin'");
		$rows  = $query->fetch_assoc();
		$row   = $rows["menikah"];

		return $row;
	}

	function warga_blmmenikah() {
		$query = $this->query("SELECT COUNT(ID_WARGA) AS blmMenikah FROM sir_warga WHERE W_STATUS = 'Belum Kawin'");
		$rows  = $query->fetch_assoc();
		$row   = $rows["blmMenikah"];

		return $row;
	}

	function cerai() {
		$query = $this->query("SELECT COUNT(ID_WARGA) AS cerai FROM sir_warga WHERE W_STATUS IN ('Cerai', 'Janda', 'Duda')");
		$rows  = $query->fetch_assoc();
		$row   = $rows["cerai"];

		return $row;
	}

	function agama_islam() {
		$query = $this->query("SELECT COUNT(ID_WARGA) AS islam FROM sir_warga WHERE W_AGAMA = 'Islam'");
		$rows  = $query->fetch_assoc();
		$row   = $rows["islam"];

		return $row;
	}

	function agama_kristen() {
		$query = $this->query("SELECT COUNT(ID_WARGA) AS kristen FROM sir_warga WHERE W_AGAMA = 'Kristen'");
		$rows  = $query->fetch_assoc();
		$row   = $rows["kristen"];

		return $row;
	}

	function agama_katolik() {
		$query = $this->query("SELECT COUNT(ID_WARGA) AS katolik FROM sir_warga WHERE W_AGAMA = 'Katolik'");
		$rows  = $query->fetch_assoc();
		$row   = $rows["katolik"];

		return $row;
	}

	function agama_budha() {
		$query = $this->query("SELECT COUNT(ID_WARGA) AS budha FROM sir_warga WHERE W_AGAMA = 'Budha'");
		$rows  = $query->fetch_assoc();
		$row   = $rows["budha"];

		return $row;
	}

	function agama_lain() {
		$query = $this->query("SELECT COUNT(ID_WARGA) AS lain FROM sir_warga WHERE W_AGAMA NOT IN ('Islam', 'Katolik', 'Kristen', 'Budha')");
		$rows  = $query->fetch_assoc();
		$row   = $rows["lain"];

		return $row;
	}
	//end warga kelompok

	//data warga
	function AllWarga() {
	 	$query = $this->query("SELECT COUNT(ID_WARGA) AS AllResident FROM sir_warga");
	 	$rows  = $query->fetch_assoc();
	 	$row   = $rows["AllResident"];

	 	return $row;
	}

	function DataWilayah() {
		$query  = $this->query("SELECT TS_KEY FROM ts_settings");
		$rows   = $query->fetch_assoc();
		$row    = $rows["TS_KEY"];

		return $row;
	}

	function study_wajar() {
		$query  = $this->query("SELECT COUNT(ID_WARGA) AS study_wajar FROM sir_warga WHERE W_PENDIDIKAN NOT IN('S1', 'S2', 'S3', '-')");
		$rows   = $query->fetch_assoc();
		$row    = $rows["study_wajar"];

		return $row;
	}

	function study_lanjut() {
		$query  = $this->query("SELECT COUNT(ID_WARGA) AS study_lanjut FROM sir_warga WHERE W_PENDIDIKAN IN('S1', 'S2', 'S3')");
		$rows   = $query->fetch_assoc();
		$row    = $rows["study_lanjut"];

		return $row;
	}

	function gender_male() {
		$query  = $this->query("SELECT COUNT(ID_WARGA) AS male FROM sir_warga WHERE W_JK = 'Laki-Laki'");
		$rows   = $query->fetch_assoc();
		$row    = $rows["male"];

		return $row;
	}

	function gender_female() {
		$query  = $this->query("SELECT COUNT(ID_WARGA) AS female FROM sir_warga WHERE W_JK = 'Perempuan'");
		$rows   = $query->fetch_assoc();
		$row    = $rows["female"];

		return $row;
	}

	function work() {
		$query  = $this->query("SELECT COUNT(ID_WARGA) AS work FROM sir_warga WHERE W_PEKERJAAN NOT IN('-')");
		$rows   = $query->fetch_assoc();
		$row    = $rows["work"];

		return $row;
	}

	function notWork() {
		$query  = $this->query("SELECT COUNT(ID_WARGA) AS notwork FROM sir_warga WHERE W_PEKERJAAN IN('-')");
		$rows   = $query->fetch_assoc();
		$row    = $rows["notwork"];

		return $row;
	}

	function jumlah_permintaan() {
		$row   = array();
		$query = $this->query("SELECT *, COUNT(PER_ID) AS jmlMinta FROM `sir_permintaan` GROUP BY NIK ORDER BY jmlMinta DESC LIMIT 0,5");
		while($rows  = $query->fetch_assoc()) {
			$row[]   = $rows;
		}

		return $row;
	}

	function jumlah_aduan() {
		$row    = array();
		$query  = $this->query("SELECT *, COUNT(SP_USERID) AS jmlAduan FROM sir_pengaduan GROUP BY SP_USERID ORDER BY jmlAduan DESC LIMIT 0,5");
		while($rows = $query->fetch_assoc()) {
			$row[]  = $rows;
		}

		return $row;
	}
	//end data warga

	// === END ADMIN ==== 
}

?>