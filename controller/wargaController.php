<?php
include_once '../../model/config.php';

/**
* 
*/
class WargaController extends Connection
{
	
	function __construct()
	{
		# code...
		date_default_timezone_set("Asia/Jakarta");
	}

	function fetchAll() {
		$rows   = array();
		$query  = $this->query("SELECT * FROM sir_warga ORDER BY W_CREATED_AT DESC");
		while($row  = $query->fetch_assoc()) {
			$rows[] = $row;
		}

		return $rows;
	}


	function fetchByOne($id) {
		$kodeId = $this->clean_all($id);
		if($kodeId != "" || $kodeId != null) :
			$query = $this->query("SELECT * FROM sir_warga WHERE ID_WARGA = '$kodeId'");
			$row   = $query->fetch_assoc();

			return $row;
		else :
			echo "<script>alert('Id warga tidak ditemukan')
                    location.replace('../warga')</script>";
		endif;
	}

	// new 

	function fetchByNIK($kodeId) {
		$query = $this->query("SELECT * FROM sir_warga WHERE W_NIK = '$kodeId'");
		$row   = $query->fetch_assoc();

		return $row;
	}

	function updateFetchOne($warga) {
		$kodeId = $this->clean_all($_GET["key"]);
		$nama   = $this->clean_post($warga["nama"]);
		$tempat = $this->clean_post($warga["tempat"]);
		$tgl    = $this->clean_all($warga["tgl"]);
		$gender = $this->clean_post($warga["gender"]);
		$alamat = $this->clean_post($warga["alamat"]);
		$agama  = $this->clean_post($warga["agama"]);
		$status = $this->clean_post($warga["status"]);
		$study  = $this->clean_post($warga["study"]);
		$work   = $this->clean_post($warga["work"]);

		$query  = $this->query("UPDATE sir_warga SET W_NAMA = '$nama', W_TMP_LAHIR = '$tempat', W_TGL_LAHIR = '$tgl', W_JK = '$gender', W_ALAMAT = '$alamat', W_AGAMA = '$agama', W_STATUS = '$status', W_PENDIDIKAN = '$study', W_PEKERJAAN = '$work' WHERE ID_WARGA = '$kodeId'");

		if($query) {
			echo "<script>alert('Data Warga berhasil diubah')
                    location.replace('../warga')</script>";
		} else {
			echo "<script>alert('Data warga gagal diubah')
                    location.replace('../warga')</script>";
		}
	}

	function deleteWarga($id) {
		$query = $this->query("DELETE FROM sir_warga WHERE ID_WARGA = '$id'");

		if($query) {
			echo "<script>alert('Data Warga berhasil dihapus')
                    location.replace('../warga')</script>";
		} else {
			echo "<script>alert('Data warga gagal dihapus')
                    location.replace('../warga')</script>";
		}
	}
}
?>