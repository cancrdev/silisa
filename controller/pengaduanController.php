<?php
include_once '../model/config.php';

/**
* 
*/
class PengaduanController extends Connection
{
	
	function __construct(argument)
	{
		date_default_timezone_set("Asia/Jakarta");
	}

	//user warga
	function tambah_aduan($aduan) {
		$userId = $this->clean_all($aduan["userId"]);
		$rtId   = $this->clean_all($aduan["rtId"]);
		$rwId   = $this->clean_all($aduan["rwId"]);
		$judul  = $this->clean_post($aduan["judul"]);
		$note   = $this->clean_post($aduan["keterangan"]);

		$target_dir = "../../assets/uploads/";
		$name    = "image21";
		$image      = $_FILES["image"]["name"];
		$newimage = str_replace(" ", "", basename($name))."_".date('dmYHis')."_".str_replace(" ", "", basename($image));
	    $tar_img	= $target_dir . $newimage;
	    $upload1    = move_uploaded_file($_FILES["image"]["tmp_name"], $tar_img);

	    if($image != "" || $image != null) :
	    	$query = $this->query("INSERT INTO sir_pengaduan (SP_USERID, SP_RTID, SP_RWID, SP_SUBJECT, SP_KETERANGAN, 	SP_IMAGE, SP_STATUS, SP_NOTIF) VALUES ('$userId', '$rtId', '$rwId', '$judul', '$note', '$newimage', 'WAITING_RT', '0')");
	    	if($query) :
	    		echo "<script>alert('Pengaduan berhasil direcord')
                location.replace('../official/listrt')</script>";
	    	else :
	    		echo "<script>alert('Pengaduan gagal diinputkan,silahkan ulangi langi')</script>";
	    	endif;
	    else :
	    	$query = $this->query("INSERT INTO sir_pengaduan (SP_USERID, SP_RTID, SP_RWID, SP_SUBJECT, SP_KETERANGAN, SP_STATUS, SP_NOTIF) VALUES ('$userId', '$rtId', '$rwId', '$judul', '$note', 'WAITING_RT', '0')");
	    	if($query) :
	    		echo "<script>alert('Pengaduan berhasil direcord')
                location.replace('../official/listrt')</script>";
	    	else :
	    		echo "<script>alert('Pengaduan gagal diinputkan,silahkan ulangi langi')</script>";
	    	endif;
	    endif;
	}

	function get_history_aduan_byuser() {
		$userId = $_SESSION["ID_USER"];
		$rows   = array();
		$query  = $this->query("SELECT * FROM sir_pengaduan WHERE SP_USERID = '$userId' ORDER BY SP_CREATED_AT DESC");
		while($row = $query->fetch_assoc()) {
			$rows[]= $row;
		}

		return $rows;
	}   
}

?>