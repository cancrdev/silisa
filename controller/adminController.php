<?php
include_once '../../model/config.php';

/**
* 
*/
class AdminController extends Connection
{
	
	function __construct()
	{
		# code...
		date_default_timezone_set("Asia/Jakarta");
	}

	function get_settings() {
		$query = $this->query("SELECT * FROM sir_about WHERE TS_BIGID = '1'");
		$row   = $query->fetch_assoc();

		return $row;
	}

	function fetchSettings() {
		$query = $this->query("SELECT * FROM sir_about");
		$row   = $query->fetch_assoc();

		return $row;
	}

	function fetchByOneSettings($id) {
		$kodeId  = $this->clean_all($_GET["value"]);
		$query = $this->query("SELECT * FROM sir_about WHERE TS_BIGID = '$kodeId'");
		$row   = $query->fetch_assoc();

		return $row;
	}

	function updateSejarah($gear) {
		$kodeId  = $this->clean_all($_GET["value"]);
		$sejarah = $this->clean_post($gear["sejarah"]);

		$query   = $this->query("UPDATE sir_about SET TS_SEJARAH = '$sejarah' WHERE TS_BIGID = '$kodeId'");

		if($query) {
			echo "<script>alert('Settings Sejarah Updated Succesfully')
                    location.replace('../setting')</script>";
		} else {
			echo "<script>alert('Settings Sejarah Updated Failed')
                    location.replace('../setting')</script>";
		}
	}

	function updateProfil($gear) {
		$kodeId  = $this->clean_all($_GET["value"]);
		$profil  = $this->clean_post($gear["profil"]);

		$query   = $this->query("UPDATE sir_about SET TS_PROFIL = '$profil' WHERE TS_BIGID = '$kodeId'");

		if($query) {
			echo "<script>alert('Settings Updated Succesfully')
                    location.replace('../setting/profile-desa')</script>";
		} else {
			echo "<script>alert('Settings Updated Failed')
                    location.replace('../setting/profile-desa')</script>";
		}
	}

	function updateVisi($gear) {
		$kodeId  = $this->clean_all($_GET["value"]);
		$visi    = $this->clean_post($gear["visi"]);

		$query   = $this->query("UPDATE sir_about SET TS_VISI = '$visi' WHERE TS_BIGID = '$kodeId'");

		if($query) {
			echo "<script>alert('Settings Updated Succesfully')
                    location.replace('../setting/visi-misi')</script>";
		} else {
			echo "<script>alert('Settings Updated Failed')
                    location.replace('../setting/visi-misi')</script>";
		}
	}

	function updatePemerintah($gear) {
		$kodeId  = $this->clean_all($_GET["value"]);
		$gov     = $this->clean_post($gear["goverment"]);

		$query   = $this->query("UPDATE sir_about SET TS_PEMERINTAH = '$gov' WHERE TS_BIGID = '$kodeId'");

		if($query) {
			echo "<script>alert('Settings Updated Succesfully')
                    location.replace('../settings')</script>";
		} else {
			echo "<script>alert('Settings Updated Failed')
                    location.replace('../settings')</script>";
		}
	}

	function updatePeta($gear) {
		$kodeId  = $this->clean_all($_GET["value"]);
		$visi    = $this->clean_post($gear["peta"]);

		$query   = $this->query("UPDATE sir_about SET TS_PETA = '$visi' WHERE TS_BIGID = '$kodeId'");

		if($query) {
			echo "<script>alert('Settings Updated Succesfully')
                    location.replace('../setting/peta-desa')</script>";
		} else {
			echo "<script>alert('Settings Updated Failed')
                    location.replace('../setting/peta-desa')</script>";
		}
	}

	function updateLpm($gear) {
		$kodeId  = $this->clean_all($_GET["value"]);
		$lpm     = $this->clean_post($gear["lpm"]);

		$query   = $this->query("UPDATE sir_about SET TS_LPM = '$lpm' WHERE TS_BIGID = '$kodeId'");

		if($query) {
			echo "<script>alert('Settings Updated Succesfully')
                    location.replace('../setting/lpm')</script>";
		} else {
			echo "<script>alert('Settings Updated Failed')
                    location.replace('../setting/lpm')</script>";
		}
	}

	function updateKarang($gear) {
		$kodeId  = $this->clean_all($_GET["value"]);
		$karang  = $this->clean_post($gear["karang"]);

		$query   = $this->query("UPDATE sir_about SET TS_KARANG_TARUNA = '$karang' WHERE TS_BIGID = '$kodeId'");

		if($query) {
			echo "<script>alert('Settings Updated Succesfully')
                    location.replace('../setting/karang-taruna')</script>";
		} else {
			echo "<script>alert('Settings Updated Failed')
                    location.replace('../setting/karang-taruna')</script>";
		}
	}

	function updatePkk($gear) {
		$kodeId  = $this->clean_all($_GET["value"]);
		$pkk     = $this->clean_post($gear["pkk"]);

		$query   = $this->query("UPDATE sir_about SET TS_PKK = '$pkk' WHERE TS_BIGID = '$kodeId'");

		if($query) {
			echo "<script>alert('Settings Updated Succesfully')
                    location.replace('../setting/pkk')</script>";
		} else {
			echo "<script>alert('Settings Updated Failed')
                    location.replace('../setting/pkk')</script>";
		}
	}

	function ubahStatus($id, $status, $type) {
		$query   = $this->query("UPDATE sir_user SET U_STATUS = '$status' WHERE ID_USER = '$id'");
		if($query) {
			if($type == "kades") :
				$sql = $this->query("UPDATE sir_lurah SET U_STATUS = '$status' WHERE PL_USERID = '$id'");
				echo "<script>alert('Status Updated Succesfully')
                    location.replace('../official/listlurah')</script>";
			elseif($type == "rt") :
				$sql = $this->query("UPDATE sir_rt SET P_STATUS = '$status' WHERE P_USERID = '$id'");
				echo "<script>alert('Status Updated Succesfully')
                    location.replace('../official/listrt')</script>";
			elseif($type == "rw") :
				$sql = $this->query("UPDATE sir_rw SET PW_STATUS = '$status' WHERE PW_USERID = '$id'");
				echo "<script>alert('Status Updated Succesfully')
                    location.replace('../official/listrw')</script>";
			endif;
		} else {
			echo "<script>alert('Settings Updated Failed')</script>";
		}
	}

	function fetchAllLetter() {
		$rows   = array();
		$query  = $this->query("SELECT * FROM  sir_surat ORDER BY S_CREATED_AT DESC");
		while($row  = $query->fetch_assoc()) {
			$rows[] = $row; 
		}

		return $rows;
	}

	function add_letter($letters) {
		// count jumlah urutan surat
		$sql   = $this->query("SELECT COUNT(S_ID) AS jml_letter FROM sir_surat");
		$row   = $sql->fetch_assoc();
		$urut  = intval($row["jml_letter"])+1;

		$nama  = $this->clean_post($letters["nama"]);

		$query = $this->query("INSERT INTO sir_surat (S_NAMA, S_URUTAN) VALUES ('$nama', '$urut')");

		if($query) {
			echo "<script>alert('Surat berhasil ditambahkan')
                    location.replace('../letter')</script>";
		} else {
			echo "<script>alert('Surat gagal ditambahkan')</script>";
		}
	}

	function fetchByOneLetter($id) {
		if($id == "" || $id == null) {
			header("Location: ../../404");
		} else {
			$query = $this->query("SELECT * FROM sir_surat WHERE S_ID = '$id'");
			$row   = $query->fetch_assoc();

			return $row;
		}
	}

	function updateByOneLetter($letters) {
		$id    = $this->clean_all($letters["bigId"]);
		$nama  = $this->clean_post($letters["nama"]);

		$query = $this->query("UPDATE sir_surat SET S_NAMA = '$nama' WHERE S_ID = '$id'");

		if($query) {
			echo "<script>alert('Surat berhasil diubah')
                    location.replace('../letter')</script>";
		} else {
			echo "<script>alert('Surat gagal diubah')</script>";
		}
	}

	function delete_letter($id) {
		$query  = $this->query("DELETE FROM sir_surat WHERE S_ID = '$id'");

		if($query) {
			//count data urut
			$sql = $this->query("SELECT COUNT(S_ID) AS jml FROM sir_surat");
			$row = $sql->fetch_assoc();
			$urut= intval($row["jml"]);

			// max data surat
			$qq   = $this->query("SELECT MAX(S_ID) AS maxId FROM sir_surat");
			$rows = $qq->fetch_assoc();
			$maxId= $rows["maxId"];

			$ubah = $this->query("UPDATE sir_surat SET S_URUTAN = '$urut' WHERE S_ID = '$maxId'");

			echo "<script>alert('Surat berhasil dihapus')
                    location.replace('../letter')</script>";
		} else {
			echo "<script>alert('Surat gagal dihapus')</script>";
		}
	}

	function get_gear() {
		$query = $this->query("SELECT * FROM  ts_settings WHERE TS_BIGID = '1'");
		$row   = $query->fetch_assoc();

		return $row;
	}

	function ubah_gear($gear) {
		$wilayah = $this->clean_post($gear["wilayah"]);
		$data    = $this->clean_post($gear["datawilayah"]);

		$query   = $this->query("UPDATE ts_settings SET TS_VALUE = '$wilayah', TS_KEY = '$data' WHERE TS_BIGID = '1'");

		if($query) {
			echo "<script>alert('Settings berhasil diubah')
            location.replace('../setting/wilayah')</script>";
        } else {
        	echo "<script>alert('Settings gagal diubah')</script>";
        }
	}
	
	// get letter per status di admin page

	function waiting_approvert() {
		$rows  = array();
		$query = $this->query("SELECT * FROM  sir_permintaan AS A INNER JOIN sir_warga AS B ON A.NIK = B.W_NIK INNER JOIN sir_surat AS C ON A.S_ID = C.S_ID WHERE PER_STATUS = 'WAITING_APPROVAL_RT' ORDER BY PER_CREATED_AT DESC");
		while($row  = $query->fetch_assoc()) {
			$rows[] = $row;
		}

		return $rows;
	}

	function waiting_approverw() {
		$rows  = array();
		$query = $this->query("SELECT * FROM  sir_permintaan AS A INNER JOIN sir_warga AS B ON A.NIK = B.W_NIK INNER JOIN sir_surat AS C ON A.S_ID = C.S_ID WHERE PER_STATUS = 'WAITING_APPROVAL_RW' ORDER BY PER_CREATED_AT DESC");
		while($row  = $query->fetch_assoc()) {
			$rows[] = $row;
		}

		return $rows;
	}

	function waiting_approvelurah() {
		$rows  = array();
		$query = $this->query("SELECT * FROM  sir_permintaan AS A INNER JOIN sir_warga AS B ON A.NIK = B.W_NIK INNER JOIN sir_surat AS C ON A.S_ID = C.S_ID WHERE PER_STATUS = 'WAITING_KADES' ORDER BY PER_CREATED_AT DESC");
		while($row  = $query->fetch_assoc()) {
			$rows[] = $row;
		}

		return $rows;
	}

	function reject_letter() {
		$rows  = array();
		$query = $this->query("SELECT * FROM  sir_permintaan AS A INNER JOIN sir_warga AS B ON A.NIK = B.W_NIK INNER JOIN sir_surat AS C ON A.S_ID = C.S_ID WHERE PER_STATUS = 'REJECT' ORDER BY PER_CREATED_AT DESC");
		while($row  = $query->fetch_assoc()) {
			$rows[] = $row;
		}

		return $rows;
	}

	function print_letter() {
		$rows  = array();
		$query = $this->query("SELECT * FROM  sir_permintaan AS A INNER JOIN sir_warga AS B ON A.NIK = B.W_NIK INNER JOIN sir_surat AS C ON A.S_ID = C.S_ID WHERE PER_STATUS = 'PRINT' ORDER BY PER_CREATED_AT DESC");
		while($row  = $query->fetch_assoc()) {
			$rows[] = $row;
		}

		return $rows;
	}

	function view_detail_request($id) {
		$kodeId = $this->clean_all($id);
		$v 		= $this->query("SELECT PER_ID FROM sir_permintaan WHERE PER_ID = '$kodeId'");
		$vi     = $v->fetch_assoc();
		$bigId  = $vi["PER_ID"];

		if($kodeId == "" || $bigId != $kodeId) {
			header("Location: ../../404");
		} else {
			$query = $this->query("SELECT * FROM  sir_permintaan AS A INNER JOIN sir_warga AS B ON A.NIK = B.W_NIK INNER JOIN sir_surat AS C ON A.S_ID = C.S_ID WHERE PER_ID = '$kodeId'");
			$row   = $query->fetch_assoc();

			return $row;
		}
	}

	// PENGADUAN MASALAH

	function get_pengaduanByRT() {
		$rows   = array();
		$query  = $this->query("SELECT FROM sir_pengaduan AS A INNER JOIN sir_warga AS B ON A.SP_NIK = B.W_NIK WHERE SP_STATUS = 'WAITING_APPROVAL_RT' GROUP BY A.SP_NIK ORDER BY A.SP_CREATED_AT DESC");
		while($row = $query->fetch_assoc()) {
			$rows[] = $row;
		}

		return $rows;
	}

	function get_pengaduanByRW() {
		$rows   = array();
		$query  = $this->query("SELECT FROM sir_pengaduan AS A INNER JOIN sir_warga AS B ON A.SP_NIK = B.W_NIK WHERE SP_STATUS = 'WAITING_APPROVAL_RW' GROUP BY A.SP_NIK ORDER BY A.SP_CREATED_AT DESC");
		while($row = $query->fetch_assoc()) {
			$rows[] = $row;
		}

		return $rows;
	}

	function get_pengaduanByLurah() {
		$rows   = array();
		$query  = $this->query("SELECT FROM sir_pengaduan AS A INNER JOIN sir_warga AS B ON A.SP_NIK = B.W_NIK WHERE SP_STATUS = 'WAITING_KADES' GROUP BY A.SP_NIK ORDER BY A.SP_CREATED_AT DESC");
		while($row = $query->fetch_assoc()) {
			$rows[] = $row;
		}

		return $rows;
	}

	function pengadauanSelesai() {
		$rows   = array();
		$query  = $this->query("SELECT FROM sir_pengaduan AS A INNER JOIN sir_warga AS B ON A.SP_NIK = B.W_NIK WHERE SP_STATUS = 'SELESAI' GROUP BY A.SP_NIK ORDER BY A.SP_CREATED_AT DESC");
		while($row = $query->fetch_assoc()) {
			$rows[] = $row;
		}

		return $rows;
	}

	function view_detail_pengaduan($id) {
		$kodeId = $this->clean_all($id);
		$sql    = $this->query("SELECT SP_BIGID FROM sir_pengaduan WHERE SP_BIGID = '$kodeId'");
		$row    = $sql->fetch_assoc();
		$bigId  = $row["SP_BIGID"];

		if($bigId == $kodeId) {
			$query = $this->query("SELECT * FROM sir_pengaduan WHERE SP_BIGID = '$kodeId'");
			$rows  = $query->fetch_assoc();

			return $rows;
		} else {
			header("Location: ../../404");
		}
	}

	
}
?>