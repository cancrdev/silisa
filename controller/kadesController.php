<?php
include_once '../../model/config.php';

/**
* 
*/
class KadesController extends Connection
{
	
	function __construct()
	{
		date_default_timezone_set("Asia/Jakarta");
	}

	function fetchAll() {
		$rows   = array();
		$query  = $this->query("SELECT * FROM sir_lurah A INNER JOIN sir_user B ON A.PL_USERID = B.ID_USER WHERE B.U_GROUP_RULE = 'U_KADES'");
		while($row  = $query->fetch_assoc()) {
			$rows[] = $row; 
		}

		return $rows;
	}

	function add_kades($kades) {
		$nik   	  = $this->clean_all($kades["nik"]);
		$name  	  = $this->clean_post($kades["username"]);
		$fullname = $this->clean_post($kades["fullname"]);
		$email    = $this->clean_post($kades["email"]);
		$telpon   = $this->clean_post($kades["telpon"]);
		$jabatan  = $this->clean_post($kades["jabatan"]);
		$gender   = $this->clean_post($kades["gender"]);
		$wilayah  = $this->clean_post($kades["wilayah"]);
		$pass     = md5(md5(12345678));
		$token    = base64_encode('kades'.date('YmdHis').$name);

		//get nik
		$nik      = $this->query("SELECT * FROM sir_user WHERE U_NIK = '$nik'");
		// get username
		$uname    = $this->query("SELECT * FROM sir_user WHERE U_NAME = '$name'");
		// get email & no telpon
		$mail	  = $this->query("SELECT * FROM sir_user WHERE U_EMAIL = '$email' AND U_MOBILE = '$telpon'");

		if(mysqli_num_rows($nik) > 0) {
			 echo '<div class="alert bg-pink alert-dismissible" role="alert">
                <button typ="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  NIK yang anda masukkan sudah tersedia
          		</div>';
		} elseif(mysqli_num_rows($uname) > 0) {
			 echo '<div class="alert bg-pink alert-dismissible" role="alert">
                <button typ="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  Username yang anda masukkan sudah tersedia
          </div>';
		} elseif(mysqli_num_rows($mail) > 0) {
			echo '<div class="alert bg-pink alert-dismissible" role="alert">
                <button typ="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  Email / No Telpon yang anda masukkan sudah tersedia
          </div>';
		} else {
			$query  = $this->query("INSERT INTO sir_user (U_NIK, 	U_NAME, U_FULLNAME, U_EMAIL, U_PASSWORD, U_MOBILE, U_GROUP_RULE, U_STATUS, U_REMINDER, U_KEY) VALUES ('$nik', '$name', '$fullname', '$mail', '$pass', '$telpon', 'U_KADES', '1', '0', '$token')");
			if($query) {
				$max   = $this->query("SELECT ID_USER FROM sir_user");
				$row   = $max->fetch_assoc();
				$maxId = $row["ID_USER"];
				$sql = $this->query("INSERT INTO sir_lurah (	PL_USERID, 	PL_LURAH, PL_NAMA, 	PL_MASA_JABATAN, PL_GENDER, PL_STATUS) VALUES ('$maxId', '$wilayah', '$fullname', '$jabatan', 'Laki-Laki', '1')");
				echo "<script>alert('User Lurah Added Succesfully')
                    location.replace('../official/listlurah')</script>";
			} else {
				echo "<script>alert('User Lurah Added failed')
                    location.replace('../official/listlurah')</script>";
			}
		}
	} 

	function fetchByOne($id) {
		$kodeId  = $this->clean_all($id);
		if($kodeId != "") {
			$query = $this->query("SELECT * FROM sir_user A INNER JOIN sir_lurah B ON A.ID_USER = B.PL_USERID WHERE A.ID_USER = '$kodeId'");
			if(mysqli_num_rows($query) > 0) {
				$row = $query->fetch_assoc();

				return $row;
			} else {
				echo '<div class="alert bg-pink alert-dismissible" role="alert">
                <button typ="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  Masukkan ID User yang benar
          </div>';
			}
		} else {
			echo '<div class="alert bg-pink alert-dismissible" role="alert">
                <button typ="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  Id User tidak ditemukan
          </div>';
		}
	}

	function updateStatus($id, $status) {
		$query  = $this->query("UPDATE sir_user SET U_STATUS = '0' WHERE ID_USER = '$id'");
		$sql    = $this->query("UPDATE sir_lurah SET PL_STATUS = '0' WHERE PL_USERID = '$id'");

		if($query && $sql) {
			echo "<script>alert('User Lurah status updated')
                    location.replace('../official/listlurah')</script>";
		} else {
			echo "<script>alert('User Lurah status updated failed')
                    location.replace('../official/listlurah')</script>";
		}
	}

	function fetchAllCount() {
		$query = $this->query("SELECT COUNT(PL_ID) AS jmlLurah FROM sir_lurah");
		$row   = $query->fetch_assoc();
		$data  = $row["jmlLurah"];

		return $data;
	}

	function fetchLurahAktif() {
		$query = $this->query("SELECT COUNT(PL_ID) AS aktifLurah FROM sir_lurah WHERE PL_STATUS = '1'");
		$row   = $query->fetch_assoc();
		$data  = $row["aktifLurah"];

		return $data;
	}

	function fetchLurahDeAktif() {
		$query = $this->query("SELECT COUNT(PL_ID) AS deaktifLurah FROM sir_lurah WHERE PL_STATUS = '0'");
		$row   = $query->fetch_assoc();
		$data  = $row["deaktifLurah"];

		return $data;
	}
}

?>