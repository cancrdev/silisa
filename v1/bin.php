<?php
// error_reporting(0);
session_start();
include_once '../controller/globalController.php';
include_once '../controller/loginController.php';

$log    = new globalController();
$logins = new loginController();

if(isset($_POST["sigIn"])) :
    $login["loginEmail"]    = $_POST["loginEmail"];
    $login["loginPassword"] = $_POST["loginPassword"];
    $logins->mobileLogin($login);
endif;
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sign In | <?= $log->name_app(); ?></title>
    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?= $log->base_url() ?>assets/admin/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?= $log->base_url() ?>assets/admin/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?= $log->base_url() ?>assets/admin/plugins/animate-css/animate.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <!-- Custom Css -->
    <link href="<?= $log->base_url() ?>assets/admin/css/style.css" rel="stylesheet">
</head>

<body class="login-page">
    <div class="login-box">
        
        <div class="card">
            <div class="body">
                <form id="sign_in" action="" method="POST">
                    <div class="logo">
                        <a href="javascript:void(0);">
                            <img width="100px" src="<?= $log->logo_situs(); ?>" alt="gambar logo">
                        </a>
                    </div>
                    <div class="msg">Sign in to start your session</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="loginEmail" placeholder="Username / NIK" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="loginPassword" placeholder="Password" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8 p-t-5">
                            <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                            <label for="rememberme">Remember Me</label>
                        </div>
                        <div class="col-xs-4">
                            <button class="btn btn-block bg-pink waves-effect" name="sigIn" type="submit">SIGN IN</button>
                        </div>
                    </div>
                    <div class="row m-t-15 m-b--20">
                        <div class="col-xs-6">
                            <a href="<?= $log->base_url(); ?>">Back To Website</a>
                        </div>
                        <div class="col-xs-6 align-right">
                            <a href="forgot-password">Lupa kata sandi?</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="<?= $log->base_url() ?>assets/admin/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?= $log->base_url() ?>assets/admin/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?= $log->base_url() ?>assets/admin/plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="<?= $log->base_url() ?>assets/admin/plugins/jquery-validation/jquery.validate.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <!-- Custom Js -->
    <script src="<?= $log->base_url() ?>assets/admin/js/admin.js"></script>
    <script src="<?= $log->base_url() ?>assets/admin/js/pages/examples/sign-in.js"></script>
</body>

</html>
