<?php
session_start();
error_reporting(0);
include_once 'controller/globalController.php';
include_once 'controller/news-vistorController.php';

$log = new globalController();
$gear= new NewsVisitorController;

//$base
$accesId= $_GET["q"];

// wilayah
$row    = $gear->namaLurah();
$rt     = $gear->countRT();
$rw     = $gear->countRW();
$kk1    = $gear->countKeluarga();
$male   = $gear->countLaki();
$female = $gear->countFemale();

// study
$study  = $gear->allStudy();
$work   = $gear->allWarga();
$sex    = $gear->allGender();
$meride = $gear->allMeride();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="description" content="">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <!-- Title -->
  <title><?= $log->name_app(); ?> | Open Data</title>
  <!-- Favicon -->
  <link rel="icon" href="<?= $log->base_url(); ?>assets/visitor/img/core-img/favicon.ico">
  <!-- Core Stylesheet -->
  <link rel="stylesheet" href="<?= $log->base_url(); ?>assets/visitor/style.css">
</head>

<body>
  <!-- Preloader -->
  <div class="preloader d-flex align-items-center justify-content-center">
    <div class="spinner"></div>
  </div>

  <!-- ##### Header Area Start ##### -->
  <?php include_once 'layouts/visitor/navbar.php'; ?>
  <!-- ##### Header Area End ##### -->

  <!-- ##### Breadcrumb Area Start ##### -->
  <?php if($accesId === "wilayah") : ?>
  <div class="breadcrumb-area bg-img bg-overlay jarallax" style="background-image: url('<?= $log->base_url(); ?>assets/visitor/img/bg-img/18.jpg');">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-12">
          <div class="breadcrumb-text">
            <h2>WILAYAH PEMERINTAHAN DESA</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="famie-breadcrumb">
    <div class="container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Beranda</a></li>
          <li class="breadcrumb-item active" aria-current="page">Wilayah Desa</li>
        </ol>
      </nav>
    </div>
  </div>
  <!-- ##### Breadcrumb Area End ##### -->

  <!-- ##### Contact Information Area Start ##### -->
  <section class="contact-info-area">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <!-- Section Heading -->
          <div class="section-heading text-center">
            <p>Wilayah DESA</p>
            <h2><span>SILISA</span></h2>
            <img src="<?= $log->base_url(); ?>assets/visitor/img/core-img/decor2.png" alt="">
          </div>
        </div>
      </div>

      <div class="row">

        <!-- Single Information Area -->
        <div class="col-12 col-md-12">
          <div class="single-information-area mb-100 wow fadeInUp" data-wow-delay="100ms">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">No</th>
                  <th scope="col">Kelurahan/Desa</th>
                  <th scope="col">Nama Kades</th>
                  <th scope="col">Jumlah RT</th>
                  <th scope="col">Jumlah RW</th>
                  <th scope="col">Jumlah Warga</th>
                  <th scope="col">Laki-Laki</th>
                  <th scope="col">Perempuan</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">1</th>
                  <td><?= $row["PL_LURAH"]; ?></td>
                  <td><?= $row["PL_NAMA"]; ?></td>
                  <td><?= $rt." RT"; ?></td>
                  <td><?= $rw." RW"; ?></td>
                  <td><?= $kk1." Warga"; ?></td>
                  <td><?= $male; ?></td>
                  <td><?= $female; ?></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="c-border"></div>
    </div>
  </section>
  <?php elseif($accesId === "study") : ?>
    <div class="breadcrumb-area bg-img bg-overlay jarallax" style="background-image: url('<?= $log->base_url(); ?>assets/visitor/img/bg-img/18.jpg');">
      <div class="container h-100">
        <div class="row h-100 align-items-center">
          <div class="col-12">
            <div class="breadcrumb-text">
              <h2>DATA PENDIDIKAN DESA</h2>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="famie-breadcrumb">
      <div class="container">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Beranda</a></li>
            <li class="breadcrumb-item active" aria-current="page">Pendidikan Desa</li>
          </ol>
        </nav>
      </div>
    </div>
    <!-- ##### Breadcrumb Area End ##### -->

    <!-- ##### Contact Information Area Start ##### -->
    <section class="contact-info-area">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <!-- Section Heading -->
            <div class="section-heading text-center">
              <p>Demografi Pendidikan DESA</p>
              <h2><span>SILISA</span></h2>
              <img src="<?= $log->base_url(); ?>assets/visitor/img/core-img/decor2.png" alt="">
            </div>
          </div>
        </div>

        <div class="row">

          <!-- Single Information Area -->
          <div class="col-12 col-md-12">
            <div class="single-information-area mb-100 wow fadeInUp" data-wow-delay="100ms">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">Kelompok</th>
                    <th scope="col">Jumlah Data</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no =1; foreach($study as $sts) : ?>
                  <tr>
                    <th scope="row"><?= $no++; ?></th>
                    <td><?= $sts["W_PENDIDIKAN"]; ?></td>
                    <td><?= $sts["dataS"]." Jiwa"; ?></td>
                  </tr>
                 <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="c-border"></div>
      </div>
    </section>
  <?php elseif($accesId === "work") : ?>
    <div class="breadcrumb-area bg-img bg-overlay jarallax" style="background-image: url('<?= $log->base_url(); ?>assets/visitor/img/bg-img/18.jpg');">
      <div class="container h-100">
        <div class="row h-100 align-items-center">
          <div class="col-12">
            <div class="breadcrumb-text">
              <h2>DATA PEKERJAAN DESA</h2>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="famie-breadcrumb">
      <div class="container">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Beranda</a></li>
            <li class="breadcrumb-item active" aria-current="page">Pekerjaan Desa</li>
          </ol>
        </nav>
      </div>
    </div>
    <!-- ##### Breadcrumb Area End ##### -->

    <!-- ##### Contact Information Area Start ##### -->
    <section class="contact-info-area">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <!-- Section Heading -->
            <div class="section-heading text-center">
              <p>Demografi Pekerjaan DESA</p>
              <h2><span>SILISA</span></h2>
              <img src="<?= $log->base_url(); ?>assets/visitor/img/core-img/decor2.png" alt="">
            </div>
          </div>
        </div>

        <div class="row">

          <!-- Single Information Area -->
          <div class="col-12 col-md-12">
            <div class="single-information-area mb-100 wow fadeInUp" data-wow-delay="100ms">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">Kelompok</th>
                    <th scope="col">Jumlah Data</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no =1; foreach($work as $st) : ?>
                  <tr>
                    <th scope="row"><?= $no++; ?></th>
                    <td><?= $st["W_PEKERJAAN"]; ?></td>
                    <td><?= $st["data"]." Jiwa"; ?></td>
                  </tr>
                 <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="c-border"></div>
      </div>
    </section>
  <?php elseif($accesId === "gender") : ?>
    <div class="breadcrumb-area bg-img bg-overlay jarallax" style="background-image: url('<?= $log->base_url(); ?>assets/visitor/img/bg-img/18.jpg');">
      <div class="container h-100">
        <div class="row h-100 align-items-center">
          <div class="col-12">
            <div class="breadcrumb-text">
              <h2>DATA JENIS KELAMIN DESA</h2>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="famie-breadcrumb">
      <div class="container">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Beranda</a></li>
            <li class="breadcrumb-item active" aria-current="page">Jenis Kelamin Desa</li>
          </ol>
        </nav>
      </div>
    </div>
    <!-- ##### Breadcrumb Area End ##### -->

    <!-- ##### Contact Information Area Start ##### -->
    <section class="contact-info-area">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <!-- Section Heading -->
            <div class="section-heading text-center">
              <p>Demografi Jenis Kelamin DESA</p>
              <h2><span>SILISA</span></h2>
              <img src="<?= $log->base_url(); ?>assets/visitor/img/core-img/decor2.png" alt="">
            </div>
          </div>
        </div>

        <div class="row">

          <!-- Single Information Area -->
          <div class="col-12 col-md-12">
            <div class="single-information-area mb-100 wow fadeInUp" data-wow-delay="100ms">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">Gender</th>
                    <th scope="col">Jumlah Data</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no =1; foreach($sex as $stf) : ?>
                  <tr>
                    <th scope="row"><?= $no++; ?></th>
                    <td><?= $stf["W_JK"]; ?></td>
                    <td><?= $stf["dataJk"]." Jiwa"; ?></td>
                  </tr>
                 <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="c-border"></div>
      </div>
    </section>
  <?php elseif($accesId === "marriage") : ?>
    <div class="breadcrumb-area bg-img bg-overlay jarallax" style="background-image: url('<?= $log->base_url(); ?>assets/visitor/img/bg-img/18.jpg');">
      <div class="container h-100">
        <div class="row h-100 align-items-center">
          <div class="col-12">
            <div class="breadcrumb-text">
              <h2>DATA PERKAWINAN DESA</h2>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="famie-breadcrumb">
      <div class="container">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Beranda</a></li>
            <li class="breadcrumb-item active" aria-current="page">Data Perkawinan Desa</li>
          </ol>
        </nav>
      </div>
    </div>
    <!-- ##### Breadcrumb Area End ##### -->

    <!-- ##### Contact Information Area Start ##### -->
    <section class="contact-info-area">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <!-- Section Heading -->
            <div class="section-heading text-center">
              <p>Demografi DAta perkawinan DESA</p>
              <h2><span>SILISA</span></h2>
              <img src="<?= $log->base_url(); ?>assets/visitor/img/core-img/decor2.png" alt="">
            </div>
          </div>
        </div>

        <div class="row">

          <!-- Single Information Area -->
          <div class="col-12 col-md-12">
            <div class="single-information-area mb-100 wow fadeInUp" data-wow-delay="100ms">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">Gender</th>
                    <th scope="col">Jumlah Data</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no =1; foreach($meride as $stf) : ?>
                  <tr>
                    <th scope="row"><?= $no++; ?></th>
                    <td><?= $stf["W_STATUS"]; ?></td>
                    <td><?= $stf["dataM"]." Jiwa"; ?></td>
                  </tr>
                 <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="c-border"></div>
      </div>
    </section>
  <?php else : ?>
    <div class="breadcrumb-area bg-img bg-overlay jarallax" style="background-image: url('<?= $log->base_url(); ?>assets/visitor/img/bg-img/18.jpg');">
      <div class="container h-100">
        <div class="row h-100 align-items-center">
          <div class="col-12">
            <div class="breadcrumb-text">
              <h2>SILISA</h2>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="famie-breadcrumb">
      <div class="container">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Beranda</a></li>
            <li class="breadcrumb-item active" aria-current="page">Not Found</li>
          </ol>
        </nav>
      </div>
    </div>
    <!-- ##### Breadcrumb Area End ##### -->

    <!-- ##### Contact Information Area Start ##### -->
    <section class="contact-info-area">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <!-- Section Heading -->
            <div class="section-heading text-center">
              <!-- <p>Pendidikan DESA</p> -->
              <h2><span>SILISA</span></h2>
              <img src="<?= $log->base_url(); ?>assets/visitor/img/core-img/decor2.png" alt="">
            </div>
          </div>
        </div>

        <div class="row">

          <!-- Single Information Area -->
          <div class="col-12 col-md-12">
            <div class="single-information-area mb-100 wow fadeInUp" data-wow-delay="100ms">
              <h3 class="text-center">Data tidak ditemukan</h3>
            </div>
          </div>
        </div>
        <div class="c-border"></div>
      </div>
    </section>
  <?php endif; ?>
  <!-- ##### Footer Area Start ##### -->
  <?php include_once 'layouts/visitor/footer.php'; ?>
  <!-- ##### Footer Area End ##### -->

  <!-- ##### All Javascript Files ##### -->
  <!-- jquery 2.2.4  -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jquery.min.js"></script>
  <!-- Popper js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/popper.min.js"></script>
  <!-- Bootstrap js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/bootstrap.min.js"></script>
  <!-- Owl Carousel js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/owl.carousel.min.js"></script>
  <!-- Classynav -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/classynav.js"></script>
  <!-- Wow js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/wow.min.js"></script>
  <!-- Sticky js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jquery.sticky.js"></script>
  <!-- Magnific Popup js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jquery.magnific-popup.min.js"></script>
  <!-- Scrollup js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jquery.scrollup.min.js"></script>
  <!-- Jarallax js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jarallax.min.js"></script>
  <!-- Jarallax Video js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jarallax-video.min.js"></script>
  <!-- Active js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/active.js"></script>
</body>

</html>