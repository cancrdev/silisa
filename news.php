<?php
session_start();
error_reporting(0);
include_once 'controller/globalController.php';
include_once 'controller/news-vistorController.php';

$log  = new globalController();
$news = new NewsVisitorController();

$page  = (isset($_GET['page']))? $_GET['page'] : 1;
$limit = 4;

$limit_start = ($page - 1) * $limit;

$data   = $news->limitPage($limit_start, $limit);
$berita = $news->limitData();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="description" content="">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <!-- Title -->
  <title><?= $log->name_app(); ?> | News</title>
  <!-- Favicon -->
  <link rel="icon" href="<?= $log->base_url(); ?>assets/visitor/img/core-img/favicon.ico">
  <!-- Core Stylesheet -->
  <link rel="stylesheet" href="<?= $log->base_url(); ?>assets/visitor/style.css">
</head>

<body>
  <!-- Preloader -->
  <div class="preloader d-flex align-items-center justify-content-center">
    <div class="spinner"></div>
  </div>

  <!-- ##### Header Area Start ##### -->
  <?php include_once 'layouts/visitor/navbar.php'; ?>
  <!-- ##### Header Area End ##### -->

  <!-- ##### Breadcrumb Area Start ##### -->
  <div class="breadcrumb-area bg-img bg-overlay jarallax" style="background-image: url('<?= $log->base_url(); ?>assets/visitor/img/bg-img/18.jpg');">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-12">
          <div class="breadcrumb-text">
            <h2>Berita</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="famie-breadcrumb">
    <div class="container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.html"><i class="fa fa-home"></i> Beranda</a></li>
          <li class="breadcrumb-item active" aria-current="page">Berita</li>
        </ol>
      </nav>
    </div>
  </div>
  <!-- ##### Breadcrumb Area End ##### -->

  <!-- ##### Blog Area Start ##### -->
  <section class="famie-blog-area">
    <div class="container">
      <div class="row">
        <!-- Posts Area -->
        <div class="col-12 col-md-8">
          <div class="posts-area">

            <!-- Single Blog Post Area -->
            <?php $no = $limit_start + 1; foreach($data as $ne) : 
            $pecah    = substr($ne["N_SUBJECT"], 0, 305);
            $lin      = $ne["N_ID"];
            ?>
            <div class="single-blog-post-area mb-50 wow fadeInUp" data-wow-delay="100ms">
              <h6>Dipublikasi <a href="#" class="post-date"><?= $log->TanggalIndo($ne["N_TANGGAL"]) ?></a> / <a href="#" class="post-author"><?= $ne["N_CREATED_BY"]; ?></a></h6>
              <a href="#" class="post-title"><?= $ne["N_TITLE"]; ?></a>
              <img src="<?= $log->base_url(); ?>assets/news/<?= $ne["N_IMAGE"]; ?>" alt="" class="post-thumb">
              <p class="post-excerpt">
                <?= $pecah." ...<br><a class='btn famie-btn mt-4' data-animation='bounceInUp' data-delay='600ms' href='news-detail?key=$lin'>Read More</a>";
                 ?>
              </p>
            </div>
           <?php endforeach; ?>
          </div>

          <!-- pagination -->
          <nav>
            <ul class="pagination wow fadeInUp" data-wow-delay="900ms">
              <?php if($page == 1) : ?>
              <li class="page-item active"><a class="page-link" href="news?page=1">1</a></li>
              <?php else : ?>
              <li class="page-item"><a class="page-link" href="#"><?= $link_prev = ($page > 1)? $page - 1 : 1 ?></a></li>
              <li class="page-item"><a class="page-link" href="#"><i class="fa fa-angle-right"></i></a></li>
              <?php endif; ?>
            </ul>
          </nav>
        </div>

        <!-- Sidebar Area -->
        <div class="col-12 col-md-4">
          <div class="sidebar-area">

            <!-- Single Widget Area -->
           <!--  <div class="single-widget-area">
              <form action="#" method="get" class="search-widget-form">
                <input type="search" name="search" class="form-control" placeholder="Search">
                <button type="submit" name="cari"><i class="icon_search" aria-hidden="true"></i></button>
              </form>
            </div> -->

            <!-- Single Widget Area -->
            <div class="single-widget-area">
              <!-- Title -->
              <h5 class="widget-title">Recent News</h5>

              <!-- Single Recent News Start -->
              <?php foreach($berita as $br) { ?>
              <div class="single-recent-blog style-2 d-flex align-items-center">
                <div class="post-thumbnail">
                  <img src="<?= $log->base_url(); ?>assets/news/<?= $br["N_IMAGE"]; ?>" alt="">
                </div>
                <div class="post-content">
                  <a href="#" class="post-title"><?= $br["N_TITLE"]; ?></a>
                  <div class="post-date"><?= $log->TanggalIndo($br["N_TANGGAL"]); ?></div>
                </div>
              </div>
              <?php } ?>

              <!-- Single Recent News Start -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- ##### Blog Area End ##### -->

  <!-- ##### Footer Area Start ##### -->
  <?php include_once 'layouts/visitor/footer.php'; ?>
  <!-- ##### Footer Area End ##### -->

  <!-- ##### All Javascript Files ##### -->
  <!-- jquery 2.2.4  -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jquery.min.js"></script>
  <!-- Popper js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/popper.min.js"></script>
  <!-- Bootstrap js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/bootstrap.min.js"></script>
  <!-- Owl Carousel js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/owl.carousel.min.js"></script>
  <!-- Classynav -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/classynav.js"></script>
  <!-- Wow js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/wow.min.js"></script>
  <!-- Sticky js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jquery.sticky.js"></script>
  <!-- Magnific Popup js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jquery.magnific-popup.min.js"></script>
  <!-- Scrollup js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jquery.scrollup.min.js"></script>
  <!-- Jarallax js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jarallax.min.js"></script>
  <!-- Jarallax Video js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jarallax-video.min.js"></script>
  <!-- Active js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/active.js"></script>
</body>

</html>