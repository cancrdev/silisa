<?php
session_start();
include_once 'controller/globalController.php';
include_once 'controller/news-vistorController.php';

$log = new globalController();
$gear= new NewsVisitorController;

$row = $gear->fetchSettingsVisit();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="description" content="">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <!-- Title -->
  <title><?= $log->name_app(); ?> | Contact</title>
  <!-- Favicon -->
  <link rel="icon" href="<?= $log->base_url(); ?>assets/visitor/img/core-img/favicon.ico">
  <!-- Core Stylesheet -->
  <link rel="stylesheet" href="<?= $log->base_url(); ?>assets/visitor/style.css">
</head>

<body>
  <!-- Preloader -->
  <div class="preloader d-flex align-items-center justify-content-center">
    <div class="spinner"></div>
  </div>

  <!-- ##### Header Area Start ##### -->
  <?php include_once 'layouts/visitor/navbar.php'; ?>
  <!-- ##### Header Area End ##### -->

  <!-- ##### Breadcrumb Area Start ##### -->
  <div class="breadcrumb-area bg-img bg-overlay jarallax" style="background-image: url('<?= $log->base_url(); ?>assets/visitor/img/bg-img/18.jpg');">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-12">
          <div class="breadcrumb-text">
            <h2>PKK (pembinaan kesejahteraan keluarga)</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="famie-breadcrumb">
    <div class="container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Beranda</a></li>
          <li class="breadcrumb-item active" aria-current="page">PKK Desa</li>
        </ol>
      </nav>
    </div>
  </div>
  <!-- ##### Breadcrumb Area End ##### -->

  <!-- ##### Contact Information Area Start ##### -->
  <section class="contact-info-area">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <!-- Section Heading -->
          <div class="section-heading text-center">
            <p>PKK DESA</p>
            <h2><span>Desaku</span></h2>
            <img src="<?= $log->base_url(); ?>assets/visitor/img/core-img/decor2.png" alt="">
          </div>
        </div>
      </div>

      <div class="row">

        <!-- Single Information Area -->
        <div class="col-12 col-md-12">
          <div class="single-information-area mb-100 wow fadeInUp" data-wow-delay="100ms">
           <h5>
             <?= $row["TS_PKK"]; ?>
           </h5>
          </div>
        </div>
      </div>
      <div class="c-border"></div>
    </div>
  </section>

  <!-- ##### Footer Area Start ##### -->
  <?php include_once 'layouts/visitor/footer.php'; ?>
  <!-- ##### Footer Area End ##### -->

  <!-- ##### All Javascript Files ##### -->
  <!-- jquery 2.2.4  -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jquery.min.js"></script>
  <!-- Popper js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/popper.min.js"></script>
  <!-- Bootstrap js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/bootstrap.min.js"></script>
  <!-- Owl Carousel js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/owl.carousel.min.js"></script>
  <!-- Classynav -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/classynav.js"></script>
  <!-- Wow js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/wow.min.js"></script>
  <!-- Sticky js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jquery.sticky.js"></script>
  <!-- Magnific Popup js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jquery.magnific-popup.min.js"></script>
  <!-- Scrollup js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jquery.scrollup.min.js"></script>
  <!-- Jarallax js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jarallax.min.js"></script>
  <!-- Jarallax Video js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jarallax-video.min.js"></script>
  <!-- Active js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/active.js"></script>
</body>

</html>