<?php
error_reporting(0);
session_start();

include_once '../../controller/globalController.php';
include_once '../../controller/rtController.php';
include_once '../../controller/adminController.php';
include_once '../../controller/wargaController.php';

$log     = new globalController();
$rt      = new RtController();

$surat   = new AdminController();
$warga   = new WargaController();

$let     = $surat->fetchAllLetter();
$wrga    = $warga->fetchAll();
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?= $log->name_app(); ?> | Dashboard Pemerintahan Desa</title>
    <!-- Favicon-->
    <link rel="icon" href="" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Sweet Alert Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/sweetalert/sweetalert.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?= $log->base_url(); ?>assets/admin/css/themes/all-themes.css" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p><?= $log->loading_apps(); ?></p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <?php include_once '../../layouts/official/navbar.php'; ?>
    <!-- #Top Bar -->
    <?php include_once '../../layouts/official/sidebar.php'; ?>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>
                    FORM Tambah Surat Offline
                </h2>
            </div>
            <!-- Advanced Form Example With Validation -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Tambah Surat offline</h2>
                        </div>
                        <div class="body">
                            <form id="wizard_with_validation" method="POST">
                                <h3>Surat Information</h3>
                                <fieldset>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <select class="form-control show-tick" required>
                                                <?php foreach($let as $key) : ?>
                                                <option value="<?= $key["S_ID"]; ?>"><?= $key["S_NAMA"]; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div><br>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="password" required>
                                            <label class="form-label">Keterangan*</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" onkeyup="angka(this);" name="conf" required>
                                            <label class="form-label">Lampiran*</label>
                                        </div>
                                    </div>
                                </fieldset>

                                <h3>Profile Information</h3>
                                <fieldset>
                                     <div class="form-group">
                                        <div class="form-line">
                                            <select class="form-control show-tick" name="nik" id="id" onchange="cek_database()" required>
                                                <?php foreach($wrga as $key) { ?>
                                                <option value="<?= $key["W_NIK"]; ?>"><?= $key["W_NIK"]; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" name="no_kk" id="no_kk" class="form-control" required>
                                            <label class="form-label">No KK*</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" name="nama_warga" id="nama_warga" class="form-control" required>
                                            <label class="form-label">Nama Warga*</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <textarea name="alamat" cols="30" rows="3" id="alamat" class="form-control no-resize" required></textarea>
                                            <label class="form-label">Alamat*</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input min="18" type="number" id="sex" name="age" class="form-control" required>
                                            <label class="form-label">Jenis Kelamin*</label>
                                        </div>
                                        <!-- <div class="help-info">The warning step will show up if age is less than 18</div> -->
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input min="18" type="number" id="rt" name="age" class="form-control" required>
                                            <label class="form-label">RT</label>
                                        </div>
                                        <!-- <div class="help-info">The warning step will show up if age is less than 18</div> -->
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input min="18" type="number" id="rw" name="age" class="form-control" required>
                                            <label class="form-label">RW*</label>
                                        </div>
                                        <!-- <div class="help-info">The warning step will show up if age is less than 18</div> -->
                                    </div>
                                </fieldset>
                                <h3>Terms & Conditions - Finish</h3>
                                <fieldset>
                                    <input id="acceptTerms-2" name="acceptTerms" type="checkbox" required>
                                    <label for="acceptTerms-2">I agree with the Terms and Conditions.</label>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Advanced Form Example With Validation -->
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Jquery Validation Plugin Css -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- JQuery Steps Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-steps/jquery.steps.js"></script>

    <!-- Sweet Alert Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/node-waves/waves.js"></script>

    <!-- Custom Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/js/admin.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/js/pages/forms/form-wizard.js"></script>

    <!-- Demo Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/js/demo.js"></script>
    <script type="text/javascript">
        function angka(e) {
          if (!/^[0-9]+$/.test(e.value)) {
            e.value = e.value.substring(0,e.value.length-1);
          }
        }

        function cek_database() {
            var kodeId = $("#id").val();
            $.ajax({
                url: '<?= $log->base_url(); ?>goverment/letter/view-data?kode',
                data:"kode="+kodeId,
            }).success(function (data) {
                    var json = data,
                    obj = JSON.parse(json);
                    $('#no_kk').val(obj.no_kk);
                    $('#nama_warga').val(obj.nama_warga);
                    $('#alamat').val(obj.alamat);
                    $('#sex').val(obj.sex);
                    $('#rt').val(obj.rt);
                    $('#rw').val(obj.rw);
                });
            }
    </script>
</body>
</html>
