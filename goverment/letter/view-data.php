<?php
// error_reporting(0);
session_start();
include_once '../../controller/wargaController.php';

$warga   = new WargaController();

$data    = $warga->fetchByNIK($_GET["kode"]);
$array   = array(
	          'no_kk'      => $data["W_KK"],
	          'nama_warga' => $data["W_NAMA"],
	          'alamat'     => $data['W_ALAMAT'],
	          'sex'		   => $data['W_JK'],
	          'rt'         => $data['W_RT'],
	          'rw'		   => $data['W_RW']
           );
echo json_encode($array);
?>