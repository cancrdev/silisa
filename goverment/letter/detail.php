<?php
error_reporting(0);
session_start();

include_once '../../controller/globalController.php';
include_once '../../controller/rtController.php';

$log  = new globalController();
$rt      = new RtController();

$waiting  = $rt->get_letter_waiting($_SESSION["P_RT"]);
$detail   = $rt->fetch_detail_letter_wait($_GET["view"]);
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?= $log->name_app(); ?> | Dashboard Pemerintahan Desa</title>
    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?= $log->base_url(); ?>assets/admin/css/themes/all-themes.css" rel="stylesheet" />
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p><?= $log->loading_apps(); ?></p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <?php include_once '../../layouts/official/navbar.php'; ?>
    <!-- #Top Bar -->
    <?php include_once '../../layouts/official/sidebar.php'; ?>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>TABS</h2>
            </div>
            <!-- Tabs With Custom Animations -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Detail Permintaan No. <?= $detail["NO_SURAT"]; ?> A/N <?= $detail["PER_NAMA_WARGA"]; ?>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        <li role="presentation" class="active"><a href="#home_animation_2" data-toggle="tab">PROFIL</a></li>
                                        <li role="presentation"><a href="#profile_animation_2" data-toggle="tab">SURAT</a></li>
                                        <li role="presentation"><a href="#messages_animation_2" data-toggle="tab">KETERANGAN</a></li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane animated fadeInRight active" id="home_animation_2">
                                            <b>Profil Content</b><br>
                                            <table width="100%">
                                                <tr>
                                                    <td>Nama Warga</td>
                                                    <td> : </td>
                                                    <td><?= $detail["PER_NAMA_WARGA"]; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>No NIK</td>
                                                    <td> : </td>
                                                    <td><?= $detail["NIK"]; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>No KK</td>
                                                    <td> : </td>
                                                    <td><?= $detail["PER_KK"]; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>RT/RW</td>
                                                    <td> : </td>
                                                    <td><?= '0'.$detail["PM_RT"].'/0'.$detail["PM_RW"]; ?></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div role="tabpanel" class="tab-pane animated fadeInRight" id="profile_animation_2">
                                            <b>Surat Content</b>
                                            <table width="100%">
                                                <tr>
                                                    <td>No Surat</td>
                                                    <td> : </td>
                                                    <td><?= $detail["NO_SURAT"]; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Lampiran</td>
                                                    <td> : </td>
                                                    <td><?= $detail["PER_LAMPIRAN"]; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Keperluan</td>
                                                    <td> : </td>
                                                    <td><?= $detail["PER_KEPERLUAN"]; ?></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div role="tabpanel" class="tab-pane animated fadeInRight" id="messages_animation_2">
                                            <b>Keterangan Content</b>
                                            <table>
                                                <tr>
                                                    <td>Status</td>
                                                    <td> : </td>
                                                    <td><span class="bg-green"><?= $detail["PER_STATUS"]; ?></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Dibuat</td>
                                                    <td> : </td>
                                                    <td><?= $log->TanggalIndo($detail["PER_CREATED_AT"]); ?></td>
                                                </tr>
                                            </table>
                                        </div><br>
                                        <a href="../letter" class="btn bg-light-blue waves-effect"><i class="material-icons">undo</i> Kembali</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Tabs With Custom Animations -->
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/node-waves/waves.js"></script>

    <!-- Custom Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/js/admin.js"></script>

    <!-- Demo Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/js/demo.js"></script>
</body>

</html>
