<?php
error_reporting(0);
session_start();

include_once '../../controller/globalController.php';
include_once '../../controller/rtController.php';

$log  = new globalController();
$rt      = new RtController();

if (isset($_POST["cari"])) {
    $lap["awal"]  = $_POST["awal"];
    $lap["akhir"] = $_POST["akhir"];
    $data = $rt->get_slow_report($lap);
    $awal = $log->TanggalIndo($_POST["awal"]);
    $akhir= $log->TanggalIndo($_POST["akhir"]);

    $target = 'awal=' . $_POST['awal'] .'&akhir=' . $_POST['akhir'];
    $range = "Dari Tanggal $awal sampai $akhir";
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?= $log->name_app(); ?> | Dashboard Pemerintahan Desa</title>
    <!-- Favicon-->
    <link rel="icon" href="" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

     <!-- Bootstrap Material Datetime Picker Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />

     <link href="<?= $log->base_url(); ?>assets/admin/plugins/waitme/waitMe.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?= $log->base_url(); ?>assets/admin/css/themes/all-themes.css" rel="stylesheet" />
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p><?= $log->loading_apps(); ?></p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <?php include_once '../../layouts/official/navbar.php'; ?>
    <!-- #Top Bar -->
    <?php include_once '../../layouts/official/sidebar.php'; ?>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>
                    Report Data Surat <?= $range ?>
                    <small><b>Taken from Ketua RT 0<?= $_SESSION["P_RT"]; ?></b></small>
                </h2>
            </div>
            <div>
            </div>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                List Data Warga
                            </h2>
                        </div>
                        <div class="body">
                            <form action="" method="post">
                                <div class="form-group">
                                    <label for="">Tanggal</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <div class="form-line">
                                                    <input type="text" name="awal" id="awal" required class="form-control">
                                                </div>
                                                <div class="input-group-addon bg-gray"> s/d &nbsp;&nbsp;&nbsp;&nbsp;</div> 
                                                <div class="form-line">
                                                    <input type="text" name="akhir" id="akhir" required class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <button type="submit" name="cari" class="btn btn-info"><i class="material-icons">search</i> Cari Data</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>No Surat</th>
                                            <th>Keperluan</th>
                                            <th>KK - NIK</th>
                                            <th>Nama Warga</th>
                                            <th>Dibuat</th>
                                            <th></th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <?php if (!empty($data)): ?>
                                    <tbody>
                                        <?php $no = 1; foreach($data as $wt) : ?>
                                        <tr>
                                            <td><?= $no++; ?></td>
                                            <td><?= $wt["NO_SURAT"]; ?></td>
                                            <td><?= $wt["PER_KEPERLUAN"]; ?></td>
                                            <td><?= $wt["NIK"].' - '.$wt["PER_KK"]; ?></td>
                                            <td><?= $wt["PER_NAMA_WARGA"]; ?></td>
                                            <td><?= $log->Tanggalindo($wt["PER_CREATED_AT"]); ?></td>
                                            <td></td>
                                            <td>
                                                <a href="../letter/detail?view=<?= $wt["PER_ID"]; ?>" class="btn bg-indigo waves-effect"><i class="material-icons">pageview</i></a>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                    <?php else: ?>
                                    <tbody>
                                        <tr>
                                            <td colspan="5" align="center">TIDAK ADA PERMINTAAN SURAT</td>
                                        </tr>
                                    </tbody>
                                    <?php endif ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/node-waves/waves.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

     <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/momentjs/moment.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

    <!-- Custom Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/js/admin.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/js/pages/tables/jquery-datatable.js"></script>
    <!-- Demo Js -->

    <!-- Demo Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/js/demo.js"></script>
    <script>
        $(document).ready(function () {
            $('#akhir').bootstrapMaterialDatePicker({ weekStart: 1, time: false });
            $('#awal').bootstrapMaterialDatePicker({ weekStart: 1, time: false }).on('change', function(e, date) {
                $('#akhir').bootstrapMaterialDatePicker('setMinDate', date);
            });
        });
    </script>
</body>

</html>
