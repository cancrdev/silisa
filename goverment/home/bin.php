<?php
error_reporting(0);
session_start();
include_once '../../controller/globalController.php';
include_once '../../controller/rtController.php';

$log     = new globalController();
$rt      = new RtController();

//config
$waiting  = $rt->get_letter_waiting($_SESSION["P_RT"]);
$end      = $rt->get_letter_selesai($_SESSION["P_RT"]);
$aduan_w  = $rt->get_aduan_waiting($_SESSION["P_RT"]);
$aduan_end= $rt->get_aduan_selesai($_SESSION["P_RT"]);
$all      = $rt->get_warga_byRt($_SESSION["P_RT"]);

//data list
$data     = $rt->get_letter_status($_SESSION["P_RT"]);
$row      = $rt->get_aduan_status($_SESSION["P_RT"]);
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?= $log->name_app(); ?> | Dashboard Pemerintahan Desa</title>
    <!-- Favicon-->
    <link rel="icon" href="" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/morrisjs/morris.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?= $log->base_url(); ?>assets/admin/css/themes/all-themes.css" rel="stylesheet" />
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p><?= $log->loading_apps(); ?></p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <?php include_once '../../layouts/official/navbar.php'; ?>
    <!-- #Top Bar -->
    <?php include_once '../../layouts/official/sidebar.php'; ?>
    <section class="content-off">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>
            <!-- Widgets -->
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">playlist_add_check</i>
                        </div>
                        <div class="content">
                            <div class="text">Surat Menunggu Disetujui</div>
                            <div class="number count-to" data-from="0" data-to="<?= count($waiting); ?>" data-speed="15" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">help</i>
                        </div>
                        <div class="content">
                            <div class="text">Surat Siap Dicetak</div>
                            <div class="number count-to" data-from="0" data-to="<?= count($end); ?>" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">forum</i>
                        </div>
                        <div class="content">
                            <div class="text">Jumlah Warga Per RT</div>
                            <div class="number count-to" data-from="0" data-to="<?= count($all); ?>" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">person_add</i>
                        </div>
                        <div class="content">
                            <div class="text">Aduan Menunggu Diketahui</div>
                            <div class="number count-to" data-from="0" data-to="<?= count($aduan_w); ?>" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="card">
                        <div class="header">
                            <h2>PERMINTAAN SURAT TERBARU</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>No Surat</th>
                                            <th>Perihal</th>
                                            <th>Dari</th>
                                            <th>Progress</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1; foreach($data as $dt): ?>
                                        <tr>
                                            <td><?= $no++; ?></td>
                                            <td><?= $dt["NO_SURAT"]; ?></td>
                                            <td><span class="label bg-green"><?= $dt["PER_KEPERLUAN"]; ?></span></td>
                                            <td><?= $dt["W_NAMA"]; ?></td>
                                            <td>
                                                <div class="progress">
                                                    <?php if($dt["PER_STATUS"] == "WAITING_APPROVAL_RT") : ?>
                                                    <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 62%"></div>
                                                    <?php elseif($dt["PER_STATUS"] == "WAITING_APPROVAL_RW") : ?>
                                                    <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 62%"></div>
                                                    <?php elseif($dt["PER_STATUS"] == "WAITING_KADES") : ?>
                                                    <div class="progress-bar bg-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 62%"></div>
                                                    <?php elseif($dt["PER_STATUS"] == "REJECT") : ?>
                                                    <div class="progress-bar bg-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 62%"></div>
                                                    <?php elseif($dt["PER_STATUS"] == "PRINT") : ?>
                                                    <div class="progress-bar bg-green" role="progressbar" aria-valuenow="100" aria-valuemin="100" aria-valuemax="100" style="width: 62%"></div>
                                                    <?php endif; ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="card">
                        <div class="header">
                            <h2>INFO PENGADUAN TERBARU</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>No Pengaduan</th>
                                            <th>Perihal</th>
                                            <th>Dari</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1; foreach($row as $rws) : ?>
                                        <tr>
                                            <td><?= $no++; ?></td>
                                            <td><?= $rws["SP_NOMER"]; ?></td>
                                            <td><span class="label bg-green"><?= $rws["SP_KETERANGAN"]; ?></span></td>
                                            <td><?= $rws["W_NAMA"]; ?></td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
      
    <!-- Waves Effect Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/raphael/raphael.min.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/morrisjs/morris.js"></script>

    <!-- ChartJs -->
    <script src="<?= $log->base_url(); ?>assets/admin/js/chartjs/Chart.bundle.js"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-sparkline/jquery.sparkline.js"></script>

    <!-- Custom Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/js/admin.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/js/pages/index.js"></script>

    <!-- Demo Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/js/demo.js"></script>
</body>

</html>
