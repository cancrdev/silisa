<?php
if(isset($_GET["q"]) != "prov") {
    include_once '../../model/connect.php';
} else {
    include_once '../model/connect.php';
}

$sessId = $_SESSION["ID_USER"];
$avat = mysqli_query($conn, "SELECT U_AVATAR FROM sir_user WHERE ID_USER = '$sessId'");
$vrow = mysqli_fetch_assoc($avat);
?>
<section>
    <aside id="leftsidebar" class="sidebar">
                <!-- User Info -->
        <div class="user-info" style="background-color: <?= $log->background(); ?>">
            <div class="image">
                <?php if(isset($_GET["q"]) != "prov") : ?>
                <?php if($vrow["U_AVATAR"] == "" || $vrow["U_AVATAR"] == null) : ?>
                <img src="<?= $log->base_url(); ?>assets/admin/images/user.png" width="48" height="48" alt="User" />
                <?php else : ?>
                <img src="<?= $log->base_url(); ?>assets/avatar/<?= $vrow["U_AVATAR"]; ?>" width="48" height="48" alt="User" />
                <?php endif; else : ?>
                <?php if($vrow["U_AVATAR"] == "" || $vrow["U_AVATAR"] == null) : ?>
                <img src="<?= $log->base_url(); ?>assets/admin/images/user.png" width="48" height="48" alt="User" />
                <?php else : ?>
                <img src="<?= $log->base_url(); ?>assets/avatar/<?= $vrow["U_AVATAR"]; ?>" width="48" height="48" alt="User" />
                <?php endif; endif; ?>
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= $_SESSION["U_FULLNAME"]; ?></div>
                <div class="email"><?= $_SESSION["U_EMAIL"]; ?></div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">DASHBOARD INFORMATION</li>
                <li class="active">
                    <a href="<?= $log->base_url(); ?>admin/home">
                        <i class="material-icons">home</i>
                        <span>Home</span>
                    </a>
                </li>
                <li>
                    <a href="<?= $log->base_url(); ?>admin/news">
                        <i class="material-icons">assessment</i>
                        <span>Master Berita</span>
                    </a>
                </li>
                <li>
                    <a href="<?= $log->base_url(); ?>admin/letter">
                        <i class="material-icons">chrome_reader_mode</i>
                        <span>Master Surat</span>
                    </a>
                </li>
                <li>
                    <a href="<?= $log->base_url(); ?>admin/slider">
                        <i class="material-icons">wallpaper</i>
                        <span>Master Slider</span>
                    </a>
                </li>
                <li>
                    <a href="<?= $log->base_url(); ?>admin/warga">
                        <i class="material-icons">group</i>
                        <span>Master Warga</span>
                    </a>
                </li>
                <li>
                    <a href="<?= $log->base_url(); ?>admin/official/listrt"><i class="material-icons">people</i><span>Data RT</span></a>
                </li>
                <li>
                    <a href="<?= $log->base_url(); ?>admin/official/listrw"><i class="material-icons">people</i><span>Data RW</span></a>
                </li>
                <li>
                    <a href="<?= $log->base_url(); ?>admin/official/listlurah"><i class="material-icons">people</i><span>Data Lurah</span></a>
                </li>
                <li>
                    <a href="<?= $log->base_url(); ?>admin/request/print"><i class="material-icons">assignment</i>
                        <span>Surat Siap Dicetak</span></a>
                </li>
                <li>
                    <a href="<?= $log->base_url(); ?>admin/request/reject"><i class="material-icons">assignment</i><span>Surat Direject</span></a>
                </li>
                <li>
                    <a href="<?= $log->base_url(); ?>admin/request/waitingkades"><i class="material-icons">assignment</i><span>Menunggu Konfirmasi Kades</span></a>
                </li>
                <li>
                    <a href="<?= $log->base_url(); ?>admin/request/waitingrw"><i class="material-icons">assignment</i><span>Menunggu Konfirmasi RW</span></a>
                </li>
                <li>
                    <a href="<?= $log->base_url(); ?>admin/request/waitingrt"><i class="material-icons">assignment</i><span>Menunggu Konfirmasi RT</span></a>
                </li>
                <li>
                    <a href="<?= $log->base_url(); ?>admin/pengaduan/waitingkades"><i class="material-icons">widgets</i>Pengaduan Diproses Kades</a>
                </li>
                <li>
                    <a href="<?= $log->base_url(); ?>admin/pengaduan/waitingrw"><i class="material-icons">widgets</i>Pengaduan Diproses RW</a>
                </li>
                <li>
                    <a href="<?= $log->base_url(); ?>admin/pengaduan/waitingrt"><i class="material-icons">widgets</i>Pengaduan Diproses RT</a>
                </li>
                <li>
                    <a href="<?= $log->base_url(); ?>admin/pengaduan/complaint-end"><i class="material-icons">widgets</i>Pengaduan Selesai</a>
                </li>
                <li>
                    <a href="<?= $log->base_url(); ?>admin/pengaduan/complaint-reject"><i class="material-icons">widgets</i>Pengaduan Ditolak</a>
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">settings</i>
                        <span>Settings</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="<?= $log->base_url(); ?>admin/setting">Sejarah Desa</a>
                        </li>
                        <li>
                            <a href="<?= $log->base_url(); ?>admin/setting/profile-desa">Profil Desa</a>
                        </li>
                        <li>
                            <a href="<?= $log->base_url(); ?>admin/setting/visi-misi">Visi & Misi</a>
                        </li>
                        <li>
                            <a href="<?= $log->base_url(); ?>admin/setting/peta-desa">Peta Desa</a>
                        </li>
                        <li>
                            <a href="<?= $log->base_url(); ?>admin/setting/lpm">LPM</a>
                        </li>
                        <li>
                            <a href="<?= $log->base_url(); ?>admin/setting/karang-taruna">Karang Taruna</a>
                        </li>
                        <li>
                            <a href="<?= $log->base_url(); ?>admin/setting/pkk">PKK</a>
                        </li>
                        <li>
                            <a href="<?= $log->base_url(); ?>admin/setting/wilayah">Wilayah Desa</a>
                        </li>
                    </ul>
                </li>
               <!--  <li class="header">REPORT</li>
                <li>
                    <a href="<?= $log->base_url() ?>admin/report">
                        <i class="material-icons col-red">donut_large</i>
                        <span>Report Permintaan Surat</span>
                    </a>
                </li> -->
            </ul>
        </div>
        <!-- #Menu -->
    </aside>
</section>