<nav class="navbar" style="background-color: <?= $log->themeBackend(); ?>">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="<?= $log->base_url(); ?>admin/home"><?= $log->name_app(); ?></a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- Call Search -->
                <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li>
                <!-- #END# Call Search -->
                <!-- Notifications -->
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <i class="material-icons">person</i>
                        <!-- <span class="label-count"></span> -->
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">Profile <?= $_SESSION["U_FULLNAME"]; ?></li>
                        <li class="body">
                            <ul class="menu">
                                <li>
                                    <a href="<?= $log->base_url() ?>v1/my-profile?q=prov">
                                        <div class="icon-circle bg-light-green">
                                            <i class="material-icons">person</i>
                                        </div>
                                        <div class="menu-info">
                                            <h4>My Profile</h4>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= $log->base_url() ?>v1/settings?q=prov">
                                        <div class="icon-circle bg-cyan">
                                            <i class="material-icons">settings</i>
                                        </div>
                                        <div class="menu-info">
                                            <h4>Settings</h4>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= $log->base_url() ?>v1/logout" onclick="return confirm('Apakah anda yakin akan keluar..?');">
                                        <div class="icon-circle bg-red">
                                            <i class="material-icons">arrow_forward</i>
                                        </div>
                                        <div class="menu-info">
                                            <h4>Logout</h4>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <!-- #END# Notifications -->
                <!-- Tasks -->
                <!-- #END# Tasks -->
                <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li>
            </ul>
        </div>
    </div>
</nav>