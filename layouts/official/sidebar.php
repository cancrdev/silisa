<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info" style="background-color: <?= $log->background(); ?>">
            <div class="image">
                <img src="<?= $log->base_url(); ?>assets/admin/images/user.png" width="48" height="48" alt="User" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= $_SESSION["U_FULLNAME"]; ?></div>
                <div class="email"><?= $_SESSION["U_EMAIL"]; ?></div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">MAIN NAVIGATION</li>
                <li class="active">
                    <a href="index.html">
                        <i class="material-icons">home</i>
                        <span>Beranda</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">widgets</i>
                        <span>List Surat</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="<?= $log->base_url(); ?>goverment/letter">
                                <span>Surat Menunggu Persetujuan</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?= $log->base_url(); ?>goverment/letter/history">
                                <span>History</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?= $log->base_url(); ?>goverment/letter/selesai">Surat Proses Selesai</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">swap_calls</i>
                        <span>Pengaduan</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="<?= $log->base_url(); ?>goverment/aduan">Menunggu Tindak Lanjut RT</a>
                        </li>
                        <li>
                            <a href="<?= $log->base_url(); ?>goverment/aduan/history">History</a>
                        </li>
                        <li>
                            <a href="<?= $log->base_url(); ?>goverment/aduan/selesai">Aduan Selesai</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">assignment</i>
                        <span>Report</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="<?= $log->base_url(); ?>goverment/report">Report Surat</a>
                        </li>
                        <li>
                            <a href="<?= $log->base_url(); ?>goverment/report/aduan">Report Aduan</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">view_list</i>
                        <span>Tambah Surat Offline</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="<?= $log->base_url(); ?>goverment/letter/tambah-surat">Tambah Surat</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </aside>
    <!-- #END# Left Sidebar -->
</section>