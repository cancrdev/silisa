 <header class="header-area">
    <!-- Top Header Area -->
    <div class="top-header-area">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="top-header-content d-flex align-items-center justify-content-between">
              <!-- Top Header Content -->
              <div class="top-header-meta">
                <p>Selamat datang di website resmi <span><?= $log->name_app(); ?></span></p>
              </div>
              <!-- Top Header Content -->
              <div class="top-header-meta text-right">
                <a href="https://mail.google.com/mail/u/0/?view=cm&fs=1&to=haloo@dokterapps.com&tf=1" target="_blank" data-toggle="tooltip" data-placement="bottom" title="haloo@dokterapps.com"><i class="fa fa-envelope-o" aria-hidden="true"></i> <span>Email: hallo@dokterapps.com</span></a>
                <a href="tel:+6285555575355" data-toggle="tooltip" data-placement="bottom" title="+62855-5557-5355"><i class="fa fa-phone" aria-hidden="true"></i> <span>Call Us: +62855-5557-5355</span></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Navbar Area -->
    <div class="famie-main-menu">
      <div class="classy-nav-container breakpoint-off">
        <div class="container">
          <!-- Menu -->
          <nav class="classy-navbar justify-content-between" id="famieNav">
            <!-- Nav Brand -->
            <h4><a href="<?= $log->base_url(); ?>" class="nav-brand">
              <?= $log->name_app(); ?>
              <!-- <img src="<?= $log->base_url(); ?>assets/visitor/img/core-img/logo.png" alt=""> -->
            </a></h4>
            <!-- Navbar Toggler -->
            <div class="classy-navbar-toggler">
              <span class="navbarToggler"><span></span><span></span><span></span></span>
            </div>
            <!-- Menu -->
            <div class="classy-menu">
              <!-- Close Button -->
              <div class="classycloseIcon">
                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
              </div>
              <!-- Navbar Start -->
              <div class="classynav">
                <ul>
                  <li class="active"><a href="<?= $log->base_url(); ?>">Home</a></li>
                  <!-- <li><a href="about.html">About</a></li> -->
                  <li><a href="#">Profil Desa</a>
                    <ul class="dropdown">
                      <li><a href="<?= $log->base_url()?>history">Sejarah Desa</a></li>
                      <li><a href="<?= $log->base_url()?>profile-desa">Profil Wilayah Desa</a></li>
                      <li><a href="<?= $log->base_url()?>first">Visi & Misi</a></li>
                      <li><a href="<?= $log->base_url()?>peta-desa">Peta Desa</a></li>
                    </ul>
                  </li>
                  <li><a href="#">LemMas</a>
                    <ul class="dropdown">
                      <li><a href="<?= $log->base_url()?>lpm">LPM</a></li>
                      <li><a href="<?= $log->base_url()?>karang-taruna">Karang Taruna</a></li>
                      <li><a href="<?= $log->base_url()?>pkk">PKK</a></li>
                    </ul>
                  </li>
                  <li><a href="#">Data Desa</a>
                    <ul class="dropdown">
                      <li><a href="<?= $log->base_url()?>open-data?q=wilayah">Data Wilayah</a></li>
                      <li><a href="<?= $log->base_url()?>open-data?q=study">Data Pendidikan</a></li>
                      <li><a href="<?= $log->base_url()?>open-data?q=work">Data Pekerjaan</a></li>
                      <li><a href="<?= $log->base_url()?>open-data?q=gender">Data Jenis Kelamin</a></li>
                      <li><a href="<?= $log->base_url()?>open-data?q=marriage">Data Perkawinan</a></li>
                    </ul>
                  </li>
                  <li><a href="https://drive.google.com/open?id=1zzjeetvaH-Sy91cl3bJ5FgnEULPaJBXA" target="_blank">PPID</a></li>
                  <li><a href="#">Permintaan</a>
                    <ul class="dropdown">
                      <?php if(!empty($_SESSION["ID_USER"]) && !empty($_SESSION["U_LOGIN_TOKEN"])) { ?>
                      <li><a href="#">Surat Online</a></li>
                      <li><a href="#">Pengaduan</a></li>
                      <?php } else { ?>
                      <li><a href="v1">Surat Online</a></li>
                      <li><a href="v1">Pengaduan</a></li>
                      <li><a href="form-permintaan" target="_blank">Form Online</a></li>
                      <?php } ?>
                    </ul>
                  </li>
                  <li><a href="news">Berita</a></li>
                  <li><a href="contact">Kontak Kami</a></li>
                </ul>
                <!-- Search Icon -->
                <!-- <div id="searchIcon">
                  <i class="icon_search" aria-hidden="true"></i>
                </div> -->
                <div id="cartIcon">
                  <a href="v1">
                    <i class="icon_profile" aria-hidden="true"></i> Login
                    <!-- <span class="cart-quantity"></span> -->
                  </a>
                </div>
              </div>
              <!-- Navbar End -->
            </div>
          </nav>

          <!-- Search Form -->
          <div class="search-form">
            <form action="#" method="get">
              <input type="search" name="search" id="search" placeholder="Type keywords &amp; press enter...">
              <button type="submit" class="d-none"></button>
            </form>
            <!-- Close Icon -->
            <div class="closeIcon"><i class="fa fa-times" aria-hidden="true"></i></div>
          </div>
        </div>
      </div>
    </div>
  </header>