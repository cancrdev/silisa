<footer class="footer-area">
    <!-- Main Footer Area -->
    <div class="main-footer bg-img bg-overlay section-padding-80-0" style="background-image: url(<?= $log->base_url(); ?>assets/visitor/img/bg-img/3.jpg);">
      <div class="container">
        <div class="row">

          <!-- Single Footer Widget Area -->
          <div class="col-12 col-sm-6 col-lg-4">
            <div class="footer-widget mb-80">
              <a href="#" class="foo-logo d-block mb-30">
                <!-- <img src="<?= $log->base_url(); ?>assets/visitor/img/core-img/logo2.png" alt=""> -->
                <h2 class="text-white"><?= $log->name_app(); ?></h2>
              </a>
              <p>Desa Silisa Berada diwilayah online</p>
              <div class="contact-info">
                <p><i class="fa fa-map-pin" aria-hidden="true"></i><span>Jl.Warigalit,Semarang</span></p>
                <p><i class="fa fa-envelope" aria-hidden="true"></i><span>silisa@gmail.com</span></p>
                <p><i class="fa fa-phone" aria-hidden="true"></i><span>+84 223 9000</span></p>
              </div>
            </div>
          </div>

          <!-- Single Footer Widget Area -->
          <div class="col-12 col-sm-6 col-lg-4">
            <div class="footer-widget mb-80">
              <h5 class="widget-title text">QUICK LINK</h5>
              <!-- Footer Widget Nav -->
              <nav class="footer-widget-nav">
                <ul>
                  <li><a href="<?= $log->base_url(); ?>">Beranda</a></li>
                  <li><a href="https://drive.google.com/open?id=1zzjeetvaH-Sy91cl3bJ5FgnEULPaJBXA" target="_blank">PPID</a></li>
                  <li><a href="<?= $log->base_url(); ?>contact">Kontak Kami</a></li>
                  <li><a href="<?= $log->base_url(); ?>history">Sejarah Desa</a></li>
                  <li><a href="<?= $log->base_url(); ?>profile-desa">Profil Desa</a></li>
                  <li><a href="<?= $log->base_url(); ?>first">Visi & Misi</a></li>
                  <li><a href="<?= $log->base_url(); ?>peta-desa">Peta Desa</a></li>
                  <li><a href="<?= $log->base_url(); ?>lpm">LPM</a></li>
                  <li><a href="<?= $log->base_url(); ?>karang-taruna">Karang Taruna</a></li>
                  <li><a href="<?= $log->base_url(); ?>pkk">PKK</a></li>
                  <li><a href="<?= $log->base_url(); ?>news">Berita</a></li>
                  <li><a target="_blank" href="<?= $log->base_url(); ?>/v1">Login</a></li>
                </ul>
              </nav>
            </div>
          </div>

          <!-- Single Footer Widget Area -->

          <!-- Single Footer Widget Area -->
          <div class="col-12 col-sm-6 col-lg-4">
            <div class="footer-widget mb-80">
              <h5 class="widget-title">Ikuti Kami</h5>
              <!-- Footer Social Info -->
              <div class="footer-social-info">
                <a href="#">
                  <i class="fa fa-facebook" aria-hidden="true"></i>
                  <span>Facebook</span>
                </a>
                <a href="#">
                  <i class="fa fa-twitter" aria-hidden="true"></i>
                  <span>Twitter</span>
                </a>
                <a href="#">
                  <i class="fa fa-pinterest" aria-hidden="true"></i>
                  <span>Instagram</span>
                </a>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>

    <!-- Copywrite Area  -->
    <div class="copywrite-area">
      <div class="container">
        <div class="copywrite-text">
          <div class="row align-items-center">
            <div class="col-md-6">
              <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Develop with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="http://dokterapps.com" target="_blank">DOKTERAPPS</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
</p>
            </div>
            <div class="col-md-6">
              <div class="footer-nav">
               <!--  <nav>
                  <ul>
                    <li><a href="#">About</a></li>
                    <li><a href="#">Produce</a></li>
                    <li><a href="#">Practice</a></li>
                    <li><a href="#">Products</a></li>
                    <li><a href="#">News</a></li>
                    <li><a href="#">Contact</a></li>
                  </ul>
                </nav> -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>