<?php 
/**
* 
*/
class Connection
{
	// hosting
	// var $host		= "192.168.23.207";
	// var $username	= "cancoid_crm";
	// var $password	= "toor@123@";
	// var $db			= "cancoid_crm";

	//local
	var $host		= "localhost";
	var $username	= "root";
	var $password	= "";
	var $db			= "sideli";

	function conn() {
		$koneksi = mysqli_connect($this->host, $this->username, $this->password) or die("Gagal koneksi database! msg: ". mysqli_connect_error());
		$select_db = mysqli_select_db($koneksi, $this->db) or die("Database tidak ditemukan! msg: ". mysqli_error($koneksi));
		return $koneksi;
	}

	function query($request){
		return mysqli_query($this->conn(), $request);
	}
	
	function num_rows($request){
		return mysqli_num_rows(mysqli_query($this->conn(), $request));
	}

	function fetch_assoc($request) {
		return mysqli_fetch_assoc(mysqli_query($this->conn(), $request));
	}

	
	function clean_post($request)
	{
		return mysqli_escape_string($this->conn(), $request);
	}

	function clean_xss($request)
	{
		$xss = strip_tags($request);
		return $this->clean_post($xss);

	}

	function clean_all($request)
	{
		$post = $this->clean_post($request);
		$xss = $this->clean_xss($post);
		return $xss;
	}
}

?>