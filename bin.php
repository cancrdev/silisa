<?php
include_once 'controller/globalController.php';
include_once 'controller/news-vistorController.php';

$log = new globalController();
$new = new NewsVisitorController();

$slider = $new->fetchSlider();
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="description" content="">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <!-- Title -->
  <title><?= $log->name_app(); ?> | Home</title>
  <!-- Favicon -->
  <link rel="icon" href="<?= $log->base_url(); ?>assets/visitor/img/core-img/favicon.ico">
  <!-- Core Stylesheet -->
  <link rel="stylesheet" href="<?= $log->base_url(); ?>assets/visitor/style.css">
  <style type="text/css">
    .offline {
        background: #ff1e2d;
      }

      .offline-simulate-ui, .hide {
        display: none;
      }

  </style>
</head>

<body>
  <!-- Preloader -->
  <div class="preloader d-flex align-items-center justify-content-center">
    <div class="spinner"></div>
  </div>

  <!-- ##### Header Area Start ##### -->
  <?php include_once 'layouts/visitor/navbar.php'; ?>
  <!-- ##### Header Area End ##### -->

  <!-- ##### Hero Area Start ##### -->
  <div class="hero-area">
    <div class="welcome-slides owl-carousel">

      <!-- Single Welcome Slides -->
      <?php foreach($slider as $sl) : ?>
      <div class="single-welcome-slides bg-img bg-overlay jarallax" style="background-image: url(<?= $log->base_url(); ?>assets/slider/<?= $sl["SS_IMAGE"]; ?>);">
        <div class="container h-100">
          <div class="row h-100 align-items-center">
            <div class="col-12 col-lg-10">
              <div class="welcome-content">
                <h2 data-animation="fadeInUp" data-delay="200ms"><?= $sl["SS_JUDUL"]; ?></h2>
                <p data-animation="fadeInUp" data-delay="400ms"><?= $sl["SS_KETERANGAN"]; ?></p>
                <?php if($sl["SS_ACTION"] == "" || $sl["SS_ACTION"] == null) : ?>
                <?php else : ?>
                <a href="<?= $log->base_url().$sl["SS_LINK"]; ?>" class="btn famie-btn mt-4" data-animation="bounceInUp" data-delay="600ms"><?= $sl["SS_ACTION"]; ?></a>
                <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
     <?php endforeach; ?>
      <!-- Single Welcome Slides -->
    </div>
  </div>
  <!-- ##### Hero Area End ##### -->

  <!-- ##### Famie Benefits Area Start ##### -->
  <section class="famie-benefits-area section-padding-100-0 pb-5">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h3 class="text-center">Tentang Desa <?= $log->name_app(); ?></h3>
        </div><br>
        <div class="col-12">
          <div class="benefits-thumbnail mb-50">
            <img src="<?= $log->base_url(); ?>assets/visitor/img/bg-img/2.jpg" alt="">
          </div>
        </div>
      </div>

      <div class="row justify-content-center">
        <!-- Single Benefits Area -->
        <div class="col-12 col-sm-4 col-lg">
          <div class="single-benefits-area wow fadeInUp mb-50" data-wow-delay="100ms">
            <img src="<?= $log->base_url(); ?>assets/visitor/img/core-img/digger.png" alt="">
            <h5>Desa Terpintar</h5>
          </div>
        </div>

        <!-- Single Benefits Area -->
        <div class="col-12 col-sm-4 col-lg">
          <div class="single-benefits-area wow fadeInUp mb-50" data-wow-delay="300ms">
            <img src="<?= $log->base_url(); ?>assets/visitor/img/core-img/windmill.png" alt="">
            <h5>Infrastuktur Terbaik</h5>
          </div>
        </div>

        <!-- Single Benefits Area -->
        <div class="col-12 col-sm-4 col-lg">
          <div class="single-benefits-area wow fadeInUp mb-50" data-wow-delay="500ms">
            <img src="<?= $log->base_url(); ?>assets/visitor/img/core-img/cereals.png" alt="">
            <h5>100% Asri Alam</h5>
          </div>
        </div>

        <!-- Single Benefits Area -->
        <div class="col-12 col-sm-4 col-lg">
          <div class="single-benefits-area wow fadeInUp mb-50" data-wow-delay="700ms">
            <img src="<?= $log->base_url(); ?>assets/visitor/img/core-img/tractor.png" alt="">
            <h5>Desa Entrepreuner</h5>
          </div>
        </div>

        <!-- Single Benefits Area -->
        <div class="col-12 col-sm-4 col-lg">
          <div class="single-benefits-area wow fadeInUp mb-50" data-wow-delay="900ms">
            <img src="<?= $log->base_url(); ?>assets/visitor/img/core-img/sunrise.png" alt="">
            <h5>100% Prganik</h5>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- ##### Famie Benefits Area End ##### -->

  <!-- ##### About Us Area Start ##### -->
  <section class="about-us-area">
    <div class="container">
      <div class="row align-items-center">

        <!-- About Us Content -->
        <div class="col-12 col-md-8">
          <div class="about-us-content mb-100">
            <!-- Section Heading -->
            <div class="section-heading">
              <p>Tentang Desa <?= $log->name_app(); ?></p>
              <h2><span>Cerita Tentang Desa <?= $log->name_app(); ?></span></h2>
              <img src="<?= $log->base_url(); ?>assets/visitor/img/core-img/decor.png" alt="">
            </div>
            <p><?= $log->deskripsi(); ?></p>
          </div>
        </div>

        <!-- Famie Video Play -->
        <div class="col-12 col-md-4">
          <div class="famie-video-play mb-100">
            <img src="<?= $log->base_url(); ?>assets/visitor/img/bg-img/6.jpg" alt="">
            <!-- Play Icon -->
            <a href="https://www.youtube.com/watch?v=IkuzGEwdXuw" class="play-icon"><i class="fa fa-play"></i></a>
          </div>
        </div>

      </div>
    </div>
  </section>
  <!-- ##### About Us Area End ##### -->

  <!-- ##### Services Area Start ##### -->
  <section class="services-area d-flex flex-wrap">
    <!-- Service Thumbnail -->
    <div class="services-thumbnail bg-img jarallax" style="background-image: url('<?= $log->base_url(); ?>assets/visitor/img/bg-img/7.jpg');"></div>

    <!-- Service Content -->
    <div class="services-content section-padding-100-50 px-5">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- Section Heading -->
            <div class="section-heading">
              <p>Apa yang menarik dari Desa <?= $log->name_app(); ?></p>
              <h2><span>Kekayaan Alam Desa</span> Menjadi Andalan Kami</h2>
              <img src="<?= $log->base_url(); ?>assets/visitor/img/core-img/decor.png" alt="">
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-12 mb-50">
            <p>Kekayaan alam di desa <?= $log->name_app(); ?> sangat berpengaruh besar bagi perekonomian desa,dan menjadi mata pencarian didesa <?= $log->name_app(); ?></p>
          </div>

          <!-- Single Service Area -->
          <div class="col-12 col-lg-6">
            <div class="single-service-area mb-50 wow fadeInUp" data-wow-delay="100ms">
              <!-- Service Title -->
              <div class="service-title mb-3 d-flex align-items-center">
                <img src="<?= $log->base_url(); ?>assets/visitor/img/core-img/s1.png" alt="">
                <h5>Buah &amp; Sayuran</h5>
              </div>
              <p>Sayur &amp; Sayuran didesa <?= $log->name_app(); ?> sangat alami tanpa bahan kimia</p>
            </div>
          </div>

          <!-- Single Service Area -->
          <div class="col-12 col-lg-6">
            <div class="single-service-area mb-50 wow fadeInUp" data-wow-delay="300ms">
              <!-- Service Title -->
              <div class="service-title mb-3 d-flex align-items-center">
                <img src="<?= $log->base_url(); ?>assets/visitor/img/core-img/s2.png" alt="">
                <h5>Daging &amp; Telur</h5>
              </div>
              <p>Didesa <?= $log->name_app(); ?> banyak yang menggantungkan mata pencarian sebagai peternak untuk produksi pedaging / telur</p>
            </div>
          </div>

          <!-- Single Service Area -->
          <div class="col-12 col-lg-6">
            <div class="single-service-area mb-50 wow fadeInUp" data-wow-delay="500ms">
              <!-- Service Title -->
              <div class="service-title mb-3 d-flex align-items-center">
                <img src="<?= $log->base_url(); ?>assets/visitor/img/core-img/s3.png" alt="">
                <h5>Susu</h5>
              </div>
              <p>Karena banyak yang memiliki usaha pedaging,sehingga banyak juga yang mensuplai susu </p>
            </div>
          </div>

          <!-- Single Service Area -->
          <div class="col-12 col-lg-6">
            <div class="single-service-area mb-50 wow fadeInUp" data-wow-delay="700ms">
              <!-- Service Title -->
              <div class="service-title mb-3 d-flex align-items-center">
                <img src="<?= $log->base_url(); ?>assets/visitor/img/core-img/s4.png" alt="">
                <h5>Pertanian</h5>
              </div>
              <p>Mayoritas masyarakat desa <?= $log->name_app(); ?> juga mempunyai mata pencarian sebagai petani jadi banyak produk yang berasal dari pertanian</p>
            </div>
          </div>

        </div>
      </div>
    </div>
  </section>
  <!-- ##### Services Area End ##### -->

  <!-- ##### Newsletter Area Start ##### -->
  <section class="newsletter-area section-padding-100 bg-img bg-overlay jarallax" style="background-image: url('<?= $log->base_url(); ?>assets/visitor/img/bg-img/8.jpg');">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12 col-lg-10">
          <div class="newsletter-content">
            <!-- Section Heading -->
            <div class="section-heading white text-center">
              <p>Apakah kamu tahu?</p>
              <h2><span>Kekayaan Alam Desa</span> Menjadi andalan kami</h2>
              <img src="<?= $log->base_url(); ?>assets/visitor/img/core-img/decor2.png" alt="">
            </div>
            <p class="text-white mb-50 text-center">Desa <?= $log->name_app(); ?> juga akan memberikan informasi yang berguna jika mengikuti kami,dan dapatkan informasi berita terbaru melalui email</p>
          </div>
        </div>
      </div>
      <!-- Newsletter Form -->
      <div class="row justify-content-center">
        <div class="col-12 col-lg-6">
          <?php 
          if(isset($_POST["tambah"])) {
            $sub["email"] = $_POST["email"];
            $new->subcribeNews($sub);
          }
          ?>
          <form action="" method="post">
            <input type="email" name="email" required class="form-control" placeholder="Masukkkan Email yang anda punya">
            <button type="submit" name="tambah" class="btn famie-btn">Subscribe</button>
          </form>
        </div>
      </div>
    </div>
  </section>
  <!-- ##### Newsletter Area End ##### -->

  <!-- ##### Farming Practice Area Start ##### -->
  <section class="farming-practice-area section-padding-100-50">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <!-- Section Heading -->
          <div class="section-heading text-center">
            <p>BUAT DUNIA HIJAU</p>
            <h2><span>Farming Practices</span> To Preserve Land & Water</h2>
            <img src="<?= $log->base_url(); ?>assets/visitor/img/core-img/decor2.png" alt="">
          </div>
        </div>
      </div>

      <div class="row">

        <!-- Single Farming Practice Area -->
        <div class="col-12 col-sm-6 col-lg-4">
          <div class="single-farming-practice-area mb-50 wow fadeInUp" data-wow-delay="100ms">
            <!-- Thumbnail -->
            <div class="farming-practice-thumbnail">
              <img src="<?= $log->base_url(); ?>assets/visitor/img/bg-img/9.jpg" alt="">
            </div>
            <!-- Content -->
            <div class="farming-practice-content text-center">
              <!-- Icon -->
              <div class="farming-icon">
                <img src="<?= $log->base_url(); ?>assets/visitor/img/core-img/chicken.png" alt="">
              </div>
              <span>Farming practice for</span>
              <h4>Chicken Farmed For Meat</h4>
              <p>Donec nec justo eget felis facilisis ferme ntum. Aliquam portitor mauris sit amet orci. donec salim...</p>
            </div>
          </div>
        </div>

        <!-- Single Farming Practice Area -->
        <div class="col-12 col-sm-6 col-lg-4">
          <div class="single-farming-practice-area mb-50 wow fadeInUp" data-wow-delay="200ms">
            <!-- Thumbnail -->
            <div class="farming-practice-thumbnail">
              <img src="<?= $log->base_url(); ?>assets/visitor/img/bg-img/10.jpg" alt="">
            </div>
            <!-- Content -->
            <div class="farming-practice-content text-center">
              <!-- Icon -->
              <div class="farming-icon">
                <img src="<?= $log->base_url(); ?>assets/visitor/img/core-img/pig.png" alt="">
              </div>
              <span>Farming practice for</span>
              <h4>Pig Farm Management</h4>
              <p>Donec nec justo eget felis facilisis ferme ntum. Aliquam portitor mauris sit amet orci. donec salim...</p>
            </div>
          </div>
        </div>

        <!-- Single Farming Practice Area -->
        <div class="col-12 col-sm-6 col-lg-4">
          <div class="single-farming-practice-area mb-50 wow fadeInUp" data-wow-delay="300ms">
            <!-- Thumbnail -->
            <div class="farming-practice-thumbnail">
              <img src="<?= $log->base_url(); ?>assets/visitor/img/bg-img/11.jpg" alt="">
            </div>
            <!-- Content -->
            <div class="farming-practice-content text-center">
              <!-- Icon -->
              <div class="farming-icon">
                <img src="<?= $log->base_url(); ?>assets/visitor/img/core-img/cow.png" alt="">
              </div>
              <span>Farming practice for</span>
              <h4>Beef Cattle Farming</h4>
              <p>Donec nec justo eget felis facilisis ferme ntum. Aliquam portitor mauris sit amet orci. donec salim...</p>
            </div>
          </div>
        </div>

        <!-- Single Farming Practice Area -->
        <div class="col-12 col-sm-6 col-lg-4">
          <div class="single-farming-practice-area mb-50 wow fadeInUp" data-wow-delay="400ms">
            <!-- Thumbnail -->
            <div class="farming-practice-thumbnail">
              <img src="<?= $log->base_url(); ?>assets/visitor/img/bg-img/12.jpg" alt="">
            </div>
            <!-- Content -->
            <div class="farming-practice-content text-center">
              <!-- Icon -->
              <div class="farming-icon">
                <img src="<?= $log->base_url(); ?>assets/visitor/img/core-img/cereal.png" alt="">
              </div>
              <span>Farming practice for</span>
              <h4>Improved Rice Cultivation</h4>
              <p>Donec nec justo eget felis facilisis ferme ntum. Aliquam portitor mauris sit amet orci. donec salim...</p>
            </div>
          </div>
        </div>

        <!-- Single Farming Practice Area -->
        <div class="col-12 col-sm-6 col-lg-4">
          <div class="single-farming-practice-area mb-50 wow fadeInUp" data-wow-delay="500ms">
            <!-- Thumbnail -->
            <div class="farming-practice-thumbnail">
              <img src="<?= $log->base_url(); ?>assets/visitor/img/bg-img/13.jpg" alt="">
            </div>
            <!-- Content -->
            <div class="farming-practice-content text-center">
              <!-- Icon -->
              <div class="farming-icon">
                <img src="<?= $log->base_url(); ?>assets/visitor/img/core-img/sprout.png" alt="">
              </div>
              <span>Farming practice for</span>
              <h4>Soil Improvement Techniques</h4>
              <p>Donec nec justo eget felis facilisis ferme ntum. Aliquam portitor mauris sit amet orci. donec salim...</p>
            </div>
          </div>
        </div>

        <!-- Single Farming Practice Area -->
        <div class="col-12 col-sm-6 col-lg-4">
          <div class="single-farming-practice-area mb-50 wow fadeInUp" data-wow-delay="600ms">
            <!-- Thumbnail -->
            <div class="farming-practice-thumbnail">
              <img src="<?= $log->base_url(); ?>assets/visitor/img/bg-img/14.jpg" alt="">
            </div>
            <!-- Content -->
            <div class="farming-practice-content text-center">
              <!-- Icon -->
              <div class="farming-icon">
                <img src="<?= $log->base_url(); ?>assets/visitor/img/core-img/vegetable.png" alt="">
              </div>
              <span>Farming practice for</span>
              <h4>Intensive Fruit Farming</h4>
              <p>Donec nec justo eget felis facilisis ferme ntum. Aliquam portitor mauris sit amet orci. donec salim...</p>
            </div>
          </div>
        </div>

      </div>
    </div>
  </section>
  <!-- ##### Farming Practice Area End ##### -->

  <!-- ##### Testimonial Area Start ##### -->
  <!-- ##### Footer Area Start ##### -->
  <?php include_once 'layouts/visitor/footer.php'; ?>
  <!-- ##### Footer Area End ##### -->

  <!-- ##### All Javascript Files ##### -->
  <!-- jquery 2.2.4  -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jquery.min.js"></script>
  <!-- Popper js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/popper.min.js"></script>
  <!-- Bootstrap js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/bootstrap.min.js"></script>
  <!-- Owl Carousel js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/owl.carousel.min.js"></script>
  <!-- Classynav -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/classynav.js"></script>
  <!-- Wow js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/wow.min.js"></script>
  <!-- Sticky js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jquery.sticky.js"></script>
  <!-- Magnific Popup js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jquery.magnific-popup.min.js"></script>
  <!-- Scrollup js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jquery.scrollup.min.js"></script>
  <!-- Jarallax js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jarallax.min.js"></script>
  <!-- Jarallax Video js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/jarallax-video.min.js"></script>
  <!-- Active js -->
  <script src="<?= $log->base_url(); ?>assets/visitor/js/active.js"></script>
  <script type="text/javascript">
    (function () {
      var $offline, $online;

      $online = $('.online');

      $offline = $('.offline');

      Offline.on('confirmed-down', function () {
        return $online.fadeOut(function () {
          return $offline.fadeIn();
        });
      });

      Offline.on('confirmed-up', function () {
        return $offline.fadeOut(function () {
          return $online.fadeIn();
        });
      });

    }).call(this);

//# sourceURL=coffeescript
  </script>
</body>

</html>