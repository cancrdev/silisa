<?php
if(isset($_GET["q"])) {
   $kode  = $_GET["q"];
   $value = $_GET["key"];

   if($kode == "kurang_mampu") {
   	 header("Location: ../print/SKTM/SKTM.php?key=$value");
   } elseif($kode == "skhw") {
     header("Location: ../print/SKHewan/SKHewan.php?key=$value");
   } elseif($kode == "jenis_usaha") {
   	 header("Location: ../print/SKUsaha/SKUsaha.php?key=$value");
   } elseif($kode == "nikah") {
      header("Location: ../print/SNikah/SKNikah.php?key=$value");
   } elseif($kode == "jual_beli") { 
      header("Location: ../print/SKJB/SKJB.php?key=$value");
   } elseif($kode == "belum_nikah") {
     header("Location: ../print/SKBNikah/SKBNikah.php?key=$value");
   } elseif($kode == "hilang") {
     header("Location: ../print/SKHilang/SKHilang.php?key=$value");
   } elseif($kode == "ganti_ktp") {
     header("Location: ../print/SKSG/SKSG.php?key=$value");
   } elseif($kode == "kk"){
     header("Location: ../print/SKK/Skk.php?key=$value");
   } elseif($kode == "pindah") {
     header("Location: ../print/SKPindah/SKPindah.php");
   } else {
   	 echo "<script>alert('kode surat tidak tersedia')
                    location.replace('javascript:history.back()')</script>";
   }
} else {
	 header("Location: ../../404");
}

?>