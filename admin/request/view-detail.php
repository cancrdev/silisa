<?php
session_start();
include_once '../../controller/adminController.php';
include_once '../../controller/globalController.php';

$letter = new AdminController();
$log  = new globalController();

$new  = $letter->view_detail_request($_GET["key"]);

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?= $log->name_app(); ?>  | PERMINTAAN SURAT</title>
    <!-- Favicon-->
    <!-- <link rel="icon" href="../../favicon.ico" type="image/x-icon"> -->

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

    <!-- Custom Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?= $log->base_url(); ?>assets/admin/css/themes/all-themes.css" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p><?= $log->loading_apps(); ?></p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <?php include_once '../../layouts/admin/navbar.php'; ?>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
         <?php include_once '../../layouts/admin/leftbar.php'; ?>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>
                     Data Detail Permintaan Surat
                    <!-- <small>Taken from <a href="https://datatables.net/" target="_blank">datatables.net</a></small> -->
                </h2>
            </div>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Data Detail Permintaan Surat - <?= $new["NIK"]." a/n ".$new["PER_NAMA_WARGA"]; ?>
                            </h2>
                        </div>
                        <div class="body">
                            <table width="100%" >
                               <tr>
                                 <td>No Surat</td>
                                 <td>:</td>
                                 <td><?= $new["NO_SURAT"]; ?></td> 
                               </tr>
                               <tr>
                                 <td>Keperluan</td>
                                 <td>:</td>
                                 <td><?= $new["PER_KEPERLUAN"]; ?></td> 
                               </tr>
                               <tr>
                                 <td>Masa Berlaku</td>
                                 <td>:</td>
                                 <td><?= $log->TanggalIndo($new["PER_MASA_BERLAKU"]); ?></td> 
                               </tr>
                               <tr>
                                 <td>NIK</td>
                                 <td>:</td>
                                 <td><?= $new["NIK"]; ?></td> 
                               </tr>
                               <tr>
                                 <td>No KK</td>
                                 <td>:</td>
                                 <td><?= $new["PER_KK"]; ?></td> 
                               </tr>
                               <tr>
                                 <td>Nama Warga</td>
                                 <td>:</td>
                                 <td><?= $new["PER_NAMA_WARGA"]; ?></td> 
                               </tr>
                               <tr>
                                 <td>Jenis Kelamin</td>
                                 <td>:</td>
                                 <td><?= $new["W_JK"]; ?></td> 
                               </tr>
                               <tr>
                                 <td>Alamat</td>
                                 <td>:</td>
                                 <td><?= $new["W_ALAMAT"]; ?></td> 
                               </tr>
                               <tr>
                                 <td>RT/RW</td>
                                 <td>:</td>
                                 <td><?= '0'.$new["W_RT"]."/0".$new["W_RW"]; ?></td> 
                               </tr>
                               <tr>
                                 <td>Agama</td>
                                 <td>:</td>
                                 <td><?= $new["W_AGAMA"]; ?></td> 
                               </tr>
                               <tr>
                                 <td>Pendidikan</td>
                                 <td>:</td>
                                 <td><?= $new["W_PENDIDIKAN"]; ?></td> 
                               </tr>
                               <tr>
                                 <td>Pekerjaan</td>
                                 <td>:</td>
                                 <td><?= $new["W_PEKERJAAN"]; ?></td> 
                               </tr>
                               <tr>
                                 <td>Tanggal Pengajuan</td>
                                 <td>:</td>
                                 <td><?= $log->TanggalIndo($new["PER_CREATED_AT"]); ?></td> 
                               </tr>
                            </table>
                        </div>
                        <div align="center">
                            <a href="javascript:history.back()" class="btn btn-danger waves-effect"><span class="material-icons">fast_rewind</span> Kembali</a><br><br>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/node-waves/waves.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Custom Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/js/admin.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/js/pages/ui/modals.js"></script>

    <!-- Custom Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/js/admin.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/js/pages/tables/jquery-datatable.js"></script>

    <!-- Demo Js -->
    <!-- <script src="<?= $log->base_url(); ?>assets/admin/js/demo.js"></script> -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

	<script type="text/javascript">
    jQuery(document).ready(function($){
        $('.delete-link').on('click',function(){
            var getLink = $(this).attr('href');
            swal({
            title: "Apakah Kamu yakin menghapus data?",
            text: "Data yang dihapus akan hilang",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Hapus",
            cancelButtonText: "Kembali",
            closeOnConfirm: false,
            closeOnCancel: true,
          },function(){
            window.location.href = getLink
          });
            return false;
          });
    });
</script>
</body>

</html>
