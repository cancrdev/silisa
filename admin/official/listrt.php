<?php
session_start();
include_once '../../controller/rtController.php';
include_once '../../controller/wargaController.php';
include_once '../../controller/globalController.php';
include_once '../../model/config.php';
include_once '../../model/connect.php';

$news    = new RtController();
$log     = new globalController();
$connect = new Connection();
$warga   = new WargaController();

$wargas  = $warga->fetchAll();

$new  = $news->fetchAll();

if(isset($_POST["save-data"])) {
    $nik      = $_POST["nik"];
    $name     = $_POST["username"];
    $fullname = $_POST["fullname"];
    $email    = $_POST["email"];
    $telpon   = $_POST["telpon"];
    $jabatan  = $_POST["jabatan"];
    $gender   = $_POST["gender"];
    $wilayah  = $_POST["wilayah"];
    $rwId     = $_POST["rw"];
    $pass     = md5(md5(12345678));
    $token    = base64_encode('rt'.date('YmdHis').$name);

    // $nik      = mysqli_query($conn, "SELECT COUNT(ID_USER) jml FROM sir_user WHERE U_NIK = '$nik' OR U_NAME = '$name' OR U_MOBILE = '$telpon'");
    // $cek      = mysqli_fetch_assoc($nik);
    // $ceks     = $cek["jml"];

    // if($ceks > 0) {
    //      echo "<script>alert('NIK yang anda masukkan sudah tersedia')
    //             location.replace('../official/listrt')</script>";
    // } else {
         $qq    =  mysqli_query($conn, "INSERT INTO sir_user (U_NIK, U_NAME, U_FULLNAME, U_EMAIL, U_PASSWORD, U_MOBILE, U_GROUP_RULE, U_STATUS, U_REMINDER, U_KEY) VALUES ('$nik', '$name', '$fullname', '$email', '$pass', '$telpon', 'U_RT', '1', '0', '$token')");
         if($qq) {
             $max   = mysqli_query($conn, "SELECT MAX(ID_USER) AS maxId FROM sir_user");
             $row   = mysqli_fetch_assoc($max);
             $maxId = $row["maxId"];

             $sql = mysqli_query($conn, "INSERT INTO sir_rt (P_USERID, P_RW, P_RT,  P_KETUA_RT, P_MASA_JABATAN, P_GENDER, P_STATUS) VALUES ('$maxId', '$rwId', '$wilayah', '$fullname', '$jabatan', '$gender', '1')");
            echo "<script>alert('User RT Added Succesfully')
                location.replace('../official/listrt')</script>";
         } else {
            echo '<div class="alert bg-pink alert-dismissible" role="alert">
                <button typ="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  Tambah RT Gagal
          </div>';
         } 
    // }
}

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?= $log->name_app(); ?>  | SETTINGS</title>
    <!-- Favicon-->
    <!-- <link rel="icon" href="../../favicon.ico" type="image/x-icon"> -->

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

    <!-- Custom Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?= $log->base_url(); ?>assets/admin/css/themes/all-themes.css" rel="stylesheet" />


    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p><?= $log->loading_apps(); ?></p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <?php include_once '../../layouts/admin/navbar.php'; ?>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
         <?php include_once '../../layouts/admin/leftbar.php'; ?>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
        <aside id="rightsidebar" class="right-sidebar">
            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                <li role="presentation" class="active"><a href="#skins" data-toggle="tab">SKINS</a></li>
                <!-- <li role="presentation"><a href="#settings" data-toggle="tab">SETTINGS</a></li> -->
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active in active" id="skins">
                    <ul class="demo-choose-skin">
                        <li data-theme="red" class="active">
                            <div class="red"></div>
                            <span>Red</span>
                        </li>
                        <li data-theme="pink">
                            <div class="pink"></div>
                            <span>Pink</span>
                        </li>
                        <li data-theme="purple">
                            <div class="purple"></div>
                            <span>Purple</span>
                        </li>
                        <li data-theme="deep-purple">
                            <div class="deep-purple"></div>
                            <span>Deep Purple</span>
                        </li>
                        <li data-theme="indigo">
                            <div class="indigo"></div>
                            <span>Indigo</span>
                        </li>
                        <li data-theme="blue">
                            <div class="blue"></div>
                            <span>Blue</span>
                        </li>
                        <li data-theme="light-blue">
                            <div class="light-blue"></div>
                            <span>Light Blue</span>
                        </li>
                        <li data-theme="cyan">
                            <div class="cyan"></div>
                            <span>Cyan</span>
                        </li>
                        <li data-theme="teal">
                            <div class="teal"></div>
                            <span>Teal</span>
                        </li>
                        <li data-theme="green">
                            <div class="green"></div>
                            <span>Green</span>
                        </li>
                        <li data-theme="light-green">
                            <div class="light-green"></div>
                            <span>Light Green</span>
                        </li>
                        <li data-theme="lime">
                            <div class="lime"></div>
                            <span>Lime</span>
                        </li>
                        <li data-theme="yellow">
                            <div class="yellow"></div>
                            <span>Yellow</span>
                        </li>
                        <li data-theme="amber">
                            <div class="amber"></div>
                            <span>Amber</span>
                        </li>
                        <li data-theme="orange">
                            <div class="orange"></div>
                            <span>Orange</span>
                        </li>
                        <li data-theme="deep-orange">
                            <div class="deep-orange"></div>
                            <span>Deep Orange</span>
                        </li>
                        <li data-theme="brown">
                            <div class="brown"></div>
                            <span>Brown</span>
                        </li>
                        <li data-theme="grey">
                            <div class="grey"></div>
                            <span>Grey</span>
                        </li>
                        <li data-theme="blue-grey">
                            <div class="blue-grey"></div>
                            <span>Blue Grey</span>
                        </li>
                        <li data-theme="black">
                            <div class="black"></div>
                            <span>Black</span>
                        </li>
                    </ul>
                </div>

            </div>
        </aside>
        <!-- #END# Right Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>
                     Master List data RT
                    <!-- <small>Taken from <a href="https://datatables.net/" target="_blank">datatables.net</a></small> -->
                </h2>
            </div>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Master List data RT
                            </h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <button type="button" class="btn btn-primary waves-effect m-r-20" data-toggle="modal" data-target="#defaultTambah"><span class="material-icons">person_add</span> Tambah Data</button><br><br>
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>No NIK</th>
                                            <th>Nama</th>
                                            <th>Mobile</th>
                                            <th>Masa Jabatan</th>
                                            <th>Gender</th>
                                            <th>Ketua RT</th>
                                            <th>RW</th>
                                            <th style="text-align: center;">Avatar</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1; foreach($new as $n) : ?>
                                        <tr>
                                            <td><?= $no++; ?></td>
                                            <td><?= $n['U_NIK']; ?></td>
                                            <td><?= $n['U_FULLNAME']; ?></td>
                                            <td><?= $n['U_MOBILE']; ?></td>
                                            <td><?= $n['P_MASA_JABATAN']; ?></td>
                                            <td><?= $n['P_GENDER']; ?></td>
                                            <td><?= $n['P_RT']; ?></td>
                                            <td><?= $n['P_RW']; ?></td>
                                            <td align="center">
                                            	<button type="button" class="btn btn-danger waves-effect m-r-20" data-toggle="modal" data-target="#defaultModal"><span class="material-icons">find_in_page</span> Detail Avatar</button>
                                            	<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
									                <div class="modal-dialog" role="document">
									                    <div class="modal-content">
									                        <div class="modal-header">
									                            <h4 class="modal-title" id="defaultModalLabel">Detail Avatar</h4>
									                        </div>
									                        <div class="modal-body">
                                                                <?php if($n['U_AVATAR'] == "" || $n['U_AVATAR'] == null) : ?>
									                            <img src="<?= $log->base_url() ?>assets/admin/images/user.png" alt="">
                                                                <?php else : ?>
                                                                <img src="<?= $log->base_url() ?>assets/uploads/<?= $n['U_AVATAR']; ?>" alt="">  
                                                                <?php endif; ?>
									                        </div>
									                        <div class="modal-footer">
									                            <!-- <button type="button" class="btn btn-link waves-effect">SAVE CHANGES</button> -->
									                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal"><span class="material-icons">close</span> CLOSE</button>
									                        </div>
									                    </div>
									                </div>
									            </div>
                                            </td>
                                            <td>
                                            	<ul class="header-dropdown m-r--5">
					                                <li class="dropdown">
					                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
					                                        <i class="material-icons">more_vert</i>
					                                    </a>
					                                    <ul class="dropdown-menu pull-right">
					                                        <li>
                                                                <?php if($n["U_STATUS"] == 0) : ?>
                                                                <a href="cek?value=<?= $n["ID_USER"]; ?>&status=1&type=rt" style="color: red" class="ubah-link"><span class="material-icons">verified_user</span> Ubah Status</a>
                                                                 <?php else : ?>
                                                                <a href="cek?value=<?= $n["ID_USER"]; ?>&status=0&type=rt" style="color: green" class="ubah-link"><span class="material-icons">verified_user</span> Ubah Status</a>
                                                                <?php endif; ?> 
                                                            </li>
					                                    </ul>
					                                </li>
					                            </ul>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
        </div>
        
        <div class="modal fade" id="defaultTambah" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalScrollableTitle">Tambah Data RT</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
              </div>    
                <form action="" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <select  name="nik" class="form-control  form-line">
                                <?php foreach($wargas as $w) : ?>
                                <option value="<?= $w["W_NIK"]; ?>"><?= $w["W_NIK"].'-'.$w["W_NAMA"]; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div><br>
                        <!-- <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="nik" onkeypress="return hanyaAngka(event)" required>
                                <label class="form-label">No NIK</label>
                            </div>
                        </div> -->
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="username" required>
                                <label class="form-label">Username</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="fullname" required>
                                <label class="form-label">Nama Lengkap</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="telpon" onkeypress="return hanyaAngka(event)" required>
                                <label class="form-label">No Telp</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="email" class="form-control" name="email" required>
                                <label class="form-label">Email</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="radio" name="gender" value="Laki-Laki" id="male" class="with-gap">
                            <label for="male">Laki-Laki</label>

                            <input type="radio" name="gender" value="Perempuan" id="female" class="with-gap">
                            <label for="female" class="m-l-20">Perempuan</label>
                        </div>
                         <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="jabatan" required>
                                <label class="form-label">Masa Jabatan</label>
                            </div>
                        </div>
                         <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="wilayah" onkeypress="return hanyaAngka(event)" required>
                                <label class="form-label">Ketua RT</label>
                            </div>
                        </div>
                         <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="rw" onkeypress="return hanyaAngka(event)" required>
                                <label class="form-label">RW</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a type="button" class="btn btn-secondary" data-dismiss="modal">Close</a>
                        <input type="submit" name="save-data" value="Save" class="btn btn-primary">
                    </div>
                </form>
            </div>
          </div>
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/node-waves/waves.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Custom Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/js/admin.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/js/pages/ui/modals.js"></script>

    <!-- Custom Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/js/admin.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/js/pages/tables/jquery-datatable.js"></script>

    <!-- Demo Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/js/demo.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

	<script type="text/javascript">
    jQuery(document).ready(function($){
        $('.ubah-link').on('click',function(){
            var getLink = $(this).attr('href');
            swal({
            title: "Apakah Kamu yakin Ubah data?",
            // text: "Data yang dihapus akan hilang",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Ubah",
            cancelButtonText: "Kembali",
            closeOnConfirm: false,
            closeOnCancel: true,
          },function(){
            window.location.href = getLink
          });
            return false;
          });
    });

    function hanyaAngka(evt) {
          var charCode = (evt.which) ? evt.which : event.keyCode
           if (charCode > 31 && (charCode < 48 || charCode > 57))
 
            return false;
          return true;
        }
</script>
</body>

</html>
