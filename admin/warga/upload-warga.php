<?php
session_start();
require '../../vendor/autoload.php';
require '../../model/config.php';

$connect = new Connection();

// $user_id = $_SESSION["U_ID"];
$random = rand(111,999);
$datetime = date('Y-m-d H:i:s');
$date = date("Y-m-d");
//target excel
$targetName = $_FILES['excel']['name'];
$extensi = explode('.', $targetName);
$filename = "file-".round(microtime(true)).".".end($extensi);
$target = $_FILES['excel']['tmp_name'];
$target_dir = '../../assets/excel/';
$target_file = $target_dir.$filename;
$upload = move_uploaded_file($target, $target_file);

$obj = PHPExcel_IOFactory::load($target_file);
$all_data = $obj->getActiveSheet()->toArray(null, true, true ,true);

//insert to tabel warga
$sql = "INSERT INTO sir_warga (W_NIK, W_KK, W_NAMA, W_TMP_LAHIR, W_TGL_LAHIR, W_JK, W_ALAMAT, W_RT, W_RW, W_KELURAHAN, W_KECAMATAN, W_KABUPATEN, W_PROVINSI, W_AGAMA, W_STATUS, W_STATUS_KK, W_PENDIDIKAN, W_PEKERJAAN, W_STATUS_PENDUDUK, W_CREATED_BY) VALUES";
for ($i=1; $i < count($all_data); $i++) { 
	$no    = $all_data[$i]['A'];
	$nik   = $all_data[$i]['B'];
	$kk    = $all_data[$i]['C'];
	$nama  = $all_data[$i]['D'];
	$tempat= $all_data[$i]['E'];
	$tgl   = $all_data[$i]['F'];
	$var   = $tgl;
	$gender= $all_data[$i]['G'];
	$alamat= $all_data[$i]['H'];
	$rt    = $all_data[$i]['I'];
	$rw    = $all_data[$i]['J'];
	$lurah = $all_data[$i]['K'];
	$kec   = $all_data[$i]['L'];
	$kab   = $all_data[$i]['M'];
	$prov  = $all_data[$i]['N'];
	$agama = $all_data[$i]['O'];
	$status= $all_data[$i]['P'];
	$sts_kk= $all_data[$i]['Q'];
	$study = $all_data[$i]['R'];
	$work  = $all_data[$i]['S'];
	$wni   = $all_data[$i]['T'];
	$sql  .= "('$nik', '$kk', '$nama', '$tempat', '$var', '$gender', '$alamat', '$rt', '$rw', '$lurah', '$kec', '$kab', '$prov', '$agama', '$status', '$sts_kk', '$study', '$work', '$wni', 'ADMIN'),";
}


$sql = substr($sql, 0, -1);
//echo sql
$query = $connect->query($sql);

if($query) {
	$data = $connect->query("INSERT INTO sir_upload (SU_PATH, SU_TGL) VALUES ('$filename', '$date')");
	$del  = $connect->query("DELETE FROM sir_warga WHERE W_NIK = 'NIK' AND W_KK = 'NO_KK'");
	$del2 = $connect->query("DELETE FROM sir_warga WHERE W_NIK = '' AND W_KK = ''");
	echo "<script>alert('Warga Berhasil diupload')
			location.replace('../warga')</script></script>";
} else {
	echo "<script>alert('Warga gagal ditambahkan')
			location.replace('../warga')</script></script>";
}
?>