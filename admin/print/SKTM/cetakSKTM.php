<?php 
include('../../../controller/suratController.php');

$view = new SuratController();

$data = $view->view_print_request($_GET["key"]);
$pl   = $view->get_lurah();
?>
<style="">
	table, th, td {
	    border: 0px solid black;
	}
	
	.absolute {
	    position: absolute;
	    top: 0px;
	    left: 0;
	    width: 50px;
	    height: 50px;
	}
</style>
<page>
<div class='absolute'>
     <img src='../../../assets/logo-semarang.png' width='50'>
     </div>
<table cellpadding='1'>
<tbody>
  <tr>    
     <td style='text-align: center;' colspan='3'>
            <span style='font-family: Verdana; font-size: x-medium;'><b>PEMERINTAH KOTA SEMARANG<br>
                      KECAMATAN SEMARANG UTARA<br>
                      KEPALA KELURAHAN BULU LOR</b><br></span>
                      <span>Jalan Raya Semarang telp.(0261) 202767, Kode Pos 53444</span>
                      <br>
                   <hr>
      </td>   
  </tr>
  <tr>    
      <td colspan='3'>
        <div align='center'>
            <span style='font-family: Verdana; text-align: center; font-size: 14px;'><b>SURAT KETERANGAN TIDAK MAMPU</b></span><br>
                       <span style='text-align: center;'>============================</span>
                       <br>
                      <span style='font-family: Verdana; text-align: center; font-size: 12px;'>Nomor : <?php echo $data["NO_SURAT"]; ?></span>
         </div>
       </td>      
  </tr>
  
  <tr>     
     <td colspan='3'>
        <br>
        <span style='font-size: x-medium;'>Yang Bertanda Tangan Dibawah ini Kepala desa <?php echo $data["W_KELURAHAN"].' Kecamatan '.$data["W_KECAMATAN"].' Kabupaten '.$data['W_KABUPATEN'].' '; ?> Menerangkan<br> Sesungguhnya Bahwa :</span>
     </td>     
  </tr>
  <tr>     
       <td valign='top' colspan='3'>
           <div align='left' style='margin-left: 30px'>
               <br>
              <table style='margin-left: 50px'>
                   <tbody>
              <tr>           
                   <td width='80'><span style='font-size: x-medium;'>NIK</span></td>       <td width='10'><span style='font-size: x-medium;'>:</span></td>           
                   <td width='248'><span style='font-size: x-medium;'><?php echo $data["NIK"]; ?></span>
                   </td>         
              </tr>
              <tr>           
                   <td width='80'><span style='font-size: x-medium;'>Nama</span></td>       <td width='10'><span style='font-size: x-medium;'>:</span></td>           
                   <td width='248'><span style='font-size: x-medium;'><?php echo $data["PER_NAMA_WARGA"]; ?></span>
                   </td>         
              </tr>
              <tr>           
                   <td><span style='font-size: x-medium;'>Tempat,Tanggal Lahir</span></td>           
                   <td><span style='font-size: x-medium;'>:</span></td>           
                   <td><span style='font-size: x-medium;'><?php echo $data["W_TMP_LAHIR"].",".date('d F Y', strtotime($data["W_TGL_LAHIR"])); ?></span></td>         
              </tr>
              <tr>           
                   <td><span style='font-size: x-medium;'>Jenis Kelamin</span></td>           
                   <td><span style='font-size: x-medium;'>:</span></td>           
                   <td><span style='font-size: x-medium;'><?php if($data["W_JK"] ==1){echo "Laki-Laki"; } else {echo "Perempuan";} ?></span></td>         
              </tr>
              <tr>           
                   <td><span style='font-size: x-medium;'>Status Perkawinan</span></td>           
                   <td><span style='font-size: x-medium;'>:</span></td>           
                   <td><span style='font-size: x-medium;'><?php echo $data["W_STATUS"]; ?></span></td>         
              </tr>
              <tr>           
                   <td><span style='font-size: x-medium;'>Agama</span></td>           
                   <td><span style='font-size: x-medium;'>:</span></td>           
                   <td><span style='font-size: x-medium;'><?php echo $data["W_AGAMA"]; ?></span></td>         
              </tr>
              <tr>           
                   <td><span style='font-size: x-medium;'>Pekerjaan</span></td>           
                   <td><span style='font-size: x-medium;'>:</span></td>           
                   <td><span style='font-size: x-medium;'><?php echo $data["W_PEKERJAAN"]; ?></span></td>         
              </tr>
              <tr>           
                   <td><span style='font-size: x-medium;'>Alamat</span></td>           
                   <td><span style='font-size: x-medium;'>:</span></td>           
                   <td><span style='font-size: x-medium;'>RT/RW 0<?php echo $data["W_RT"].'/ 0'.$data["W_RW"].', Kelurahan '.$data["W_KELURAHAN"].', '.$data["W_KECAMATAN"].'<br>'.$data["W_KABUPATEN"].', '.$data["W_PROVINSI"]; ?></span></td>         
              </tr>
            </tbody>
        </table>
        <br>
       </div>
       <br>
       <br>
       </td>   
  </tr>
    <tr>     
       <td colspan='3'>
         <span style='font-size: x-medium;'>Benar nama yang tercantum diatas adalah warga Kelurahan <?php echo $data["W_KELURAHAN"].' Kecamatan '.$data["W_KECAMATAN"].' Kabupaten '.$data['W_KABUPATEN'].'.'; ?>Dengan Sepengetahuan kamidan sesuai data yang ada dikantor Desa orang tersebut diatas<br> benar keluarga Kurang Mampu <b>(KELUARGA BERPENGHASILAN RENDAH).</b></span>
             <br>
             <br>
       </td>   
  </tr>
    <tr>     
       <td colspan='3'>
            <span style='font-size: x-medium;'>Demikian Surat Keterangan Tidak Mampu ini kami buat dengan sebenar-benarnya untuk diketahui dan dipergunakan sebagaimana semestinya.
            </span>
       <br>
       <br>
       </td>   
  </tr>
  <tr>    
     <td width='0'></td>     
     <td width='100'></td>     
      <td width='80'>
        <div align='center'>
            <span style='font-size: x-medium;'><?php echo $data["W_KELURAHAN"].','.date('d F Y', strtotime('now')); ?></span>
        </div>
        <br>
        <div align='center'>
          <span style='font-size: x-medium'>Mengetahui,</span>
        </div>
        <div align='center'>
            <span style='font-size: x-medium;'>Kepala Desa</span>
        </div>
        <div align='center'>
        </div>
        <br>
        <br>
         <div align='center'>
              <span style='font-size: x-medium;'><?php echo $pl["PL_NAMA"]; ?></span>
          </div>
          <div align='center'>
          <!-- <span style='font-size: 13px;'>NIP : 18292893890380338</span> -->
        </div>
      </td>     
  </tr>
</tbody>
</table>
</page>