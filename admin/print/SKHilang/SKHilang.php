<?php
ob_start(); 
include('cetakSKHilang.php');
$content = ob_get_clean();
require_once('../../../assets/html2pdf/html2pdf.class.php');
$filename = "Surat Keterangan Hilang.pdf";
ob_end_clean();
try 
{
	$html2pdf = new HTML2PDF('P','A4','en',  false, 'ISO-8859-15',array(10, 10, 10, 10));
	$html2pdf->setDefaultFont('Arial');
	$html2pdf->writeHTML($content);
	$html2pdf->Output($filename);
}
catch(HTML2PDF_exception $e) { echo $e; }

?>