<?php 
//fungsi untuk menghitung umur. format tgl lahir dd-mm-yyyy
function calc_age($birthdate) {
    list($year,$month,$day) = explode("-",$birthdate);
    $year_diff  = date("Y") - $year;
    $month_diff = date("m") - $month;
    $day_diff   = date("d") - $day;
    if ($month_diff < 0) $year_diff--;
        elseif (($month_diff==0) && ($day_diff < 0)) $year_diff--;
    return $year_diff;
}

include('../../../controller/suratController.php');

$view = new SuratController();

$data = $view->view_print_request($_GET["key"]);
$pl   = $view->get_lurah();
?>
<style="">
  table, th, td {
      border: 0px solid black;
  }
  
  .absolute {
      position: absolute;
      top: 40px;
      left: 0;
      width: 50px;
      height: 50px;
  }
</style>
<page>

<table cellpadding='1'>
<tbody>
  <tr>    
  <td><div class='absolute'>
     <img src='../../../assets/logo-semarang.png' width='50'>
     </div></td>
     <td style='text-align: center;' colspan='2'>
            <span style='font-family: Verdana; font-size: x-medium;'><b>PEMERINTAH KOTA SEMARANG<br>
                      KECAMATAN SEMARANG UTARA<br>
                      KEPALA KELURAHAN BULU LOR</b><br></span>
                      <span>Jalan Raya Semarang telp.(0261) 202767, Kode Pos 53444</span>
                      <br>
                   <hr>
      </td>   
  </tr>
  <tr>    
      <td colspan='3'>
        <div align='center'>
            <span style='font-family: Verdana; text-align: center; font-size: 14px;'><b>SURAT KETERANGAN HEWAN</b></span><br>
                       <span style='text-align: center;'>============================</span>
                       <br>
                      <span style='font-family: Verdana; text-align: center; font-size: 12px;'>Nomor : <?php echo $data["NO_SURAT"]; ?></span>
         </div>
       </td>      
  </tr>
  
  <tr>     
     <td colspan='3'>
        <br>
        <span style='font-size: x-medium;'>Yang Bertanda Tangan Dibawah ini Kepala desa <?php echo $data["W_KELURAHAN"].' Kecamatan '.$data["W_KECAMATAN"].' Kabupaten '.$data['W_KABUPATEN'].' '; ?> Menerangkan Sesungguhnya Bahwa :</span>
     </td>     
  </tr>
  <tr>     
       <td valign='top' colspan='3'>
           <div align='left' style='margin-left: 30px'>
               <br>
               <?php $tanggallahir = $data["W_TGL_LAHIR"]; ?>
              <table style='margin-left: 50px'>
                   <tbody>
              <tr>           
                   <td width='80'><span style='font-size: x-medium;'>NIK</span></td>       <td width='10'><span style='font-size: x-medium;'>:</span></td>           
                   <td width='248'><span style='font-size: x-medium;'><?php echo $data["NIK"]; ?></span>
                   </td>         
              </tr>
              <tr>           
                   <td width='80'><span style='font-size: x-medium;'>Nama</span></td>       <td width='10'><span style='font-size: x-medium;'>:</span></td>           
                   <td width='248'><span style='font-size: x-medium;'><?php echo $data["PER_NAMA_WARGA"]; ?></span>
                   </td>         
              </tr>
              <tr>           
                   <td><span style='font-size: x-medium;'>Umur</span></td>           
                   <td><span style='font-size: x-medium;'>:</span></td>           
                   <td><span style='font-size: x-medium;'><?php echo calc_age($tanggallahir); ?> Tahun</span></td>         
              </tr>
              <tr>           
                   <td><span style='font-size: x-medium;'>Jenis Kelamin</span></td>           
                   <td><span style='font-size: x-medium;'>:</span></td>           
                   <td><span style='font-size: x-medium;'><?php if($data["W_JK"] ==1){echo "Laki-Laki"; } else {echo "Perempuan";} ?></span></td>         
              </tr>
              <tr>           
                   <td><span style='font-size: x-medium;'>Agama</span></td>           
                   <td><span style='font-size: x-medium;'>:</span></td>           
                   <td><span style='font-size: x-medium;'><?php echo $data["W_AGAMA"]; ?></span></td>         
              </tr>
              <tr>           
                   <td><span style='font-size: x-medium;'>Pekerjaan</span></td>           
                   <td><span style='font-size: x-medium;'>:</span></td>           
                   <td><span style='font-size: x-medium;'><?php echo $data["W_PEKERJAAN"]; ?></span></td>         
              </tr>
              <tr>           
                   <td><span style='font-size: x-medium;'>Alamat</span></td>           
                   <td><span style='font-size: x-medium;'>:</span></td>           
                   <td><span style='font-size: x-medium;'>RT/RW 0<?php echo $data["W_RT"].'/ 0'.$data["W_RW"].', Kelurahan '.$data["W_KELURAHAN"].', '.$data["W_KECAMATAN"].', '.$data["W_KABUPATEN"].', '.$data["W_PROVINSI"]; ?></span>
                   </td>         
              </tr>
              <tr>           
                   <td><span style='font-size: x-medium;'>Kewarganegaraan</span></td>           
                   <td><span style='font-size: x-medium;'>:</span></td>           
                   <td><span style='font-size: x-medium;'><?php echo $data["W_STATUS_PENDUDUK"]; ?></span></td>         
              </tr>
              
            </tbody>
        </table>
        <span style="font-size: x-medium;margin-left: 50px">Selanjutnya disebut <b>PIHAK PERTAMA</b></span>
       </div>
       </td>   
  </tr>
    <tr>     
       <td valign="top" colspan='3'>
         <span style='font-size: x-medium;'></span>
             <br>
             <br>
             <?php $tglLahir = $data["PM_TGL_LAHIR"]; ?>
             <table style="margin-left: 50px">
                <tbody>
                  <tr>           
                       <td width="80"><span style="font-size: x-medium;">Nama</span></td>      
                       <td width="10"><span style="font-size: x-medium;">:</span></td>           
                       <td width="248"><span style="font-size: x-medium;"><?php echo $data["PM_NAMA"]; ?></span>
                       </td>         
                  </tr>
                  <tr>           
                   <td><span style='font-size: x-medium;'>Umur</span></td>           
                   <td><span style='font-size: x-medium;'>:</span></td>           
                   <td><span style='font-size: x-medium;'><?php echo calc_age($tglLahir); ?> Tahun</span></td>         
                   </tr>
                    <tr>           
                       <td><span style="font-size: x-medium;">Jenis Kelamin</span></td>      
                       <td><span style="font-size: x-medium;">:</span></td>           
                       <td><span style='font-size: x-medium;'><?php if($data["PM_JK"] ==1){echo "Laki-Laki"; } else {echo "Perempuan";} ?></span></td>        
                    </tr>
                    <tr>           
                       <td><span style='font-size: x-medium;'>Agama</span></td>           
                       <td><span style='font-size: x-medium;'>:</span></td>           
                       <td><span style='font-size: x-medium;'><?php echo $data["PM_AGAMA"]; ?></span></td>         
                    </tr>
                    <tr>           
                         <td><span style='font-size: x-medium;'>Pekerjaan</span></td>           
                         <td><span style='font-size: x-medium;'>:</span></td>           
                         <td><span style='font-size: x-medium;'><?php echo $data["PM_PEKERJAAN"]; ?></span></td>         
                    </tr>
                    <tr>           
                         <td><span style='font-size: x-medium;'>Alamat</span></td>           
                         <td><span style='font-size: x-medium;'>:</span></td>           
                         <td><span style='font-size: x-medium;'>RT/RW 0<?php echo $data["PM_RT"].'/ 0'.$data["PM_RW"].', Kelurahan '.$data["PM_KELURAHAN"].', '.$data["PM_KECAMATAN"].', '.$data["PM_KABUPATEN"].', '.$data["PM_PROVINSI"]; ?></span>
                         </td>         
                    </tr>
                    <tr>           
                       <td><span style='font-size: x-medium;'>Kewarganegaraan</span></td>           
                       <td><span style='font-size: x-medium;'>:</span></td>           
                       <td><span style='font-size: x-medium;'><?php echo $data["PM_STATUS_PENDUDUK"]; ?></span></td>         
                  </tr>
                </tbody>
            </table>
            <span style="font-size: x-medium;margin-left: 50px">Selanjutnya disebut <b>PIHAK KEDUA</b></span>
       </td>   
  </tr>
  <br>
  <br><tr>     
     <td colspan='3'>
          <span style='font-size: x-medium;'><b>PIHAK PERTAMA</b> telah menjual hewan ternak berupa <b><?php echo $data["SKHW_JNS_HEWAN"]; ?></b> kepada PIHAK KEDUA dengan ciri-ciri sebagai berikut :
          </span>
     <br>
     <br>
     </td>   
  </tr>
  <tr>     
       <td valign="top" colspan='3'>
         <span style='font-size: x-medium;'></span>
             <br>
             <br>
             <table style="margin-left: 50px">
                <tbody>
                  <tr>           
                       <td width="80"><span style="font-size: x-medium;">Warna Bulu</span></td>      
                       <td width="10"><span style="font-size: x-medium;">:</span></td>           
                       <td width="248"><span style="font-size: x-medium;"><?php echo $data["SKHW_WARNA"]; ?></span>
                       </td>         
                  </tr>
                  <tr>           
                   <td><span style='font-size: x-medium;'>Tanduk</span></td>           
                   <td><span style='font-size: x-medium;'>:</span></td>           
                   <td><span style='font-size: x-medium;'><?php echo $data["SKHW_TANDUK"]; ?></span></td>         
                   </tr>
                    <tr>           
                       <td><span style="font-size: x-medium;">Jenis Kelamin</span></td>      
                       <td><span style="font-size: x-medium;">:</span></td>           
                       <td><span style='font-size: x-medium;'><?php if($data["SKHW_JK_HEWAN"] ==1){echo "Jantan"; } else {echo "Betina";} ?></span></td>        
                    </tr>
                </tbody>
            </table>
       </td>   
  </tr>
  <br>
  <br>
  <tr>     
     <td colspan='3'>
          <span style='font-size: x-medium;'>Keterangan ini hanya berlaku 1 kali jalan dibawa oleh kendaraan truck/Pick Up No. Pol. <?php echo $data["SKHW_NOPOL"]; ?>
          </span>
     <br>
     <br>
     </td>   
  </tr>
  <br>
  <br>
  <tr>     
     <td colspan='3'>
          <span style='font-size: x-medium;'>Demikian Surat Keterangan Hewan ini kami buat dengan sebenar-benarnya untuk diketahui dan dipergunakan sebagaimana semestinya.
          </span>
     <br>
     <br>
     </td>   
  </tr>
  <tr>    
     <td width='0'></td>     
     <td width='100'></td>     
      <td width='80'>
        <div align='center'>
            <span style='font-size: x-medium;'><?php echo $data["W_KELURAHAN"].','.date('d F Y', strtotime('now')); ?></span>
        </div>
        <br>
        <div align='center'>
          <span style='font-size: x-medium'>Mengetahui,</span>
        </div>
        <div align='center'>
            <span style='font-size: x-medium;'>Kepala Desa</span>
        </div>
        <div align='center'>
        </div>
        <br>
        <br>
         <div align='center'>
              <span style='font-size: x-medium;'><?php echo $pl["PL_NAMA"]; ?></span>
          </div>
          <div align='center'>
          <!-- tambahin nama pemohon/warga setempat -->
        </div>
      </td>     
      
  </tr>
</tbody>
</table>
</page>