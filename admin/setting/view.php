<?php
error_reporting(0);
session_start();
include_once '../../controller/adminController.php';
include_once '../../controller/globalController.php';

$gears = new AdminController();
$log  = new globalController();

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?= $log->name_app(); ?>  | NEWS</title>
    <!-- Favicon-->
    <!-- <link rel="icon" href="../../favicon.ico" type="image/x-icon"> -->

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

    <!-- Custom Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?= $log->base_url(); ?>assets/admin/css/themes/all-themes.css" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css">

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p><?= $log->loading_apps(); ?></p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <?php include_once '../../layouts/admin/navbar.php'; ?>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
         <?php include_once '../../layouts/admin/leftbar.php'; ?>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
        <aside id="rightsidebar" class="right-sidebar">
            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                <li role="presentation" class="active"><a href="#skins" data-toggle="tab">SKINS</a></li>
                <!-- <li role="presentation"><a href="#settings" data-toggle="tab">SETTINGS</a></li> -->
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active in active" id="skins">
                    <ul class="demo-choose-skin">
                        <li data-theme="red" class="active">
                            <div class="red"></div>
                            <span>Red</span>
                        </li>
                        <li data-theme="pink">
                            <div class="pink"></div>
                            <span>Pink</span>
                        </li>
                        <li data-theme="purple">
                            <div class="purple"></div>
                            <span>Purple</span>
                        </li>
                        <li data-theme="deep-purple">
                            <div class="deep-purple"></div>
                            <span>Deep Purple</span>
                        </li>
                        <li data-theme="indigo">
                            <div class="indigo"></div>
                            <span>Indigo</span>
                        </li>
                        <li data-theme="blue">
                            <div class="blue"></div>
                            <span>Blue</span>
                        </li>
                        <li data-theme="light-blue">
                            <div class="light-blue"></div>
                            <span>Light Blue</span>
                        </li>
                        <li data-theme="cyan">
                            <div class="cyan"></div>
                            <span>Cyan</span>
                        </li>
                        <li data-theme="teal">
                            <div class="teal"></div>
                            <span>Teal</span>
                        </li>
                        <li data-theme="green">
                            <div class="green"></div>
                            <span>Green</span>
                        </li>
                        <li data-theme="light-green">
                            <div class="light-green"></div>
                            <span>Light Green</span>
                        </li>
                        <li data-theme="lime">
                            <div class="lime"></div>
                            <span>Lime</span>
                        </li>
                        <li data-theme="yellow">
                            <div class="yellow"></div>
                            <span>Yellow</span>
                        </li>
                        <li data-theme="amber">
                            <div class="amber"></div>
                            <span>Amber</span>
                        </li>
                        <li data-theme="orange">
                            <div class="orange"></div>
                            <span>Orange</span>
                        </li>
                        <li data-theme="deep-orange">
                            <div class="deep-orange"></div>
                            <span>Deep Orange</span>
                        </li>
                        <li data-theme="brown">
                            <div class="brown"></div>
                            <span>Brown</span>
                        </li>
                        <li data-theme="grey">
                            <div class="grey"></div>
                            <span>Grey</span>
                        </li>
                        <li data-theme="blue-grey">
                            <div class="blue-grey"></div>
                            <span>Blue Grey</span>
                        </li>
                        <li data-theme="black">
                            <div class="black"></div>
                            <span>Black</span>
                        </li>
                    </ul>
                </div>

            </div>
        </aside>
        <!-- #END# Right Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>
                    Edit Settings
                    <!-- <small>Taken from <a href="https://datatables.net/" target="_blank">datatables.net</a></small> -->
                </h2>
            </div>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Edit Settings
                            </h2>
                        </div>
                        <?php $accesId = $_GET["cc"]; if($accesId == "sejarah") { ?>
                        <div class="body">
                            <?php
                            $about  = $gears->get_settings();
                             if(isset($_POST['simpan-sejarah'])) :
                                $gear['Id']  = $_POST['Id'];
                                $gear['sejarah']= $_POST['sejarah'];
                                $gears->updateSejarah($gear);
                             endif;
                          ?>
                            <form action="" method="post" enctype="multipart/form-data">
                                <div class="modal-body">
                                    <div class="form-group form-float">
                                        <input type="hidden" class="form-control" value="<?= $about['TS_BIGID']; ?>" name="Id" required>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <textarea id="summernote" name="sejarah" rows="10"><?= $about['TS_SEJARAH']; ?></textarea>
                                            <label class="form-label">Subject</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" name="simpan-sejarah" class="btn btn-link waves-effect">SAVE</button>
                                    <a type="button" class="btn btn-danger waves-effect" data-dismiss="modal"><span class="material-icons">close</span> CLOSE</a>
                                </div>
                            </form>
                        </div>
                        <?php } elseif($accesId == "profile") { ?>
                        <div class="body">
                            <?php
                             $about  = $gears->get_settings();
                             if(isset($_POST['simpan-profil'])) :
                                $gear['Id']     = $_POST['Id'];
                                $gear['profil'] = $_POST['profil'];
                                $gears->updateProfil($gear);
                             endif;
                          ?>
                            <form action="" method="post" enctype="multipart/form-data">
                                <div class="modal-body">
                                    <div class="form-group form-float">
                                        <input type="hidden" class="form-control" name="Id" value="<?= $about["TS_BIGID"]; ?>" required>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <textarea id="summernote" name="profil" rows="10"><?= $about["TS_PROFIL"]; ?></textarea>
                                            <label class="form-label">Profil Desa</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" name="simpan-profil" class="btn btn-link waves-effect">SAVE</button>
                                    <a type="button" class="btn btn-danger waves-effect" data-dismiss="modal"><span class="material-icons">close</span> CLOSE</a>
                                </div>
                            </form>
                        </div>
                        <?php } elseif($accesId == "visi") { ?>
                        <div class="body">
                            <?php
                            $about = $gears->get_settings();
                             if(isset($_POST['simpan-visi'])) :
                                $gear['Id']     = $_POST['Id'];
                                $gear['visi']= $_POST['visi'];
                                $gears->updateVisi($gear);
                             endif;
                          ?>
                            <form action="" method="post" enctype="multipart/form-data">
                                <div class="modal-body">
                                    <div class="form-group form-float">
                                       <input type="hidden" class="form-control" name="Id" value="<?= $about["TS_BIGID"]; ?>" required>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <textarea id="summernote" name="visi" rows="10"><?= $about["TS_VISI"]; ?></textarea>
                                            <label class="form-label">Visi &amp; Misi</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" name="simpan-visi" class="btn btn-link waves-effect">SAVE</button>
                                    <a type="button" class="btn btn-danger waves-effect" data-dismiss="modal"><span class="material-icons">close</span> CLOSE</a>
                                </div>
                            </form>
                        </div>
                        <?php } elseif($accesId == "peta") { ?>
                        <div class="body">
                            <?php
                             $about = $gears->get_settings();
                             if(isset($_POST['simpan-peta'])) :
                                $gear['Id']  = $_POST['Id'];
                                $gear['peta']= $_POST['peta'];
                                $gears->updatePeta($gear);
                             endif;
                          ?>
                            <form action="" method="post" enctype="multipart/form-data">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="defaultModalLabel">Edit Data Settings</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group form-float">
                                        <input type="hidden" class="form-control" name="Id" value="1" required>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <textarea id="summernote" name="peta" rows="10"><?= $about["TS_PETA"]; ?></textarea>
                                            <label class="form-label">Peta Desa</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" name="simpan-peta" class="btn btn-link waves-effect">SAVE</button>
                                    <a type="button" class="btn btn-danger waves-effect" data-dismiss="modal"><span class="material-icons">close</span> CLOSE</a>
                                </div>
                            </form>
                        </div>
                        <?php } elseif($accesId == "lpm") { ?>
                        <div class="body">
                            <?php
                            $about = $gears->get_settings();
                             if(isset($_POST['simpan-lpm'])) :
                                $gear['Id']  = $_POST['Id'];
                                $gear['lpm'] = $_POST['lpm'];
                                $gears->updateLpm($gear);
                             endif;
                          ?>
                            <form action="" method="post" enctype="multipart/form-data">
                                <div class="modal-body">
                                    <div class="form-group form-float">
                                        <input type="hidden" class="form-control" name="Id" value="<?= $about['TS_BIGID']; ?>" required>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <textarea id="summernote" name="lpm" rows="10"><?= $about['TS_LPM']; ?></textarea>
                                            <label class="form-label">LPM</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" name="simpan-lpm" class="btn btn-link waves-effect">SAVE</button>
                                    <a type="button" class="btn btn-danger waves-effect" data-dismiss="modal"><span class="material-icons">close</span> CLOSE</a>
                                </div>
                            </form>
                        </div>
                        <?php } elseif($accesId == "karang") { ?>
                        <div class="body">
                            <?php
                             $about = $gears->get_settings();
                             if(isset($_POST['simpan-karang'])) :
                                $gear['Id']  = $_POST['Id'];
                                $gear['karang']= $_POST['karang'];
                                $gears->updateKarang($gear);
                             endif;
                          ?>
                            <form action="" method="post" enctype="multipart/form-data">
                                <div class="modal-body">
                                    <div class="form-group form-float">
                                        <input type="hidden" class="form-control" name="Id" value="<?= $about['TS_BIGID']; ?>" required>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <textarea id="summernote" name="karang" rows="10"><?= $about["TS_KARANG_TARUNA"]; ?></textarea>
                                            <label class="form-label">Karang Taruna</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" name="simpan-karang" class="btn btn-link waves-effect">SAVE</button>
                                    <a type="button" class="btn btn-danger waves-effect" data-dismiss="modal"><span class="material-icons">close</span> CLOSE</a>
                                </div>
                            </form>
                        </div>
                        <?php } else { ?>
                        <div class="body">
                            <?php
                             $about = $gears->get_settings();
                             if(isset($_POST['simpan-pkk'])) :
                                $gear['Id']  = $_POST['Id'];
                                $gear['pkk'] = $_POST['pkk'] ;
                                $gears->updatePkk($gear);
                             endif;
                          ?>
                            <form action="" method="post" enctype="multipart/form-data">
                                <div class="modal-body">
                                    <div class="form-group form-float">
                                        <input type="hidden" class="form-control" name="judul" value="<?= $about["TS_BIGID"]; ?>" required>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <textarea id="summernote" name="pkk" rows="10"><?= $about["TS_PKK"]; ?></textarea>
                                            <label class="form-label">PKK</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" name="simpan-pkk" class="btn btn-link waves-effect">SAVE</button>
                                    <a type="button" class="btn btn-danger waves-effect" data-dismiss="modal"><span class="material-icons">close</span> CLOSE</a>
                                </div>
                            </form>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/node-waves/waves.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Custom Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/js/admin.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/js/pages/ui/modals.js"></script>

    <!-- Custom Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/js/admin.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/js/pages/tables/jquery-datatable.js"></script>

    <!-- Demo Js -->
    <!-- <script src="<?= $log->base_url(); ?>assets/admin/js/demo.js"></script> -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>
	<script>
	  $(document).ready(function() {
	      $('#summernote').summernote({
	          placeholder: 'Isikan Keterangan',
	          // tabsize: 2,
	          height: 250
	      });
	  });
	</script>
</body>

</html>
