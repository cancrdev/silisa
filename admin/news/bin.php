<?php
session_start();
include_once '../../controller/newsController.php';
include_once '../../controller/globalController.php';

$news = new NewsController();
$log  = new globalController();

$new  = $news->fetchAll();

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?= $log->name_app(); ?>  | NEWS</title>
    <!-- Favicon-->
    <!-- <link rel="icon" href="../../favicon.ico" type="image/x-icon"> -->

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

    <!-- Custom Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?= $log->base_url(); ?>assets/admin/css/themes/all-themes.css" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css">

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p><?= $log->loading_apps(); ?></p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <?php include_once '../../layouts/admin/navbar.php'; ?>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
         <?php include_once '../../layouts/admin/leftbar.php'; ?>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
        <aside id="rightsidebar" class="right-sidebar">
            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                <li role="presentation" class="active"><a href="#skins" data-toggle="tab">SKINS</a></li>
                <!-- <li role="presentation"><a href="#settings" data-toggle="tab">SETTINGS</a></li> -->
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active in active" id="skins">
                    <ul class="demo-choose-skin">
                        <li data-theme="red" class="active">
                            <div class="red"></div>
                            <span>Red</span>
                        </li>
                        <li data-theme="pink">
                            <div class="pink"></div>
                            <span>Pink</span>
                        </li>
                        <li data-theme="purple">
                            <div class="purple"></div>
                            <span>Purple</span>
                        </li>
                        <li data-theme="deep-purple">
                            <div class="deep-purple"></div>
                            <span>Deep Purple</span>
                        </li>
                        <li data-theme="indigo">
                            <div class="indigo"></div>
                            <span>Indigo</span>
                        </li>
                        <li data-theme="blue">
                            <div class="blue"></div>
                            <span>Blue</span>
                        </li>
                        <li data-theme="light-blue">
                            <div class="light-blue"></div>
                            <span>Light Blue</span>
                        </li>
                        <li data-theme="cyan">
                            <div class="cyan"></div>
                            <span>Cyan</span>
                        </li>
                        <li data-theme="teal">
                            <div class="teal"></div>
                            <span>Teal</span>
                        </li>
                        <li data-theme="green">
                            <div class="green"></div>
                            <span>Green</span>
                        </li>
                        <li data-theme="light-green">
                            <div class="light-green"></div>
                            <span>Light Green</span>
                        </li>
                        <li data-theme="lime">
                            <div class="lime"></div>
                            <span>Lime</span>
                        </li>
                        <li data-theme="yellow">
                            <div class="yellow"></div>
                            <span>Yellow</span>
                        </li>
                        <li data-theme="amber">
                            <div class="amber"></div>
                            <span>Amber</span>
                        </li>
                        <li data-theme="orange">
                            <div class="orange"></div>
                            <span>Orange</span>
                        </li>
                        <li data-theme="deep-orange">
                            <div class="deep-orange"></div>
                            <span>Deep Orange</span>
                        </li>
                        <li data-theme="brown">
                            <div class="brown"></div>
                            <span>Brown</span>
                        </li>
                        <li data-theme="grey">
                            <div class="grey"></div>
                            <span>Grey</span>
                        </li>
                        <li data-theme="blue-grey">
                            <div class="blue-grey"></div>
                            <span>Blue Grey</span>
                        </li>
                        <li data-theme="black">
                            <div class="black"></div>
                            <span>Black</span>
                        </li>
                    </ul>
                </div>

            </div>
        </aside>
        <!-- #END# Right Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>
                    Master Data Berita
                    <!-- <small>Taken from <a href="https://datatables.net/" target="_blank">datatables.net</a></small> -->
                </h2>
            </div>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Master data Berita
                            </h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                            	<button type="button" class="btn btn-primary waves-effect m-r-20" data-toggle="modal" data-target="#defaultTambah"><span class="material-icons">note_add</span> Tambah Data</button><br><br>
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Judul</th>
                                            <th>Subject</th>
                                            <th>Image</th>
                                            <th>Tanggal</th>
                                            <th>Dibuat</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php $no = 1; foreach($new as $nw) : ?>
                                        <tr>
                                            <td><?= $no++; ?></td>
                                            <td><?= $nw["N_TITLE"]; ?></td>
                                            <td>
                                            	<button type="button" class="btn btn-danger waves-effect m-r-20" data-toggle="modal" data-target="#defaultModal<?= $no; ?>"><span class="material-icons">find_in_page</span> Detail</button>
                                            	<div class="modal fade" id="defaultModal<?= $no; ?>" tabindex="-1" role="dialog">
									                <div class="modal-dialog modal-lg" role="document">
									                    <div class="modal-content">
									                        <div class="modal-header">
									                            <h4 class="modal-title" id="defaultModalLabel">Detail Isi News</h4>
									                        </div>
									                        <div class="modal-body">
									                            <?= $nw["N_SUBJECT"]; ?>
									                        </div>
									                        <div class="modal-footer">
									                            <!-- <button type="button" class="btn btn-link waves-effect">SAVE CHANGES</button> -->
									                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal"><span class="material-icons">close</span> CLOSE</button>
									                        </div>
									                    </div>
									                </div>
									            </div>
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-danger waves-effect m-r-20" data-toggle="modal" data-target="#defaultModals<?= $no; ?>"><span class="material-icons">image</span>View Detail</button>
                                                <div class="modal fade" id="defaultModals<?= $no; ?>" tabindex="-1" role="dialog">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="defaultModalLabel">Detail Gambar</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <img width="400px" src="<?= $log->base_url(); ?>assets/news/<?= $nw["N_IMAGE"] ?>" alt="">
                                                            </div>
                                                            <div class="modal-footer">
                                                                <!-- <button type="button" class="btn btn-link waves-effect">SAVE CHANGES</button> -->
                                                                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal"><span class="material-icons">close</span> CLOSE</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td><?= $log->TanggalIndo($nw["N_TANGGAL"]); ?></td>
                                            <td><?= $nw["N_CREATED_BY"]; ?></td>
                                            <td>
                                            	<ul class="header-dropdown m-r--5">
					                                <li class="dropdown">
					                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
					                                        <i class="material-icons">more_vert</i>
					                                    </a>
					                                    <ul class="dropdown-menu pull-right">
					                                        <li><a href="view?key=<?= $nw["N_ID"]; ?>"><span class="material-icons">edit</span> EDIT</a></li>

					                                        <li><a href="delete?key=<?= $nw["N_ID"]; ?>" class="delete-link"><span class="material-icons">delete</span> DELETE</a></li>
					                                    </ul>
					                                </li>
					                            </ul>
                                            </td>
                                        </tr>
                                    	<?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
        </div>
        <div class="modal fade" id="defaultTambah" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <?php
                     if(isset($_POST['simpan'])) :
                        $new['judul']  = $_POST['judul'];
                        $new['subject']= $_POST['subject'];
                        $new['image']  = $_FILES['image'];
                        $news->add_news($new);
                     endif;
                  ?>
                	<form action="" method="post" enctype="multipart/form-data">
	                    <div class="modal-header">
	                        <h4 class="modal-title" id="defaultModalLabel">Tambah Data</h4>
	                    </div>
	                    <div class="modal-body">
	                        <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="judul" required>
                                    <label class="form-label">Judul</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="file" class="form-control" name="image" required>
                                    <!-- <label class="form-label">Judul</label> -->
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <textarea id="summernote" name="subject" rows="10"></textarea>
                                    <label class="form-label">Subject</label>
                                </div>
                            </div>
	                    </div>
	                    <div class="modal-footer">
	                        <button type="submit" name="simpan" class="btn btn-link waves-effect">SAVE</button>
	                        <a type="button" class="btn btn-danger waves-effect" data-dismiss="modal"><span class="material-icons">close</span> CLOSE</a>
	                    </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/node-waves/waves.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Custom Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/js/admin.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/js/pages/ui/modals.js"></script>

    <!-- Custom Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/js/admin.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/js/pages/tables/jquery-datatable.js"></script>

    <!-- Demo Js -->
    <!-- <script src="<?= $log->base_url(); ?>assets/admin/js/demo.js"></script> -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>
	<script>
	  $(document).ready(function() {
	      $('#summernote').summernote({
	          placeholder: 'Isikan Keterangan',
	          // tabsize: 2,
	          height: 250
	      });
	  });
	</script>

	<script type="text/javascript">
    jQuery(document).ready(function($){
        $('.delete-link').on('click',function(){
            var getLink = $(this).attr('href');
            swal({
            title: "Apakah Kamu yakin menghapus data?",
            text: "Data yang dihapus akan hilang",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Hapus",
            cancelButtonText: "Kembali",
            closeOnConfirm: false,
            closeOnCancel: true,
          },function(){
            window.location.href = getLink
          });
            return false;
          });
    });
</script>
</body>

</html>
