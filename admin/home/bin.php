<?php
session_start();
include_once '../../controller/globalController.php';
include_once '../../controller/CounterController.php';

$log     = new globalController();
$jumlah  = new CounterController();


//counter
//letter
$print    = $jumlah->letter_to_print();
$w_kades  = $jumlah->letter_waiting_kades();
$w_rw     = $jumlah->letter_waiting_rw();
$w_rt     = $jumlah->letter_waiting_rt();
//aduan
$aduan_wrt= $jumlah->aduan_waiting_rt();
$aduan_wrw= $jumlah->aduan_waiting_rw();
$aduan_kad= $jumlah->aduan_waiting_kades();
$aduan_end= $jumlah->aduan_selesai();

// warga
$menikah  = $jumlah->warga_menikah();
$blm_mnkh = $jumlah->warga_blmmenikah();
$cerai    = $jumlah->cerai();
$islam    = $jumlah->agama_islam();
$kristen  = $jumlah->agama_kristen();
$katolik  = $jumlah->agama_katolik();
$budha    = $jumlah->agama_budha();
$agama_lan= $jumlah->agama_lain();
$all_warga= $jumlah->AllWarga();
$wilayah  = $jumlah->DataWilayah();
$wajar    = $jumlah->study_wajar();
$lanjut   = $jumlah->study_lanjut();
$male     = $jumlah->gender_male();
$female   = $jumlah->gender_female();
$work     = $jumlah->work();
$not_work = $jumlah->notWork();

//permintaann
$minta    = $jumlah->jumlah_permintaan();
$adu      = $jumlah->jumlah_aduan();
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?= $log->name_app(); ?> | Dashboard Admin</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="<?= $log->base_url(); ?>assets/admin/plugins/morrisjs/morris.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?= $log->base_url(); ?>assets/admin/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?= $log->base_url(); ?>assets/admin/css/themes/all-themes.css" rel="stylesheet" />
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p><?= $log->loading_apps(); ?></p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <?php include_once '../../layouts/admin/navbar.php'; ?>
    <!-- #Top Bar -->
    
    <!-- Left Sidebar -->
    <?php include_once '../../layouts/admin/leftbar.php'; ?>
    <!-- #END# Left Sidebar -->
    

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD ADMIN (KADES) | <span class="material-icons">person </span> <?= $_SESSION["U_FULLNAME"]; ?></h2>
            </div>
            <!-- Widgets -->
            <a href="../request/print"><div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">playlist_add_check</i>
                        </div>
                        <div class="content">
                            <div class="text">Surat Siap Dicetak</div>
                            <div class="number count-to" data-from="0" data-to="<?= $print; ?>" data-speed="15" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">help</i>
                        </div>
                        <div class="content">
                            <div class="text">Surat Menunggu Kades</div>
                            <div class="number count-to" data-from="0" data-to="<?= $w_kades; ?>" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">forum</i>
                        </div>
                        <div class="content">
                            <div class="text">Surat Menunggu RW</div>
                            <div class="number count-to" data-from="0" data-to="<?= $w_rw; ?>" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">person_add</i>
                        </div>
                        <div class="content">
                            <div class="text">Surat Menunggu RT</div>
                            <div class="number count-to" data-from="0" data-to="<?= $w_rt; ?>" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
            </div></a>
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">playlist_add_check</i>
                        </div>
                        <div class="content">
                            <div class="text">Pengaduan Selesai</div>
                            <div class="number count-to" data-from="0" data-to="<?= $aduan_end; ?>" data-speed="15" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">help</i>
                        </div>
                        <div class="content">
                            <div class="text">Pegaduan Menunggu Kades</div>
                            <div class="number count-to" data-from="0" data-to="<?= $aduan_kad; ?>" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">forum</i>
                        </div>
                        <div class="content">
                            <div class="text">Pengaduan Menunggu RW</div>
                            <div class="number count-to" data-from="0" data-to="<?= $aduan_wrw; ?>" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">person_add</i>
                        </div>
                        <div class="content">
                            <div class="text">Pengaduan Menunggu RT</div>
                            <div class="number count-to" data-from="0" data-to="<?= $aduan_wrt; ?>" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Widgets -->
            <!-- CPU Usage -->
            <!-- #END# CPU Usage -->
            <div class="row clearfix">
                <!-- Visitors -->
                <!-- #END# Visitors -->
                <!-- Latest Social Trends -->
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="card">
                        <div class="body bg-cyan">
                            <div class="font-bold m-b--35">Data Status Dan Agama</div>
                            <ul class="dashboard-stat-list">
                                <li>
                                    Sudah Menikah
                                    <span class="pull-right"><b><?= $menikah; ?></b> <small>WARGA</small></span>
                                </li>
                                <li>
                                    Belum Menikah
                                    <span class="pull-right"><b><?= $blm_mnkh; ?></b> <small>WARGA</small></span>
                                </li>
                                <li>
                                    Janda/Duda
                                    <span class="pull-right"><b><?= $cerai; ?></b> <small>WARGA</small></span>
                                </li>
                                <li>
                                    Islam
                                    <span class="pull-right"><b><?= $islam; ?></b> <small>WARGA</small></span>
                                </li>
                                <li>
                                    Kristen
                                    <span class="pull-right"><b><?= $kristen; ?></b> <small>WARGA</small></span>
                                </li>
                                <li>
                                    Budha
                                    <span class="pull-right"><b><?= $budha; ?></b> <small>WARGA</small></span>
                                </li>
                                <li>
                                    Katolik
                                    <span class="pull-right"><b><?= $katolik; ?></b> <small>WARGA</small></span>
                                </li>
                                <li>
                                    Lain - Lain
                                    <span class="pull-right"><b><?= $agama_lan; ?></b> <small>WARGA</small></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- #END# Latest Social Trends -->
                <!-- Answered Tickets -->
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="card">
                        <div class="body bg-teal">
                            <div class="font-bold m-b--35">Data Kelompok Desa</div>
                            <ul class="dashboard-stat-list">
                                <li>
                                    Data Wilayah
                                    <span class="pull-right"><b><?= number_format($wilayah,2); ?></b> KM<sub>2</sub></span>
                                </li>
                                <li>
                                    Total Warga
                                    <span class="pull-right"><b><?= $all_warga; ?></b> <small>WARGA</small></span>
                                </li>
                                <li>
                                    Data Pendidikan Wajib 12 Tahun
                                    <span class="pull-right"><b><?= $wajar; ?></b> <small>WARGA</small></span>
                                </li>
                                <li>
                                    Data Pendidikan Lanjutan
                                    <span class="pull-right"><b><?= $lanjut; ?></b> <small>WARGA</small></span>
                                </li>
                                <li>
                                    Data Gender Laki-Laki
                                    <span class="pull-right"><b><?= $male; ?></b> <small>WARGA</small></span>
                                </li>
                                <li>
                                    Data Gender Perempuan
                                    <span class="pull-right"><b><?= $female; ?></b> <small>WARGA</small></span>
                                </li>
                                <li>
                                    Data Kelompok Pelajar/Mahasiswa
                                    <span class="pull-right"><b><?= $work; ?></b> <small>WARGA</small></span>
                                </li>
                                <li>
                                    Data Kelompok Belum/tidak Bekerja
                                    <span class="pull-right"><b><?= $not_work; ?></b> <small>WARGA</small></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- #END# Answered Tickets -->
            </div>
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- ChartJs -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/chartjs/Chart.bundle.js"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/plugins/jquery-sparkline/jquery.sparkline.js"></script>

    <!-- Custom Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/js/admin.js"></script>
    <script src="<?= $log->base_url(); ?>assets/admin/js/pages/index.js"></script>

    <!-- Demo Js -->
    <script src="<?= $log->base_url(); ?>assets/admin/js/demo.js"></script>
</body>

</html>
